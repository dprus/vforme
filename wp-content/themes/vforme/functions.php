<?php
// ============================ ATTACHING STYLES =======================================================================
function wptuts_styles_with_the_lot() {
	wp_register_style( 'main-style', get_template_directory_uri(). '/style.css', array(), '1', 'all');
	wp_enqueue_style('main-style');
}
add_action('wp_enqueue_scripts', 'wptuts_styles_with_the_lot');

// ============================ ATTACHING SCRIPTS ======================================================================
function wptuts_scripts_important() {
	wp_register_script('popper', 'https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js');
	wp_enqueue_script('popper');

	wp_register_script('gtag', 'https://www.googletagmanager.com/gtag/js?id=UA-106827506-2');
	wp_enqueue_script('gtag');

	wp_register_script('gtaginit', get_template_directory_uri(). '/js/gtaginit.js');
	wp_enqueue_script('gtaginit');

	wp_register_script('yametrika', get_template_directory_uri(). '/js/yametrika.js');
	wp_enqueue_script('yametrika');

	wp_register_script('main-script', get_template_directory_uri(). '/js/main.js');
	wp_enqueue_script('main-script');

	wp_register_script('jqueryui', 'http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.5/jquery-ui.min.js');
	wp_enqueue_script('jqueryui');
}
add_action('wp_enqueue_scripts', 'wptuts_scripts_important');


// ============================ DEFINING MENU POSITIONS ================================================================
register_nav_menus(array(
	'top' => __('Область верхнего меню', 'top menu text'),
));

register_nav_menus(array(
	'bottom' => __('Область нижнего меню', 'bottom menu text'),
));


// ============================ TRANSLITERATION FUNCTIONS ==============================================================
$gost = array(
	'А' => 'A', 'а' => 'a',
	'Б' => 'B', 'б' => 'b',
	'В' => 'V', 'в' => 'v',
	'Г' => 'G', 'г' => 'g',
	'Д' => 'D', 'д' => 'd',
	'Е' => 'E', 'е' => 'e',
	'Ё' => 'Jo', 'ё' => 'jo',
	'Ж' => 'Zh', 'ж' => 'zh',
	'З' => 'Z', 'з' => 'z',
	'И' => 'I', 'и' => 'i',
	'Й' => 'J', 'й' => 'jj',
	'К' => 'K', 'к' => 'k',
	'Л' => 'L', 'л' => 'l',
	'М' => 'M', 'м' => 'm',
	'Н' => "N", 'н' => 'n',
	'О' => 'O', 'о' => 'o',
	'П' => 'P', 'п' => 'p',
	'Р' => 'R', 'р' => 'r',
	'С' => 'S', 'с' => 's',
	'Т' => 'T', 'т' => 't',
	'У' => 'U', 'у' => 'u',
	'Ф' => 'F', 'ф' => 'f',
	'Х' => 'Kh', 'х' => 'kh',
	'Ц' => 'C', 'ц' => 'c',
	'Ч' => 'Ch', 'ч' => 'ch',
	'Ш' => 'Sh', 'ш' => 'sh',
	'Щ' => 'Shh', 'щ' => 'shh',
	'Ъ' => 'ʺ', 'ъ' => 'ʺ',
	'Ы' => 'Y', 'ы' => 'y',
	'Ь' => '\'', 'ь' => '\'',
	'Э' => 'E', 'э' => 'e',
	'Ю' => 'Ju', 'ю' => 'Ju',
	'Я' => 'Ja', 'я' => 'Ja',
);

$iso = array(
	'А' => 'A', 'а' => 'a',
	'B' => 'B', 'б' => 'b',
	'V' => 'V', 'в' => 'v',
	'G' => 'G', 'г' => 'g',
	'F' => 'D', 'д' => 'd',
	'' => 'E', 'е' => 'e',
	'' => 'Ё', 'ё' => 'ё',
	'' => 'Ž', 'ж' => 'ž',
	'' => 'z', 'з' => 'z',
	'' => 'I', 'и' => 'i',
	'' => 'J', 'й' => 'j',
	'' => 'K', 'к' => 'k',
	'' => 'L', 'л' => 'l',
	'' => 'M', 'м' => 'm',
	'' => 'N', 'н' => 'n',
	'' => 'O', 'о' => 'o',
	'' => 'P', 'п' => 'p',
	'' => 'R', 'р' => 'r',
	'' => 'S', 'с' => 's',
	'' => 'T', 'т' => 't',
	'' => 'U', 'у' => 'u',
	'' => 'F', 'ф' => 'f',
	'' => 'H', 'х' => 'h',
	'' => 'C', 'ц' => 'c',
	'' => 'Č', 'ч' => 'č',
	'' => 'Š', 'ш' => 'š',
	'' => 'Ŝ', 'щ' => 'ŝ',
	'' => '"', 'ъ' => '"',
	'' => 'Y', 'ы' => 'y',
	'' => '\'', 'ь' => '\'',
	'' => 'È', 'э' => 'è',
	'' => 'Û', 'ю' => 'û',
	'' => 'Â', 'я' => 'â',
);

function sanitize_title_with_translit($title) {
	global $gost, $iso;
	$rtl_standart = get_option('rtl_standart');
	switch ($rtl_standart) {
		case 'off':
			return $title;
		case 'gost':
			return strtr($title, $gost);
		default:
			return strtr($title, $iso);
	}
}

add_action('sanitize_title', 'sanitize_title_with_translit', 0);