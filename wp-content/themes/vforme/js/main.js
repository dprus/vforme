!function (e, t) {
    "use strict";
    "object" == typeof module && "object" == typeof module.exports ? module.exports = e.document ? t(e, !0) : function (e) {
        if (!e.document) throw new Error("jQuery requires a window with a document");
        return t(e)
    } : t(e)
}("undefined" != typeof window ? window : this, function (e, t) {
    "use strict";

    function n(e, t) {
        var n = (t = t || Y).createElement("script");
        n.text = e, t.head.appendChild(n).parentNode.removeChild(n)
    }

    function r(e) {
        var t = !!e && "length" in e && e.length, n = se.type(e);
        return "function" !== n && !se.isWindow(e) && ("array" === n || 0 === t || "number" == typeof t && t > 0 && t - 1 in e)
    }

    function i(e, t) {
        return e.nodeName && e.nodeName.toLowerCase() === t.toLowerCase()
    }

    function o(e, t, n) {
        return se.isFunction(t) ? se.grep(e, function (e, r) {
            return !!t.call(e, r, e) !== n
        }) : t.nodeType ? se.grep(e, function (e) {
            return e === t !== n
        }) : "string" != typeof t ? se.grep(e, function (e) {
            return ee.call(t, e) > -1 !== n
        }) : me.test(t) ? se.filter(t, e, n) : (t = se.filter(t, e), se.grep(e, function (e) {
            return ee.call(t, e) > -1 !== n && 1 === e.nodeType
        }))
    }

    function a(e, t) {
        for (; (e = e[t]) && 1 !== e.nodeType;) ;
        return e
    }

    function s(e) {
        return e
    }

    function u(e) {
        throw e
    }

    function l(e, t, n, r) {
        var i;
        try {
            e && se.isFunction(i = e.promise) ? i.call(e).done(t).fail(n) : e && se.isFunction(i = e.then) ? i.call(e, t, n) : t.apply(void 0, [e].slice(r))
        } catch (e) {
            n.apply(void 0, [e])
        }
    }

    function c() {
        Y.removeEventListener("DOMContentLoaded", c), e.removeEventListener("load", c), se.ready()
    }

    function f() {
        this.expando = se.expando + f.uid++
    }

    function p(e, t, n) {
        var r;
        if (void 0 === n && 1 === e.nodeType) if (r = "data-" + t.replace(Ae, "-$&").toLowerCase(), "string" == typeof(n = e.getAttribute(r))) {
            try {
                n = function (e) {
                    return "true" === e || "false" !== e && ("null" === e ? null : e === +e + "" ? +e : je.test(e) ? JSON.parse(e) : e)
                }(n)
            } catch (e) {
            }
            De.set(e, t, n)
        } else n = void 0;
        return n
    }

    function d(e, t, n, r) {
        var i, o = 1, a = 20, s = r ? function () {
                return r.cur()
            } : function () {
                return se.css(e, t, "")
            }, u = s(), l = n && n[3] || (se.cssNumber[t] ? "" : "px"),
            c = (se.cssNumber[t] || "px" !== l && +u) && Le.exec(se.css(e, t));
        if (c && c[3] !== l) {
            l = l || c[3], n = n || [], c = +u || 1;
            do {
                o = o || ".5", c /= o, se.style(e, t, c + l)
            } while (o !== (o = s() / u) && 1 !== o && --a)
        }
        return n && (c = +c || +u || 0, i = n[1] ? c + (n[1] + 1) * n[2] : +n[2], r && (r.unit = l, r.start = c, r.end = i)), i
    }

    function h(e) {
        var t, n = e.ownerDocument, r = e.nodeName, i = Pe[r];
        return i || (t = n.body.appendChild(n.createElement(r)), i = se.css(t, "display"), t.parentNode.removeChild(t), "none" === i && (i = "block"), Pe[r] = i, i)
    }

    function g(e, t) {
        for (var n, r, i = [], o = 0, a = e.length; o < a; o++) (r = e[o]).style && (n = r.style.display, t ? ("none" === n && (i[o] = Ne.get(r, "display") || null, i[o] || (r.style.display = "")), "" === r.style.display && Fe(r) && (i[o] = h(r))) : "none" !== n && (i[o] = "none", Ne.set(r, "display", n)));
        for (o = 0; o < a; o++) null != i[o] && (e[o].style.display = i[o]);
        return e
    }

    function v(e, t) {
        var n;
        return n = void 0 !== e.getElementsByTagName ? e.getElementsByTagName(t || "*") : void 0 !== e.querySelectorAll ? e.querySelectorAll(t || "*") : [], void 0 === t || t && i(e, t) ? se.merge([e], n) : n
    }

    function m(e, t) {
        for (var n = 0, r = e.length; n < r; n++) Ne.set(e[n], "globalEval", !t || Ne.get(t[n], "globalEval"))
    }

    function y(e, t, n, r, i) {
        for (var o, a, s, u, l, c, f = t.createDocumentFragment(), p = [], d = 0, h = e.length; d < h; d++) if ((o = e[d]) || 0 === o) if ("object" === se.type(o)) se.merge(p, o.nodeType ? [o] : o); else if ($e.test(o)) {
            for (a = a || f.appendChild(t.createElement("div")), s = (Me.exec(o) || ["", ""])[1].toLowerCase(), u = We[s] || We._default, a.innerHTML = u[1] + se.htmlPrefilter(o) + u[2], c = u[0]; c--;) a = a.lastChild;
            se.merge(p, a.childNodes), (a = f.firstChild).textContent = ""
        } else p.push(t.createTextNode(o));
        for (f.textContent = "", d = 0; o = p[d++];) if (r && se.inArray(o, r) > -1) i && i.push(o); else if (l = se.contains(o.ownerDocument, o), a = v(f.appendChild(o), "script"), l && m(a), n) for (c = 0; o = a[c++];) Ie.test(o.type || "") && n.push(o);
        return f
    }

    function x() {
        return !0
    }

    function b() {
        return !1
    }

    function w() {
        try {
            return Y.activeElement
        } catch (e) {
        }
    }

    function T(e, t, n, r, i, o) {
        var a, s;
        if ("object" == typeof t) {
            "string" != typeof n && (r = r || n, n = void 0);
            for (s in t) T(e, s, n, r, t[s], o);
            return e
        }
        if (null == r && null == i ? (i = n, r = n = void 0) : null == i && ("string" == typeof n ? (i = r, r = void 0) : (i = r, r = n, n = void 0)), !1 === i) i = b; else if (!i) return e;
        return 1 === o && (a = i, i = function (e) {
            return se().off(e), a.apply(this, arguments)
        }, i.guid = a.guid || (a.guid = se.guid++)), e.each(function () {
            se.event.add(this, t, i, r, n)
        })
    }

    function C(e, t) {
        return i(e, "table") && i(11 !== t.nodeType ? t : t.firstChild, "tr") ? se(">tbody", e)[0] || e : e
    }

    function E(e) {
        return e.type = (null !== e.getAttribute("type")) + "/" + e.type, e
    }

    function k(e) {
        var t = Ye.exec(e.type);
        return t ? e.type = t[1] : e.removeAttribute("type"), e
    }

    function S(e, t) {
        var n, r, i, o, a, s, u, l;
        if (1 === t.nodeType) {
            if (Ne.hasData(e) && (o = Ne.access(e), a = Ne.set(t, o), l = o.events)) {
                delete a.handle, a.events = {};
                for (i in l) for (n = 0, r = l[i].length; n < r; n++) se.event.add(t, i, l[i][n])
            }
            De.hasData(e) && (s = De.access(e), u = se.extend({}, s), De.set(t, u))
        }
    }

    function N(e, t) {
        var n = t.nodeName.toLowerCase();
        "input" === n && Re.test(e.type) ? t.checked = e.checked : "input" !== n && "textarea" !== n || (t.defaultValue = e.defaultValue)
    }

    function D(e, t, r, i) {
        t = K.apply([], t);
        var o, a, s, u, l, c, f = 0, p = e.length, d = p - 1, h = t[0], g = se.isFunction(h);
        if (g || p > 1 && "string" == typeof h && !ae.checkClone && Ge.test(h)) return e.each(function (n) {
            var o = e.eq(n);
            g && (t[0] = h.call(this, n, o.html())), D(o, t, r, i)
        });
        if (p && (o = y(t, e[0].ownerDocument, !1, e, i), a = o.firstChild, 1 === o.childNodes.length && (o = a), a || i)) {
            for (u = (s = se.map(v(o, "script"), E)).length; f < p; f++) l = o, f !== d && (l = se.clone(l, !0, !0), u && se.merge(s, v(l, "script"))), r.call(e[f], l, f);
            if (u) for (c = s[s.length - 1].ownerDocument, se.map(s, k), f = 0; f < u; f++) l = s[f], Ie.test(l.type || "") && !Ne.access(l, "globalEval") && se.contains(c, l) && (l.src ? se._evalUrl && se._evalUrl(l.src) : n(l.textContent.replace(Qe, ""), c))
        }
        return e
    }

    function j(e, t, n) {
        for (var r, i = t ? se.filter(t, e) : e, o = 0; null != (r = i[o]); o++) n || 1 !== r.nodeType || se.cleanData(v(r)), r.parentNode && (n && se.contains(r.ownerDocument, r) && m(v(r, "script")), r.parentNode.removeChild(r));
        return e
    }

    function A(e, t, n) {
        var r, i, o, a, s = e.style;
        return (n = n || Ze(e)) && ("" !== (a = n.getPropertyValue(t) || n[t]) || se.contains(e.ownerDocument, e) || (a = se.style(e, t)), !ae.pixelMarginRight() && Ke.test(a) && Je.test(t) && (r = s.width, i = s.minWidth, o = s.maxWidth, s.minWidth = s.maxWidth = s.width = a, a = n.width, s.width = r, s.minWidth = i, s.maxWidth = o)), void 0 !== a ? a + "" : a
    }

    function q(e, t) {
        return {
            get: function () {
                return e() ? void delete this.get : (this.get = t).apply(this, arguments)
            }
        }
    }

    function L(e) {
        var t = se.cssProps[e];
        return t || (t = se.cssProps[e] = function (e) {
            if (e in ot) return e;
            for (var t = e[0].toUpperCase() + e.slice(1), n = it.length; n--;) if ((e = it[n] + t) in ot) return e
        }(e) || e), t
    }

    function H(e, t, n) {
        var r = Le.exec(t);
        return r ? Math.max(0, r[2] - (n || 0)) + (r[3] || "px") : t
    }

    function F(e, t, n, r, i) {
        var o, a = 0;
        for (o = n === (r ? "border" : "content") ? 4 : "width" === t ? 1 : 0; o < 4; o += 2) "margin" === n && (a += se.css(e, n + He[o], !0, i)), r ? ("content" === n && (a -= se.css(e, "padding" + He[o], !0, i)), "margin" !== n && (a -= se.css(e, "border" + He[o] + "Width", !0, i))) : (a += se.css(e, "padding" + He[o], !0, i), "padding" !== n && (a += se.css(e, "border" + He[o] + "Width", !0, i)));
        return a
    }

    function O(e, t, n) {
        var r, i = Ze(e), o = A(e, t, i), a = "border-box" === se.css(e, "boxSizing", !1, i);
        return Ke.test(o) ? o : (r = a && (ae.boxSizingReliable() || o === e.style[t]), "auto" === o && (o = e["offset" + t[0].toUpperCase() + t.slice(1)]), (o = parseFloat(o) || 0) + F(e, t, n || (a ? "border" : "content"), r, i) + "px")
    }

    function P(e, t, n, r, i) {
        return new P.prototype.init(e, t, n, r, i)
    }

    function R() {
        st && (!1 === Y.hidden && e.requestAnimationFrame ? e.requestAnimationFrame(R) : e.setTimeout(R, se.fx.interval), se.fx.tick())
    }

    function M() {
        return e.setTimeout(function () {
            at = void 0
        }), at = se.now()
    }

    function I(e, t) {
        var n, r = 0, i = {height: e};
        for (t = t ? 1 : 0; r < 4; r += 2 - t) n = He[r], i["margin" + n] = i["padding" + n] = e;
        return t && (i.opacity = i.width = e), i
    }

    function W(e, t, n) {
        for (var r, i = ($.tweeners[t] || []).concat($.tweeners["*"]), o = 0, a = i.length; o < a; o++) if (r = i[o].call(n, t, e)) return r
    }

    function $(e, t, n) {
        var r, i, o = 0, a = $.prefilters.length, s = se.Deferred().always(function () {
            delete u.elem
        }), u = function () {
            if (i) return !1;
            for (var t = at || M(), n = Math.max(0, l.startTime + l.duration - t), r = 1 - (n / l.duration || 0), o = 0, a = l.tweens.length; o < a; o++) l.tweens[o].run(r);
            return s.notifyWith(e, [l, r, n]), r < 1 && a ? n : (a || s.notifyWith(e, [l, 1, 0]), s.resolveWith(e, [l]), !1)
        }, l = s.promise({
            elem: e,
            props: se.extend({}, t),
            opts: se.extend(!0, {specialEasing: {}, easing: se.easing._default}, n),
            originalProperties: t,
            originalOptions: n,
            startTime: at || M(),
            duration: n.duration,
            tweens: [],
            createTween: function (t, n) {
                var r = se.Tween(e, l.opts, t, n, l.opts.specialEasing[t] || l.opts.easing);
                return l.tweens.push(r), r
            },
            stop: function (t) {
                var n = 0, r = t ? l.tweens.length : 0;
                if (i) return this;
                for (i = !0; n < r; n++) l.tweens[n].run(1);
                return t ? (s.notifyWith(e, [l, 1, 0]), s.resolveWith(e, [l, t])) : s.rejectWith(e, [l, t]), this
            }
        }), c = l.props;
        for (function (e, t) {
            var n, r, i, o, a;
            for (n in e) if (r = se.camelCase(n), i = t[r], o = e[n], Array.isArray(o) && (i = o[1], o = e[n] = o[0]), n !== r && (e[r] = o, delete e[n]), (a = se.cssHooks[r]) && "expand" in a) {
                o = a.expand(o), delete e[r];
                for (n in o) n in e || (e[n] = o[n], t[n] = i)
            } else t[r] = i
        }(c, l.opts.specialEasing); o < a; o++) if (r = $.prefilters[o].call(l, e, c, l.opts)) return se.isFunction(r.stop) && (se._queueHooks(l.elem, l.opts.queue).stop = se.proxy(r.stop, r)), r;
        return se.map(c, W, l), se.isFunction(l.opts.start) && l.opts.start.call(e, l), l.progress(l.opts.progress).done(l.opts.done, l.opts.complete).fail(l.opts.fail).always(l.opts.always), se.fx.timer(se.extend(u, {
            elem: e,
            anim: l,
            queue: l.opts.queue
        })), l
    }

    function B(e) {
        return (e.match(Te) || []).join(" ")
    }

    function _(e) {
        return e.getAttribute && e.getAttribute("class") || ""
    }

    function z(e, t, n, r) {
        var i;
        if (Array.isArray(t)) se.each(t, function (t, i) {
            n || xt.test(e) ? r(e, i) : z(e + "[" + ("object" == typeof i && null != i ? t : "") + "]", i, n, r)
        }); else if (n || "object" !== se.type(t)) r(e, t); else for (i in t) z(e + "[" + i + "]", t[i], n, r)
    }

    function X(e) {
        return function (t, n) {
            "string" != typeof t && (n = t, t = "*");
            var r, i = 0, o = t.toLowerCase().match(Te) || [];
            if (se.isFunction(n)) for (; r = o[i++];) "+" === r[0] ? (r = r.slice(1) || "*", (e[r] = e[r] || []).unshift(n)) : (e[r] = e[r] || []).push(n)
        }
    }

    function U(e, t, n, r) {
        function i(s) {
            var u;
            return o[s] = !0, se.each(e[s] || [], function (e, s) {
                var l = s(t, n, r);
                return "string" != typeof l || a || o[l] ? a ? !(u = l) : void 0 : (t.dataTypes.unshift(l), i(l), !1)
            }), u
        }

        var o = {}, a = e === At;
        return i(t.dataTypes[0]) || !o["*"] && i("*")
    }

    function V(e, t) {
        var n, r, i = se.ajaxSettings.flatOptions || {};
        for (n in t) void 0 !== t[n] && ((i[n] ? e : r || (r = {}))[n] = t[n]);
        return r && se.extend(!0, e, r), e
    }

    var G = [], Y = e.document, Q = Object.getPrototypeOf, J = G.slice, K = G.concat, Z = G.push, ee = G.indexOf,
        te = {}, ne = te.toString, re = te.hasOwnProperty, ie = re.toString, oe = ie.call(Object), ae = {},
        se = function (e, t) {
            return new se.fn.init(e, t)
        }, ue = /^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g, le = /^-ms-/, ce = /-([a-z])/g, fe = function (e, t) {
            return t.toUpperCase()
        };
    se.fn = se.prototype = {
        jquery: "3.2.1", constructor: se, length: 0, toArray: function () {
            return J.call(this)
        }, get: function (e) {
            return null == e ? J.call(this) : e < 0 ? this[e + this.length] : this[e]
        }, pushStack: function (e) {
            var t = se.merge(this.constructor(), e);
            return t.prevObject = this, t
        }, each: function (e) {
            return se.each(this, e)
        }, map: function (e) {
            return this.pushStack(se.map(this, function (t, n) {
                return e.call(t, n, t)
            }))
        }, slice: function () {
            return this.pushStack(J.apply(this, arguments))
        }, first: function () {
            return this.eq(0)
        }, last: function () {
            return this.eq(-1)
        }, eq: function (e) {
            var t = this.length, n = +e + (e < 0 ? t : 0);
            return this.pushStack(n >= 0 && n < t ? [this[n]] : [])
        }, end: function () {
            return this.prevObject || this.constructor()
        }, push: Z, sort: G.sort, splice: G.splice
    }, se.extend = se.fn.extend = function () {
        var e, t, n, r, i, o, a = arguments[0] || {}, s = 1, u = arguments.length, l = !1;
        for ("boolean" == typeof a && (l = a, a = arguments[s] || {}, s++), "object" == typeof a || se.isFunction(a) || (a = {}), s === u && (a = this, s--); s < u; s++) if (null != (e = arguments[s])) for (t in e) n = a[t], r = e[t], a !== r && (l && r && (se.isPlainObject(r) || (i = Array.isArray(r))) ? (i ? (i = !1, o = n && Array.isArray(n) ? n : []) : o = n && se.isPlainObject(n) ? n : {}, a[t] = se.extend(l, o, r)) : void 0 !== r && (a[t] = r));
        return a
    }, se.extend({
        expando: "jQuery" + ("3.2.1" + Math.random()).replace(/\D/g, ""), isReady: !0, error: function (e) {
            throw new Error(e)
        }, noop: function () {
        }, isFunction: function (e) {
            return "function" === se.type(e)
        }, isWindow: function (e) {
            return null != e && e === e.window
        }, isNumeric: function (e) {
            var t = se.type(e);
            return ("number" === t || "string" === t) && !isNaN(e - parseFloat(e))
        }, isPlainObject: function (e) {
            var t, n;
            return !(!e || "[object Object]" !== ne.call(e) || (t = Q(e)) && ("function" != typeof(n = re.call(t, "constructor") && t.constructor) || ie.call(n) !== oe))
        }, isEmptyObject: function (e) {
            var t;
            for (t in e) return !1;
            return !0
        }, type: function (e) {
            return null == e ? e + "" : "object" == typeof e || "function" == typeof e ? te[ne.call(e)] || "object" : typeof e
        }, globalEval: function (e) {
            n(e)
        }, camelCase: function (e) {
            return e.replace(le, "ms-").replace(ce, fe)
        }, each: function (e, t) {
            var n, i = 0;
            if (r(e)) for (n = e.length; i < n && !1 !== t.call(e[i], i, e[i]); i++) ; else for (i in e) if (!1 === t.call(e[i], i, e[i])) break;
            return e
        }, trim: function (e) {
            return null == e ? "" : (e + "").replace(ue, "")
        }, makeArray: function (e, t) {
            var n = t || [];
            return null != e && (r(Object(e)) ? se.merge(n, "string" == typeof e ? [e] : e) : Z.call(n, e)), n
        }, inArray: function (e, t, n) {
            return null == t ? -1 : ee.call(t, e, n)
        }, merge: function (e, t) {
            for (var n = +t.length, r = 0, i = e.length; r < n; r++) e[i++] = t[r];
            return e.length = i, e
        }, grep: function (e, t, n) {
            for (var r = [], i = 0, o = e.length, a = !n; i < o; i++) !t(e[i], i) !== a && r.push(e[i]);
            return r
        }, map: function (e, t, n) {
            var i, o, a = 0, s = [];
            if (r(e)) for (i = e.length; a < i; a++) null != (o = t(e[a], a, n)) && s.push(o); else for (a in e) null != (o = t(e[a], a, n)) && s.push(o);
            return K.apply([], s)
        }, guid: 1, proxy: function (e, t) {
            var n, r, i;
            if ("string" == typeof t && (n = e[t], t = e, e = n), se.isFunction(e)) return r = J.call(arguments, 2), i = function () {
                return e.apply(t || this, r.concat(J.call(arguments)))
            }, i.guid = e.guid = e.guid || se.guid++, i
        }, now: Date.now, support: ae
    }), "function" == typeof Symbol && (se.fn[Symbol.iterator] = G[Symbol.iterator]), se.each("Boolean Number String Function Array Date RegExp Object Error Symbol".split(" "), function (e, t) {
        te["[object " + t + "]"] = t.toLowerCase()
    });
    var pe = function (e) {
        function t(e, t, n, r) {
            var i, o, a, s, u, l, c, p = t && t.ownerDocument, h = t ? t.nodeType : 9;
            if (n = n || [], "string" != typeof e || !e || 1 !== h && 9 !== h && 11 !== h) return n;
            if (!r && ((t ? t.ownerDocument || t : I) !== q && A(t), t = t || q, H)) {
                if (11 !== h && (u = ge.exec(e))) if (i = u[1]) {
                    if (9 === h) {
                        if (!(a = t.getElementById(i))) return n;
                        if (a.id === i) return n.push(a), n
                    } else if (p && (a = p.getElementById(i)) && R(t, a) && a.id === i) return n.push(a), n
                } else {
                    if (u[2]) return Q.apply(n, t.getElementsByTagName(e)), n;
                    if ((i = u[3]) && b.getElementsByClassName && t.getElementsByClassName) return Q.apply(n, t.getElementsByClassName(i)), n
                }
                if (b.qsa && !z[e + " "] && (!F || !F.test(e))) {
                    if (1 !== h) p = t, c = e; else if ("object" !== t.nodeName.toLowerCase()) {
                        for ((s = t.getAttribute("id")) ? s = s.replace(xe, be) : t.setAttribute("id", s = M), o = (l = E(e)).length; o--;) l[o] = "#" + s + " " + d(l[o]);
                        c = l.join(","), p = ve.test(e) && f(t.parentNode) || t
                    }
                    if (c) try {
                        return Q.apply(n, p.querySelectorAll(c)), n
                    } catch (e) {
                    } finally {
                        s === M && t.removeAttribute("id")
                    }
                }
            }
            return S(e.replace(oe, "$1"), t, n, r)
        }

        function n() {
            function e(n, r) {
                return t.push(n + " ") > w.cacheLength && delete e[t.shift()], e[n + " "] = r
            }

            var t = [];
            return e
        }

        function r(e) {
            return e[M] = !0, e
        }

        function i(e) {
            var t = q.createElement("fieldset");
            try {
                return !!e(t)
            } catch (e) {
                return !1
            } finally {
                t.parentNode && t.parentNode.removeChild(t), t = null
            }
        }

        function o(e, t) {
            for (var n = e.split("|"), r = n.length; r--;) w.attrHandle[n[r]] = t
        }

        function a(e, t) {
            var n = t && e, r = n && 1 === e.nodeType && 1 === t.nodeType && e.sourceIndex - t.sourceIndex;
            if (r) return r;
            if (n) for (; n = n.nextSibling;) if (n === t) return -1;
            return e ? 1 : -1
        }

        function s(e) {
            return function (t) {
                return "input" === t.nodeName.toLowerCase() && t.type === e
            }
        }

        function u(e) {
            return function (t) {
                var n = t.nodeName.toLowerCase();
                return ("input" === n || "button" === n) && t.type === e
            }
        }

        function l(e) {
            return function (t) {
                return "form" in t ? t.parentNode && !1 === t.disabled ? "label" in t ? "label" in t.parentNode ? t.parentNode.disabled === e : t.disabled === e : t.isDisabled === e || t.isDisabled !== !e && Te(t) === e : t.disabled === e : "label" in t && t.disabled === e
            }
        }

        function c(e) {
            return r(function (t) {
                return t = +t, r(function (n, r) {
                    for (var i, o = e([], n.length, t), a = o.length; a--;) n[i = o[a]] && (n[i] = !(r[i] = n[i]))
                })
            })
        }

        function f(e) {
            return e && void 0 !== e.getElementsByTagName && e
        }

        function p() {
        }

        function d(e) {
            for (var t = 0, n = e.length, r = ""; t < n; t++) r += e[t].value;
            return r
        }

        function h(e, t, n) {
            var r = t.dir, i = t.next, o = i || r, a = n && "parentNode" === o, s = $++;
            return t.first ? function (t, n, i) {
                for (; t = t[r];) if (1 === t.nodeType || a) return e(t, n, i);
                return !1
            } : function (t, n, u) {
                var l, c, f, p = [W, s];
                if (u) {
                    for (; t = t[r];) if ((1 === t.nodeType || a) && e(t, n, u)) return !0
                } else for (; t = t[r];) if (1 === t.nodeType || a) if (f = t[M] || (t[M] = {}), c = f[t.uniqueID] || (f[t.uniqueID] = {}), i && i === t.nodeName.toLowerCase()) t = t[r] || t; else {
                    if ((l = c[o]) && l[0] === W && l[1] === s) return p[2] = l[2];
                    if (c[o] = p, p[2] = e(t, n, u)) return !0
                }
                return !1
            }
        }

        function g(e) {
            return e.length > 1 ? function (t, n, r) {
                for (var i = e.length; i--;) if (!e[i](t, n, r)) return !1;
                return !0
            } : e[0]
        }

        function v(e, t, n, r, i) {
            for (var o, a = [], s = 0, u = e.length, l = null != t; s < u; s++) (o = e[s]) && (n && !n(o, r, i) || (a.push(o), l && t.push(s)));
            return a
        }

        function m(e, n, i, o, a, s) {
            return o && !o[M] && (o = m(o)), a && !a[M] && (a = m(a, s)), r(function (r, s, u, l) {
                var c, f, p, d = [], h = [], g = s.length, m = r || function (e, n, r) {
                        for (var i = 0, o = n.length; i < o; i++) t(e, n[i], r);
                        return r
                    }(n || "*", u.nodeType ? [u] : u, []), y = !e || !r && n ? m : v(m, d, e, u, l),
                    x = i ? a || (r ? e : g || o) ? [] : s : y;
                if (i && i(y, x, u, l), o) for (c = v(x, h), o(c, [], u, l), f = c.length; f--;) (p = c[f]) && (x[h[f]] = !(y[h[f]] = p));
                if (r) {
                    if (a || e) {
                        if (a) {
                            for (c = [], f = x.length; f--;) (p = x[f]) && c.push(y[f] = p);
                            a(null, x = [], c, l)
                        }
                        for (f = x.length; f--;) (p = x[f]) && (c = a ? K(r, p) : d[f]) > -1 && (r[c] = !(s[c] = p))
                    }
                } else x = v(x === s ? x.splice(g, x.length) : x), a ? a(null, s, x, l) : Q.apply(s, x)
            })
        }

        function y(e) {
            for (var t, n, r, i = e.length, o = w.relative[e[0].type], a = o || w.relative[" "], s = o ? 1 : 0, u = h(function (e) {
                return e === t
            }, a, !0), l = h(function (e) {
                return K(t, e) > -1
            }, a, !0), c = [function (e, n, r) {
                var i = !o && (r || n !== N) || ((t = n).nodeType ? u(e, n, r) : l(e, n, r));
                return t = null, i
            }]; s < i; s++) if (n = w.relative[e[s].type]) c = [h(g(c), n)]; else {
                if ((n = w.filter[e[s].type].apply(null, e[s].matches))[M]) {
                    for (r = ++s; r < i && !w.relative[e[r].type]; r++) ;
                    return m(s > 1 && g(c), s > 1 && d(e.slice(0, s - 1).concat({value: " " === e[s - 2].type ? "*" : ""})).replace(oe, "$1"), n, s < r && y(e.slice(s, r)), r < i && y(e = e.slice(r)), r < i && d(e))
                }
                c.push(n)
            }
            return g(c)
        }

        var x, b, w, T, C, E, k, S, N, D, j, A, q, L, H, F, O, P, R, M = "sizzle" + 1 * new Date, I = e.document, W = 0,
            $ = 0, B = n(), _ = n(), z = n(), X = function (e, t) {
                return e === t && (j = !0), 0
            }, U = {}.hasOwnProperty, V = [], G = V.pop, Y = V.push, Q = V.push, J = V.slice, K = function (e, t) {
                for (var n = 0, r = e.length; n < r; n++) if (e[n] === t) return n;
                return -1
            },
            Z = "checked|selected|async|autofocus|autoplay|controls|defer|disabled|hidden|ismap|loop|multiple|open|readonly|required|scoped",
            ee = "[\\x20\\t\\r\\n\\f]", te = "(?:\\\\.|[\\w-]|[^\0-\\xa0])+",
            ne = "\\[" + ee + "*(" + te + ")(?:" + ee + "*([*^$|!~]?=)" + ee + "*(?:'((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\"|(" + te + "))|)" + ee + "*\\]",
            re = ":(" + te + ")(?:\\((('((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\")|((?:\\\\.|[^\\\\()[\\]]|" + ne + ")*)|.*)\\)|)",
            ie = new RegExp(ee + "+", "g"), oe = new RegExp("^" + ee + "+|((?:^|[^\\\\])(?:\\\\.)*)" + ee + "+$", "g"),
            ae = new RegExp("^" + ee + "*," + ee + "*"), se = new RegExp("^" + ee + "*([>+~]|" + ee + ")" + ee + "*"),
            ue = new RegExp("=" + ee + "*([^\\]'\"]*?)" + ee + "*\\]", "g"), le = new RegExp(re),
            ce = new RegExp("^" + te + "$"), fe = {
                ID: new RegExp("^#(" + te + ")"),
                CLASS: new RegExp("^\\.(" + te + ")"),
                TAG: new RegExp("^(" + te + "|[*])"),
                ATTR: new RegExp("^" + ne),
                PSEUDO: new RegExp("^" + re),
                CHILD: new RegExp("^:(only|first|last|nth|nth-last)-(child|of-type)(?:\\(" + ee + "*(even|odd|(([+-]|)(\\d*)n|)" + ee + "*(?:([+-]|)" + ee + "*(\\d+)|))" + ee + "*\\)|)", "i"),
                bool: new RegExp("^(?:" + Z + ")$", "i"),
                needsContext: new RegExp("^" + ee + "*[>+~]|:(even|odd|eq|gt|lt|nth|first|last)(?:\\(" + ee + "*((?:-\\d)?\\d*)" + ee + "*\\)|)(?=[^-]|$)", "i")
            }, pe = /^(?:input|select|textarea|button)$/i, de = /^h\d$/i, he = /^[^{]+\{\s*\[native \w/,
            ge = /^(?:#([\w-]+)|(\w+)|\.([\w-]+))$/, ve = /[+~]/,
            me = new RegExp("\\\\([\\da-f]{1,6}" + ee + "?|(" + ee + ")|.)", "ig"), ye = function (e, t, n) {
                var r = "0x" + t - 65536;
                return r != r || n ? t : r < 0 ? String.fromCharCode(r + 65536) : String.fromCharCode(r >> 10 | 55296, 1023 & r | 56320)
            }, xe = /([\0-\x1f\x7f]|^-?\d)|^-$|[^\0-\x1f\x7f-\uFFFF\w-]/g, be = function (e, t) {
                return t ? "\0" === e ? "�" : e.slice(0, -1) + "\\" + e.charCodeAt(e.length - 1).toString(16) + " " : "\\" + e
            }, we = function () {
                A()
            }, Te = h(function (e) {
                return !0 === e.disabled && ("form" in e || "label" in e)
            }, {dir: "parentNode", next: "legend"});
        try {
            Q.apply(V = J.call(I.childNodes), I.childNodes), V[I.childNodes.length].nodeType
        } catch (e) {
            Q = {
                apply: V.length ? function (e, t) {
                    Y.apply(e, J.call(t))
                } : function (e, t) {
                    for (var n = e.length, r = 0; e[n++] = t[r++];) ;
                    e.length = n - 1
                }
            }
        }
        b = t.support = {}, C = t.isXML = function (e) {
            var t = e && (e.ownerDocument || e).documentElement;
            return !!t && "HTML" !== t.nodeName
        }, A = t.setDocument = function (e) {
            var t, n, r = e ? e.ownerDocument || e : I;
            return r !== q && 9 === r.nodeType && r.documentElement ? (q = r, L = q.documentElement, H = !C(q), I !== q && (n = q.defaultView) && n.top !== n && (n.addEventListener ? n.addEventListener("unload", we, !1) : n.attachEvent && n.attachEvent("onunload", we)), b.attributes = i(function (e) {
                return e.className = "i", !e.getAttribute("className")
            }), b.getElementsByTagName = i(function (e) {
                return e.appendChild(q.createComment("")), !e.getElementsByTagName("*").length
            }), b.getElementsByClassName = he.test(q.getElementsByClassName), b.getById = i(function (e) {
                return L.appendChild(e).id = M, !q.getElementsByName || !q.getElementsByName(M).length
            }), b.getById ? (w.filter.ID = function (e) {
                var t = e.replace(me, ye);
                return function (e) {
                    return e.getAttribute("id") === t
                }
            }, w.find.ID = function (e, t) {
                if (void 0 !== t.getElementById && H) {
                    var n = t.getElementById(e);
                    return n ? [n] : []
                }
            }) : (w.filter.ID = function (e) {
                var t = e.replace(me, ye);
                return function (e) {
                    var n = void 0 !== e.getAttributeNode && e.getAttributeNode("id");
                    return n && n.value === t
                }
            }, w.find.ID = function (e, t) {
                if (void 0 !== t.getElementById && H) {
                    var n, r, i, o = t.getElementById(e);
                    if (o) {
                        if ((n = o.getAttributeNode("id")) && n.value === e) return [o];
                        for (i = t.getElementsByName(e), r = 0; o = i[r++];) if ((n = o.getAttributeNode("id")) && n.value === e) return [o]
                    }
                    return []
                }
            }), w.find.TAG = b.getElementsByTagName ? function (e, t) {
                return void 0 !== t.getElementsByTagName ? t.getElementsByTagName(e) : b.qsa ? t.querySelectorAll(e) : void 0
            } : function (e, t) {
                var n, r = [], i = 0, o = t.getElementsByTagName(e);
                if ("*" === e) {
                    for (; n = o[i++];) 1 === n.nodeType && r.push(n);
                    return r
                }
                return o
            }, w.find.CLASS = b.getElementsByClassName && function (e, t) {
                if (void 0 !== t.getElementsByClassName && H) return t.getElementsByClassName(e)
            }, O = [], F = [], (b.qsa = he.test(q.querySelectorAll)) && (i(function (e) {
                L.appendChild(e).innerHTML = "<a id='" + M + "'></a><select id='" + M + "-\r\\' msallowcapture=''><option selected=''></option></select>", e.querySelectorAll("[msallowcapture^='']").length && F.push("[*^$]=" + ee + "*(?:''|\"\")"), e.querySelectorAll("[selected]").length || F.push("\\[" + ee + "*(?:value|" + Z + ")"), e.querySelectorAll("[id~=" + M + "-]").length || F.push("~="), e.querySelectorAll(":checked").length || F.push(":checked"), e.querySelectorAll("a#" + M + "+*").length || F.push(".#.+[+~]")
            }), i(function (e) {
                e.innerHTML = "<a href='' disabled='disabled'></a><select disabled='disabled'><option/></select>";
                var t = q.createElement("input");
                t.setAttribute("type", "hidden"), e.appendChild(t).setAttribute("name", "D"), e.querySelectorAll("[name=d]").length && F.push("name" + ee + "*[*^$|!~]?="), 2 !== e.querySelectorAll(":enabled").length && F.push(":enabled", ":disabled"), L.appendChild(e).disabled = !0, 2 !== e.querySelectorAll(":disabled").length && F.push(":enabled", ":disabled"), e.querySelectorAll("*,:x"), F.push(",.*:")
            })), (b.matchesSelector = he.test(P = L.matches || L.webkitMatchesSelector || L.mozMatchesSelector || L.oMatchesSelector || L.msMatchesSelector)) && i(function (e) {
                b.disconnectedMatch = P.call(e, "*"), P.call(e, "[s!='']:x"), O.push("!=", re)
            }), F = F.length && new RegExp(F.join("|")), O = O.length && new RegExp(O.join("|")), t = he.test(L.compareDocumentPosition), R = t || he.test(L.contains) ? function (e, t) {
                var n = 9 === e.nodeType ? e.documentElement : e, r = t && t.parentNode;
                return e === r || !(!r || 1 !== r.nodeType || !(n.contains ? n.contains(r) : e.compareDocumentPosition && 16 & e.compareDocumentPosition(r)))
            } : function (e, t) {
                if (t) for (; t = t.parentNode;) if (t === e) return !0;
                return !1
            }, X = t ? function (e, t) {
                if (e === t) return j = !0, 0;
                var n = !e.compareDocumentPosition - !t.compareDocumentPosition;
                return n || (1 & (n = (e.ownerDocument || e) === (t.ownerDocument || t) ? e.compareDocumentPosition(t) : 1) || !b.sortDetached && t.compareDocumentPosition(e) === n ? e === q || e.ownerDocument === I && R(I, e) ? -1 : t === q || t.ownerDocument === I && R(I, t) ? 1 : D ? K(D, e) - K(D, t) : 0 : 4 & n ? -1 : 1)
            } : function (e, t) {
                if (e === t) return j = !0, 0;
                var n, r = 0, i = e.parentNode, o = t.parentNode, s = [e], u = [t];
                if (!i || !o) return e === q ? -1 : t === q ? 1 : i ? -1 : o ? 1 : D ? K(D, e) - K(D, t) : 0;
                if (i === o) return a(e, t);
                for (n = e; n = n.parentNode;) s.unshift(n);
                for (n = t; n = n.parentNode;) u.unshift(n);
                for (; s[r] === u[r];) r++;
                return r ? a(s[r], u[r]) : s[r] === I ? -1 : u[r] === I ? 1 : 0
            }, q) : q
        }, t.matches = function (e, n) {
            return t(e, null, null, n)
        }, t.matchesSelector = function (e, n) {
            if ((e.ownerDocument || e) !== q && A(e), n = n.replace(ue, "='$1']"), b.matchesSelector && H && !z[n + " "] && (!O || !O.test(n)) && (!F || !F.test(n))) try {
                var r = P.call(e, n);
                if (r || b.disconnectedMatch || e.document && 11 !== e.document.nodeType) return r
            } catch (e) {
            }
            return t(n, q, null, [e]).length > 0
        }, t.contains = function (e, t) {
            return (e.ownerDocument || e) !== q && A(e), R(e, t)
        }, t.attr = function (e, t) {
            (e.ownerDocument || e) !== q && A(e);
            var n = w.attrHandle[t.toLowerCase()],
                r = n && U.call(w.attrHandle, t.toLowerCase()) ? n(e, t, !H) : void 0;
            return void 0 !== r ? r : b.attributes || !H ? e.getAttribute(t) : (r = e.getAttributeNode(t)) && r.specified ? r.value : null
        }, t.escape = function (e) {
            return (e + "").replace(xe, be)
        }, t.error = function (e) {
            throw new Error("Syntax error, unrecognized expression: " + e)
        }, t.uniqueSort = function (e) {
            var t, n = [], r = 0, i = 0;
            if (j = !b.detectDuplicates, D = !b.sortStable && e.slice(0), e.sort(X), j) {
                for (; t = e[i++];) t === e[i] && (r = n.push(i));
                for (; r--;) e.splice(n[r], 1)
            }
            return D = null, e
        }, T = t.getText = function (e) {
            var t, n = "", r = 0, i = e.nodeType;
            if (i) {
                if (1 === i || 9 === i || 11 === i) {
                    if ("string" == typeof e.textContent) return e.textContent;
                    for (e = e.firstChild; e; e = e.nextSibling) n += T(e)
                } else if (3 === i || 4 === i) return e.nodeValue
            } else for (; t = e[r++];) n += T(t);
            return n
        }, (w = t.selectors = {
            cacheLength: 50,
            createPseudo: r,
            match: fe,
            attrHandle: {},
            find: {},
            relative: {
                ">": {dir: "parentNode", first: !0},
                " ": {dir: "parentNode"},
                "+": {dir: "previousSibling", first: !0},
                "~": {dir: "previousSibling"}
            },
            preFilter: {
                ATTR: function (e) {
                    return e[1] = e[1].replace(me, ye), e[3] = (e[3] || e[4] || e[5] || "").replace(me, ye), "~=" === e[2] && (e[3] = " " + e[3] + " "), e.slice(0, 4)
                }, CHILD: function (e) {
                    return e[1] = e[1].toLowerCase(), "nth" === e[1].slice(0, 3) ? (e[3] || t.error(e[0]), e[4] = +(e[4] ? e[5] + (e[6] || 1) : 2 * ("even" === e[3] || "odd" === e[3])), e[5] = +(e[7] + e[8] || "odd" === e[3])) : e[3] && t.error(e[0]), e
                }, PSEUDO: function (e) {
                    var t, n = !e[6] && e[2];
                    return fe.CHILD.test(e[0]) ? null : (e[3] ? e[2] = e[4] || e[5] || "" : n && le.test(n) && (t = E(n, !0)) && (t = n.indexOf(")", n.length - t) - n.length) && (e[0] = e[0].slice(0, t), e[2] = n.slice(0, t)), e.slice(0, 3))
                }
            },
            filter: {
                TAG: function (e) {
                    var t = e.replace(me, ye).toLowerCase();
                    return "*" === e ? function () {
                        return !0
                    } : function (e) {
                        return e.nodeName && e.nodeName.toLowerCase() === t
                    }
                }, CLASS: function (e) {
                    var t = B[e + " "];
                    return t || (t = new RegExp("(^|" + ee + ")" + e + "(" + ee + "|$)")) && B(e, function (e) {
                        return t.test("string" == typeof e.className && e.className || void 0 !== e.getAttribute && e.getAttribute("class") || "")
                    })
                }, ATTR: function (e, n, r) {
                    return function (i) {
                        var o = t.attr(i, e);
                        return null == o ? "!=" === n : !n || (o += "", "=" === n ? o === r : "!=" === n ? o !== r : "^=" === n ? r && 0 === o.indexOf(r) : "*=" === n ? r && o.indexOf(r) > -1 : "$=" === n ? r && o.slice(-r.length) === r : "~=" === n ? (" " + o.replace(ie, " ") + " ").indexOf(r) > -1 : "|=" === n && (o === r || o.slice(0, r.length + 1) === r + "-"))
                    }
                }, CHILD: function (e, t, n, r, i) {
                    var o = "nth" !== e.slice(0, 3), a = "last" !== e.slice(-4), s = "of-type" === t;
                    return 1 === r && 0 === i ? function (e) {
                        return !!e.parentNode
                    } : function (t, n, u) {
                        var l, c, f, p, d, h, g = o !== a ? "nextSibling" : "previousSibling", v = t.parentNode,
                            m = s && t.nodeName.toLowerCase(), y = !u && !s, x = !1;
                        if (v) {
                            if (o) {
                                for (; g;) {
                                    for (p = t; p = p[g];) if (s ? p.nodeName.toLowerCase() === m : 1 === p.nodeType) return !1;
                                    h = g = "only" === e && !h && "nextSibling"
                                }
                                return !0
                            }
                            if (h = [a ? v.firstChild : v.lastChild], a && y) {
                                for (x = (d = (l = (c = (f = (p = v)[M] || (p[M] = {}))[p.uniqueID] || (f[p.uniqueID] = {}))[e] || [])[0] === W && l[1]) && l[2], p = d && v.childNodes[d]; p = ++d && p && p[g] || (x = d = 0) || h.pop();) if (1 === p.nodeType && ++x && p === t) {
                                    c[e] = [W, d, x];
                                    break
                                }
                            } else if (y && (p = t, f = p[M] || (p[M] = {}), c = f[p.uniqueID] || (f[p.uniqueID] = {}), l = c[e] || [], d = l[0] === W && l[1], x = d), !1 === x) for (; (p = ++d && p && p[g] || (x = d = 0) || h.pop()) && ((s ? p.nodeName.toLowerCase() !== m : 1 !== p.nodeType) || !++x || (y && (f = p[M] || (p[M] = {}), c = f[p.uniqueID] || (f[p.uniqueID] = {}), c[e] = [W, x]), p !== t));) ;
                            return (x -= i) === r || x % r == 0 && x / r >= 0
                        }
                    }
                }, PSEUDO: function (e, n) {
                    var i, o = w.pseudos[e] || w.setFilters[e.toLowerCase()] || t.error("unsupported pseudo: " + e);
                    return o[M] ? o(n) : o.length > 1 ? (i = [e, e, "", n], w.setFilters.hasOwnProperty(e.toLowerCase()) ? r(function (e, t) {
                        for (var r, i = o(e, n), a = i.length; a--;) r = K(e, i[a]), e[r] = !(t[r] = i[a])
                    }) : function (e) {
                        return o(e, 0, i)
                    }) : o
                }
            },
            pseudos: {
                not: r(function (e) {
                    var t = [], n = [], i = k(e.replace(oe, "$1"));
                    return i[M] ? r(function (e, t, n, r) {
                        for (var o, a = i(e, null, r, []), s = e.length; s--;) (o = a[s]) && (e[s] = !(t[s] = o))
                    }) : function (e, r, o) {
                        return t[0] = e, i(t, null, o, n), t[0] = null, !n.pop()
                    }
                }), has: r(function (e) {
                    return function (n) {
                        return t(e, n).length > 0
                    }
                }), contains: r(function (e) {
                    return e = e.replace(me, ye), function (t) {
                        return (t.textContent || t.innerText || T(t)).indexOf(e) > -1
                    }
                }), lang: r(function (e) {
                    return ce.test(e || "") || t.error("unsupported lang: " + e), e = e.replace(me, ye).toLowerCase(), function (t) {
                        var n;
                        do {
                            if (n = H ? t.lang : t.getAttribute("xml:lang") || t.getAttribute("lang")) return (n = n.toLowerCase()) === e || 0 === n.indexOf(e + "-")
                        } while ((t = t.parentNode) && 1 === t.nodeType);
                        return !1
                    }
                }), target: function (t) {
                    var n = e.location && e.location.hash;
                    return n && n.slice(1) === t.id
                }, root: function (e) {
                    return e === L
                }, focus: function (e) {
                    return e === q.activeElement && (!q.hasFocus || q.hasFocus()) && !!(e.type || e.href || ~e.tabIndex)
                }, enabled: l(!1), disabled: l(!0), checked: function (e) {
                    var t = e.nodeName.toLowerCase();
                    return "input" === t && !!e.checked || "option" === t && !!e.selected
                }, selected: function (e) {
                    return e.parentNode && e.parentNode.selectedIndex, !0 === e.selected
                }, empty: function (e) {
                    for (e = e.firstChild; e; e = e.nextSibling) if (e.nodeType < 6) return !1;
                    return !0
                }, parent: function (e) {
                    return !w.pseudos.empty(e)
                }, header: function (e) {
                    return de.test(e.nodeName)
                }, input: function (e) {
                    return pe.test(e.nodeName)
                }, button: function (e) {
                    var t = e.nodeName.toLowerCase();
                    return "input" === t && "button" === e.type || "button" === t
                }, text: function (e) {
                    var t;
                    return "input" === e.nodeName.toLowerCase() && "text" === e.type && (null == (t = e.getAttribute("type")) || "text" === t.toLowerCase())
                }, first: c(function () {
                    return [0]
                }), last: c(function (e, t) {
                    return [t - 1]
                }), eq: c(function (e, t, n) {
                    return [n < 0 ? n + t : n]
                }), even: c(function (e, t) {
                    for (var n = 0; n < t; n += 2) e.push(n);
                    return e
                }), odd: c(function (e, t) {
                    for (var n = 1; n < t; n += 2) e.push(n);
                    return e
                }), lt: c(function (e, t, n) {
                    for (var r = n < 0 ? n + t : n; --r >= 0;) e.push(r);
                    return e
                }), gt: c(function (e, t, n) {
                    for (var r = n < 0 ? n + t : n; ++r < t;) e.push(r);
                    return e
                })
            }
        }).pseudos.nth = w.pseudos.eq;
        for (x in{radio: !0, checkbox: !0, file: !0, password: !0, image: !0}) w.pseudos[x] = s(x);
        for (x in{submit: !0, reset: !0}) w.pseudos[x] = u(x);
        return p.prototype = w.filters = w.pseudos, w.setFilters = new p, E = t.tokenize = function (e, n) {
            var r, i, o, a, s, u, l, c = _[e + " "];
            if (c) return n ? 0 : c.slice(0);
            for (s = e, u = [], l = w.preFilter; s;) {
                r && !(i = ae.exec(s)) || (i && (s = s.slice(i[0].length) || s), u.push(o = [])), r = !1, (i = se.exec(s)) && (r = i.shift(), o.push({
                    value: r,
                    type: i[0].replace(oe, " ")
                }), s = s.slice(r.length));
                for (a in w.filter) !(i = fe[a].exec(s)) || l[a] && !(i = l[a](i)) || (r = i.shift(), o.push({
                    value: r,
                    type: a,
                    matches: i
                }), s = s.slice(r.length));
                if (!r) break
            }
            return n ? s.length : s ? t.error(e) : _(e, u).slice(0)
        }, k = t.compile = function (e, n) {
            var i, o = [], a = [], s = z[e + " "];
            if (!s) {
                for (n || (n = E(e)), i = n.length; i--;) (s = y(n[i]))[M] ? o.push(s) : a.push(s);
                (s = z(e, function (e, n) {
                    var i = n.length > 0, o = e.length > 0, a = function (r, a, s, u, l) {
                        var c, f, p, d = 0, h = "0", g = r && [], m = [], y = N, x = r || o && w.find.TAG("*", l),
                            b = W += null == y ? 1 : Math.random() || .1, T = x.length;
                        for (l && (N = a === q || a || l); h !== T && null != (c = x[h]); h++) {
                            if (o && c) {
                                for (f = 0, a || c.ownerDocument === q || (A(c), s = !H); p = e[f++];) if (p(c, a || q, s)) {
                                    u.push(c);
                                    break
                                }
                                l && (W = b)
                            }
                            i && ((c = !p && c) && d--, r && g.push(c))
                        }
                        if (d += h, i && h !== d) {
                            for (f = 0; p = n[f++];) p(g, m, a, s);
                            if (r) {
                                if (d > 0) for (; h--;) g[h] || m[h] || (m[h] = G.call(u));
                                m = v(m)
                            }
                            Q.apply(u, m), l && !r && m.length > 0 && d + n.length > 1 && t.uniqueSort(u)
                        }
                        return l && (W = b, N = y), g
                    };
                    return i ? r(a) : a
                }(a, o))).selector = e
            }
            return s
        }, S = t.select = function (e, t, n, r) {
            var i, o, a, s, u, l = "function" == typeof e && e, c = !r && E(e = l.selector || e);
            if (n = n || [], 1 === c.length) {
                if ((o = c[0] = c[0].slice(0)).length > 2 && "ID" === (a = o[0]).type && 9 === t.nodeType && H && w.relative[o[1].type]) {
                    if (!(t = (w.find.ID(a.matches[0].replace(me, ye), t) || [])[0])) return n;
                    l && (t = t.parentNode), e = e.slice(o.shift().value.length)
                }
                for (i = fe.needsContext.test(e) ? 0 : o.length; i-- && (a = o[i], !w.relative[s = a.type]);) if ((u = w.find[s]) && (r = u(a.matches[0].replace(me, ye), ve.test(o[0].type) && f(t.parentNode) || t))) {
                    if (o.splice(i, 1), !(e = r.length && d(o))) return Q.apply(n, r), n;
                    break
                }
            }
            return (l || k(e, c))(r, t, !H, n, !t || ve.test(e) && f(t.parentNode) || t), n
        }, b.sortStable = M.split("").sort(X).join("") === M, b.detectDuplicates = !!j, A(), b.sortDetached = i(function (e) {
            return 1 & e.compareDocumentPosition(q.createElement("fieldset"))
        }), i(function (e) {
            return e.innerHTML = "<a href='#'></a>", "#" === e.firstChild.getAttribute("href")
        }) || o("type|href|height|width", function (e, t, n) {
            if (!n) return e.getAttribute(t, "type" === t.toLowerCase() ? 1 : 2)
        }), b.attributes && i(function (e) {
            return e.innerHTML = "<input/>", e.firstChild.setAttribute("value", ""), "" === e.firstChild.getAttribute("value")
        }) || o("value", function (e, t, n) {
            if (!n && "input" === e.nodeName.toLowerCase()) return e.defaultValue
        }), i(function (e) {
            return null == e.getAttribute("disabled")
        }) || o(Z, function (e, t, n) {
            var r;
            if (!n) return !0 === e[t] ? t.toLowerCase() : (r = e.getAttributeNode(t)) && r.specified ? r.value : null
        }), t
    }(e);
    se.find = pe, se.expr = pe.selectors, se.expr[":"] = se.expr.pseudos, se.uniqueSort = se.unique = pe.uniqueSort, se.text = pe.getText, se.isXMLDoc = pe.isXML, se.contains = pe.contains, se.escapeSelector = pe.escape;
    var de = function (e, t, n) {
            for (var r = [], i = void 0 !== n; (e = e[t]) && 9 !== e.nodeType;) if (1 === e.nodeType) {
                if (i && se(e).is(n)) break;
                r.push(e)
            }
            return r
        }, he = function (e, t) {
            for (var n = []; e; e = e.nextSibling) 1 === e.nodeType && e !== t && n.push(e);
            return n
        }, ge = se.expr.match.needsContext, ve = /^<([a-z][^\/\0>:\x20\t\r\n\f]*)[\x20\t\r\n\f]*\/?>(?:<\/\1>|)$/i,
        me = /^.[^:#\[\.,]*$/;
    se.filter = function (e, t, n) {
        var r = t[0];
        return n && (e = ":not(" + e + ")"), 1 === t.length && 1 === r.nodeType ? se.find.matchesSelector(r, e) ? [r] : [] : se.find.matches(e, se.grep(t, function (e) {
            return 1 === e.nodeType
        }))
    }, se.fn.extend({
        find: function (e) {
            var t, n, r = this.length, i = this;
            if ("string" != typeof e) return this.pushStack(se(e).filter(function () {
                for (t = 0; t < r; t++) if (se.contains(i[t], this)) return !0
            }));
            for (n = this.pushStack([]), t = 0; t < r; t++) se.find(e, i[t], n);
            return r > 1 ? se.uniqueSort(n) : n
        }, filter: function (e) {
            return this.pushStack(o(this, e || [], !1))
        }, not: function (e) {
            return this.pushStack(o(this, e || [], !0))
        }, is: function (e) {
            return !!o(this, "string" == typeof e && ge.test(e) ? se(e) : e || [], !1).length
        }
    });
    var ye, xe = /^(?:\s*(<[\w\W]+>)[^>]*|#([\w-]+))$/;
    (se.fn.init = function (e, t, n) {
        var r, i;
        if (!e) return this;
        if (n = n || ye, "string" == typeof e) {
            if (!(r = "<" === e[0] && ">" === e[e.length - 1] && e.length >= 3 ? [null, e, null] : xe.exec(e)) || !r[1] && t) return !t || t.jquery ? (t || n).find(e) : this.constructor(t).find(e);
            if (r[1]) {
                if (t = t instanceof se ? t[0] : t, se.merge(this, se.parseHTML(r[1], t && t.nodeType ? t.ownerDocument || t : Y, !0)), ve.test(r[1]) && se.isPlainObject(t)) for (r in t) se.isFunction(this[r]) ? this[r](t[r]) : this.attr(r, t[r]);
                return this
            }
            return (i = Y.getElementById(r[2])) && (this[0] = i, this.length = 1), this
        }
        return e.nodeType ? (this[0] = e, this.length = 1, this) : se.isFunction(e) ? void 0 !== n.ready ? n.ready(e) : e(se) : se.makeArray(e, this)
    }).prototype = se.fn, ye = se(Y);
    var be = /^(?:parents|prev(?:Until|All))/, we = {children: !0, contents: !0, next: !0, prev: !0};
    se.fn.extend({
        has: function (e) {
            var t = se(e, this), n = t.length;
            return this.filter(function () {
                for (var e = 0; e < n; e++) if (se.contains(this, t[e])) return !0
            })
        }, closest: function (e, t) {
            var n, r = 0, i = this.length, o = [], a = "string" != typeof e && se(e);
            if (!ge.test(e)) for (; r < i; r++) for (n = this[r]; n && n !== t; n = n.parentNode) if (n.nodeType < 11 && (a ? a.index(n) > -1 : 1 === n.nodeType && se.find.matchesSelector(n, e))) {
                o.push(n);
                break
            }
            return this.pushStack(o.length > 1 ? se.uniqueSort(o) : o)
        }, index: function (e) {
            return e ? "string" == typeof e ? ee.call(se(e), this[0]) : ee.call(this, e.jquery ? e[0] : e) : this[0] && this[0].parentNode ? this.first().prevAll().length : -1
        }, add: function (e, t) {
            return this.pushStack(se.uniqueSort(se.merge(this.get(), se(e, t))))
        }, addBack: function (e) {
            return this.add(null == e ? this.prevObject : this.prevObject.filter(e))
        }
    }), se.each({
        parent: function (e) {
            var t = e.parentNode;
            return t && 11 !== t.nodeType ? t : null
        }, parents: function (e) {
            return de(e, "parentNode")
        }, parentsUntil: function (e, t, n) {
            return de(e, "parentNode", n)
        }, next: function (e) {
            return a(e, "nextSibling")
        }, prev: function (e) {
            return a(e, "previousSibling")
        }, nextAll: function (e) {
            return de(e, "nextSibling")
        }, prevAll: function (e) {
            return de(e, "previousSibling")
        }, nextUntil: function (e, t, n) {
            return de(e, "nextSibling", n)
        }, prevUntil: function (e, t, n) {
            return de(e, "previousSibling", n)
        }, siblings: function (e) {
            return he((e.parentNode || {}).firstChild, e)
        }, children: function (e) {
            return he(e.firstChild)
        }, contents: function (e) {
            return i(e, "iframe") ? e.contentDocument : (i(e, "template") && (e = e.content || e), se.merge([], e.childNodes))
        }
    }, function (e, t) {
        se.fn[e] = function (n, r) {
            var i = se.map(this, t, n);
            return "Until" !== e.slice(-5) && (r = n), r && "string" == typeof r && (i = se.filter(r, i)), this.length > 1 && (we[e] || se.uniqueSort(i), be.test(e) && i.reverse()), this.pushStack(i)
        }
    });
    var Te = /[^\x20\t\r\n\f]+/g;
    se.Callbacks = function (e) {
        e = "string" == typeof e ? function (e) {
            var t = {};
            return se.each(e.match(Te) || [], function (e, n) {
                t[n] = !0
            }), t
        }(e) : se.extend({}, e);
        var t, n, r, i, o = [], a = [], s = -1, u = function () {
            for (i = i || e.once, r = t = !0; a.length; s = -1) for (n = a.shift(); ++s < o.length;) !1 === o[s].apply(n[0], n[1]) && e.stopOnFalse && (s = o.length, n = !1);
            e.memory || (n = !1), t = !1, i && (o = n ? [] : "")
        }, l = {
            add: function () {
                return o && (n && !t && (s = o.length - 1, a.push(n)), function t(n) {
                    se.each(n, function (n, r) {
                        se.isFunction(r) ? e.unique && l.has(r) || o.push(r) : r && r.length && "string" !== se.type(r) && t(r)
                    })
                }(arguments), n && !t && u()), this
            }, remove: function () {
                return se.each(arguments, function (e, t) {
                    for (var n; (n = se.inArray(t, o, n)) > -1;) o.splice(n, 1), n <= s && s--
                }), this
            }, has: function (e) {
                return e ? se.inArray(e, o) > -1 : o.length > 0
            }, empty: function () {
                return o && (o = []), this
            }, disable: function () {
                return i = a = [], o = n = "", this
            }, disabled: function () {
                return !o
            }, lock: function () {
                return i = a = [], n || t || (o = n = ""), this
            }, locked: function () {
                return !!i
            }, fireWith: function (e, n) {
                return i || (n = n || [], n = [e, n.slice ? n.slice() : n], a.push(n), t || u()), this
            }, fire: function () {
                return l.fireWith(this, arguments), this
            }, fired: function () {
                return !!r
            }
        };
        return l
    }, se.extend({
        Deferred: function (t) {
            var n = [["notify", "progress", se.Callbacks("memory"), se.Callbacks("memory"), 2], ["resolve", "done", se.Callbacks("once memory"), se.Callbacks("once memory"), 0, "resolved"], ["reject", "fail", se.Callbacks("once memory"), se.Callbacks("once memory"), 1, "rejected"]],
                r = "pending", i = {
                    state: function () {
                        return r
                    }, always: function () {
                        return o.done(arguments).fail(arguments), this
                    }, catch: function (e) {
                        return i.then(null, e)
                    }, pipe: function () {
                        var e = arguments;
                        return se.Deferred(function (t) {
                            se.each(n, function (n, r) {
                                var i = se.isFunction(e[r[4]]) && e[r[4]];
                                o[r[1]](function () {
                                    var e = i && i.apply(this, arguments);
                                    e && se.isFunction(e.promise) ? e.promise().progress(t.notify).done(t.resolve).fail(t.reject) : t[r[0] + "With"](this, i ? [e] : arguments)
                                })
                            }), e = null
                        }).promise()
                    }, then: function (t, r, i) {
                        function o(t, n, r, i) {
                            return function () {
                                var l = this, c = arguments, f = function () {
                                    var e, f;
                                    if (!(t < a)) {
                                        if ((e = r.apply(l, c)) === n.promise()) throw new TypeError("Thenable self-resolution");
                                        f = e && ("object" == typeof e || "function" == typeof e) && e.then, se.isFunction(f) ? i ? f.call(e, o(a, n, s, i), o(a, n, u, i)) : (a++, f.call(e, o(a, n, s, i), o(a, n, u, i), o(a, n, s, n.notifyWith))) : (r !== s && (l = void 0, c = [e]), (i || n.resolveWith)(l, c))
                                    }
                                }, p = i ? f : function () {
                                    try {
                                        f()
                                    } catch (e) {
                                        se.Deferred.exceptionHook && se.Deferred.exceptionHook(e, p.stackTrace), t + 1 >= a && (r !== u && (l = void 0, c = [e]), n.rejectWith(l, c))
                                    }
                                };
                                t ? p() : (se.Deferred.getStackHook && (p.stackTrace = se.Deferred.getStackHook()), e.setTimeout(p))
                            }
                        }

                        var a = 0;
                        return se.Deferred(function (e) {
                            n[0][3].add(o(0, e, se.isFunction(i) ? i : s, e.notifyWith)), n[1][3].add(o(0, e, se.isFunction(t) ? t : s)), n[2][3].add(o(0, e, se.isFunction(r) ? r : u))
                        }).promise()
                    }, promise: function (e) {
                        return null != e ? se.extend(e, i) : i
                    }
                }, o = {};
            return se.each(n, function (e, t) {
                var a = t[2], s = t[5];
                i[t[1]] = a.add, s && a.add(function () {
                    r = s
                }, n[3 - e][2].disable, n[0][2].lock), a.add(t[3].fire), o[t[0]] = function () {
                    return o[t[0] + "With"](this === o ? void 0 : this, arguments), this
                }, o[t[0] + "With"] = a.fireWith
            }), i.promise(o), t && t.call(o, o), o
        }, when: function (e) {
            var t = arguments.length, n = t, r = Array(n), i = J.call(arguments), o = se.Deferred(), a = function (e) {
                return function (n) {
                    r[e] = this, i[e] = arguments.length > 1 ? J.call(arguments) : n, --t || o.resolveWith(r, i)
                }
            };
            if (t <= 1 && (l(e, o.done(a(n)).resolve, o.reject, !t), "pending" === o.state() || se.isFunction(i[n] && i[n].then))) return o.then();
            for (; n--;) l(i[n], a(n), o.reject);
            return o.promise()
        }
    });
    var Ce = /^(Eval|Internal|Range|Reference|Syntax|Type|URI)Error$/;
    se.Deferred.exceptionHook = function (t, n) {
        e.console && e.console.warn && t && Ce.test(t.name) && e.console.warn("jQuery.Deferred exception: " + t.message, t.stack, n)
    }, se.readyException = function (t) {
        e.setTimeout(function () {
            throw t
        })
    };
    var Ee = se.Deferred();
    se.fn.ready = function (e) {
        return Ee.then(e).catch(function (e) {
            se.readyException(e)
        }), this
    }, se.extend({
        isReady: !1, readyWait: 1, ready: function (e) {
            (!0 === e ? --se.readyWait : se.isReady) || (se.isReady = !0, !0 !== e && --se.readyWait > 0 || Ee.resolveWith(Y, [se]))
        }
    }), se.ready.then = Ee.then, "complete" === Y.readyState || "loading" !== Y.readyState && !Y.documentElement.doScroll ? e.setTimeout(se.ready) : (Y.addEventListener("DOMContentLoaded", c), e.addEventListener("load", c));
    var ke = function (e, t, n, r, i, o, a) {
        var s = 0, u = e.length, l = null == n;
        if ("object" === se.type(n)) {
            i = !0;
            for (s in n) ke(e, t, s, n[s], !0, o, a)
        } else if (void 0 !== r && (i = !0, se.isFunction(r) || (a = !0), l && (a ? (t.call(e, r), t = null) : (l = t, t = function (e, t, n) {
                return l.call(se(e), n)
            })), t)) for (; s < u; s++) t(e[s], n, a ? r : r.call(e[s], s, t(e[s], n)));
        return i ? e : l ? t.call(e) : u ? t(e[0], n) : o
    }, Se = function (e) {
        return 1 === e.nodeType || 9 === e.nodeType || !+e.nodeType
    };
    f.uid = 1, f.prototype = {
        cache: function (e) {
            var t = e[this.expando];
            return t || (t = {}, Se(e) && (e.nodeType ? e[this.expando] = t : Object.defineProperty(e, this.expando, {
                value: t,
                configurable: !0
            }))), t
        }, set: function (e, t, n) {
            var r, i = this.cache(e);
            if ("string" == typeof t) i[se.camelCase(t)] = n; else for (r in t) i[se.camelCase(r)] = t[r];
            return i
        }, get: function (e, t) {
            return void 0 === t ? this.cache(e) : e[this.expando] && e[this.expando][se.camelCase(t)]
        }, access: function (e, t, n) {
            return void 0 === t || t && "string" == typeof t && void 0 === n ? this.get(e, t) : (this.set(e, t, n), void 0 !== n ? n : t)
        }, remove: function (e, t) {
            var n, r = e[this.expando];
            if (void 0 !== r) {
                if (void 0 !== t) {
                    Array.isArray(t) ? t = t.map(se.camelCase) : (t = se.camelCase(t), t = t in r ? [t] : t.match(Te) || []), n = t.length;
                    for (; n--;) delete r[t[n]]
                }
                (void 0 === t || se.isEmptyObject(r)) && (e.nodeType ? e[this.expando] = void 0 : delete e[this.expando])
            }
        }, hasData: function (e) {
            var t = e[this.expando];
            return void 0 !== t && !se.isEmptyObject(t)
        }
    };
    var Ne = new f, De = new f, je = /^(?:\{[\w\W]*\}|\[[\w\W]*\])$/, Ae = /[A-Z]/g;
    se.extend({
        hasData: function (e) {
            return De.hasData(e) || Ne.hasData(e)
        }, data: function (e, t, n) {
            return De.access(e, t, n)
        }, removeData: function (e, t) {
            De.remove(e, t)
        }, _data: function (e, t, n) {
            return Ne.access(e, t, n)
        }, _removeData: function (e, t) {
            Ne.remove(e, t)
        }
    }), se.fn.extend({
        data: function (e, t) {
            var n, r, i, o = this[0], a = o && o.attributes;
            if (void 0 === e) {
                if (this.length && (i = De.get(o), 1 === o.nodeType && !Ne.get(o, "hasDataAttrs"))) {
                    for (n = a.length; n--;) a[n] && 0 === (r = a[n].name).indexOf("data-") && (r = se.camelCase(r.slice(5)), p(o, r, i[r]));
                    Ne.set(o, "hasDataAttrs", !0)
                }
                return i
            }
            return "object" == typeof e ? this.each(function () {
                De.set(this, e)
            }) : ke(this, function (t) {
                var n;
                if (o && void 0 === t) {
                    if (void 0 !== (n = De.get(o, e))) return n;
                    if (void 0 !== (n = p(o, e))) return n
                } else this.each(function () {
                    De.set(this, e, t)
                })
            }, null, t, arguments.length > 1, null, !0)
        }, removeData: function (e) {
            return this.each(function () {
                De.remove(this, e)
            })
        }
    }), se.extend({
        queue: function (e, t, n) {
            var r;
            if (e) return t = (t || "fx") + "queue", r = Ne.get(e, t), n && (!r || Array.isArray(n) ? r = Ne.access(e, t, se.makeArray(n)) : r.push(n)), r || []
        }, dequeue: function (e, t) {
            t = t || "fx";
            var n = se.queue(e, t), r = n.length, i = n.shift(), o = se._queueHooks(e, t);
            "inprogress" === i && (i = n.shift(), r--), i && ("fx" === t && n.unshift("inprogress"), delete o.stop, i.call(e, function () {
                se.dequeue(e, t)
            }, o)), !r && o && o.empty.fire()
        }, _queueHooks: function (e, t) {
            var n = t + "queueHooks";
            return Ne.get(e, n) || Ne.access(e, n, {
                empty: se.Callbacks("once memory").add(function () {
                    Ne.remove(e, [t + "queue", n])
                })
            })
        }
    }), se.fn.extend({
        queue: function (e, t) {
            var n = 2;
            return "string" != typeof e && (t = e, e = "fx", n--), arguments.length < n ? se.queue(this[0], e) : void 0 === t ? this : this.each(function () {
                var n = se.queue(this, e, t);
                se._queueHooks(this, e), "fx" === e && "inprogress" !== n[0] && se.dequeue(this, e)
            })
        }, dequeue: function (e) {
            return this.each(function () {
                se.dequeue(this, e)
            })
        }, clearQueue: function (e) {
            return this.queue(e || "fx", [])
        }, promise: function (e, t) {
            var n, r = 1, i = se.Deferred(), o = this, a = this.length, s = function () {
                --r || i.resolveWith(o, [o])
            };
            for ("string" != typeof e && (t = e, e = void 0), e = e || "fx"; a--;) (n = Ne.get(o[a], e + "queueHooks")) && n.empty && (r++, n.empty.add(s));
            return s(), i.promise(t)
        }
    });
    var qe = /[+-]?(?:\d*\.|)\d+(?:[eE][+-]?\d+|)/.source, Le = new RegExp("^(?:([+-])=|)(" + qe + ")([a-z%]*)$", "i"),
        He = ["Top", "Right", "Bottom", "Left"], Fe = function (e, t) {
            return "none" === (e = t || e).style.display || "" === e.style.display && se.contains(e.ownerDocument, e) && "none" === se.css(e, "display")
        }, Oe = function (e, t, n, r) {
            var i, o, a = {};
            for (o in t) a[o] = e.style[o], e.style[o] = t[o];
            i = n.apply(e, r || []);
            for (o in t) e.style[o] = a[o];
            return i
        }, Pe = {};
    se.fn.extend({
        show: function () {
            return g(this, !0)
        }, hide: function () {
            return g(this)
        }, toggle: function (e) {
            return "boolean" == typeof e ? e ? this.show() : this.hide() : this.each(function () {
                Fe(this) ? se(this).show() : se(this).hide()
            })
        }
    });
    var Re = /^(?:checkbox|radio)$/i, Me = /<([a-z][^\/\0>\x20\t\r\n\f]+)/i, Ie = /^$|\/(?:java|ecma)script/i, We = {
        option: [1, "<select multiple='multiple'>", "</select>"],
        thead: [1, "<table>", "</table>"],
        col: [2, "<table><colgroup>", "</colgroup></table>"],
        tr: [2, "<table><tbody>", "</tbody></table>"],
        td: [3, "<table><tbody><tr>", "</tr></tbody></table>"],
        _default: [0, "", ""]
    };
    We.optgroup = We.option, We.tbody = We.tfoot = We.colgroup = We.caption = We.thead, We.th = We.td;
    var $e = /<|&#?\w+;/;
    !function () {
        var e = Y.createDocumentFragment().appendChild(Y.createElement("div")), t = Y.createElement("input");
        t.setAttribute("type", "radio"), t.setAttribute("checked", "checked"), t.setAttribute("name", "t"), e.appendChild(t), ae.checkClone = e.cloneNode(!0).cloneNode(!0).lastChild.checked, e.innerHTML = "<textarea>x</textarea>", ae.noCloneChecked = !!e.cloneNode(!0).lastChild.defaultValue
    }();
    var Be = Y.documentElement, _e = /^key/, ze = /^(?:mouse|pointer|contextmenu|drag|drop)|click/,
        Xe = /^([^.]*)(?:\.(.+)|)/;
    se.event = {
        global: {}, add: function (e, t, n, r, i) {
            var o, a, s, u, l, c, f, p, d, h, g, v = Ne.get(e);
            if (v) for (n.handler && (o = n, n = o.handler, i = o.selector), i && se.find.matchesSelector(Be, i), n.guid || (n.guid = se.guid++), (u = v.events) || (u = v.events = {}), (a = v.handle) || (a = v.handle = function (t) {
                return void 0 !== se && se.event.triggered !== t.type ? se.event.dispatch.apply(e, arguments) : void 0
            }), l = (t = (t || "").match(Te) || [""]).length; l--;) s = Xe.exec(t[l]) || [], d = g = s[1], h = (s[2] || "").split(".").sort(), d && (f = se.event.special[d] || {}, d = (i ? f.delegateType : f.bindType) || d, f = se.event.special[d] || {}, c = se.extend({
                type: d,
                origType: g,
                data: r,
                handler: n,
                guid: n.guid,
                selector: i,
                needsContext: i && se.expr.match.needsContext.test(i),
                namespace: h.join(".")
            }, o), (p = u[d]) || (p = u[d] = [], p.delegateCount = 0, f.setup && !1 !== f.setup.call(e, r, h, a) || e.addEventListener && e.addEventListener(d, a)), f.add && (f.add.call(e, c), c.handler.guid || (c.handler.guid = n.guid)), i ? p.splice(p.delegateCount++, 0, c) : p.push(c), se.event.global[d] = !0)
        }, remove: function (e, t, n, r, i) {
            var o, a, s, u, l, c, f, p, d, h, g, v = Ne.hasData(e) && Ne.get(e);
            if (v && (u = v.events)) {
                for (l = (t = (t || "").match(Te) || [""]).length; l--;) if (s = Xe.exec(t[l]) || [], d = g = s[1], h = (s[2] || "").split(".").sort(), d) {
                    for (f = se.event.special[d] || {}, p = u[d = (r ? f.delegateType : f.bindType) || d] || [], s = s[2] && new RegExp("(^|\\.)" + h.join("\\.(?:.*\\.|)") + "(\\.|$)"), a = o = p.length; o--;) c = p[o], !i && g !== c.origType || n && n.guid !== c.guid || s && !s.test(c.namespace) || r && r !== c.selector && ("**" !== r || !c.selector) || (p.splice(o, 1), c.selector && p.delegateCount--, f.remove && f.remove.call(e, c));
                    a && !p.length && (f.teardown && !1 !== f.teardown.call(e, h, v.handle) || se.removeEvent(e, d, v.handle), delete u[d])
                } else for (d in u) se.event.remove(e, d + t[l], n, r, !0);
                se.isEmptyObject(u) && Ne.remove(e, "handle events")
            }
        }, dispatch: function (e) {
            var t, n, r, i, o, a, s = se.event.fix(e), u = new Array(arguments.length),
                l = (Ne.get(this, "events") || {})[s.type] || [], c = se.event.special[s.type] || {};
            for (u[0] = s, t = 1; t < arguments.length; t++) u[t] = arguments[t];
            if (s.delegateTarget = this, !c.preDispatch || !1 !== c.preDispatch.call(this, s)) {
                for (a = se.event.handlers.call(this, s, l), t = 0; (i = a[t++]) && !s.isPropagationStopped();) for (s.currentTarget = i.elem, n = 0; (o = i.handlers[n++]) && !s.isImmediatePropagationStopped();) s.rnamespace && !s.rnamespace.test(o.namespace) || (s.handleObj = o, s.data = o.data, void 0 !== (r = ((se.event.special[o.origType] || {}).handle || o.handler).apply(i.elem, u)) && !1 === (s.result = r) && (s.preventDefault(), s.stopPropagation()));
                return c.postDispatch && c.postDispatch.call(this, s), s.result
            }
        }, handlers: function (e, t) {
            var n, r, i, o, a, s = [], u = t.delegateCount, l = e.target;
            if (u && l.nodeType && !("click" === e.type && e.button >= 1)) for (; l !== this; l = l.parentNode || this) if (1 === l.nodeType && ("click" !== e.type || !0 !== l.disabled)) {
                for (o = [], a = {}, n = 0; n < u; n++) r = t[n], i = r.selector + " ", void 0 === a[i] && (a[i] = r.needsContext ? se(i, this).index(l) > -1 : se.find(i, this, null, [l]).length), a[i] && o.push(r);
                o.length && s.push({elem: l, handlers: o})
            }
            return l = this, u < t.length && s.push({elem: l, handlers: t.slice(u)}), s
        }, addProp: function (e, t) {
            Object.defineProperty(se.Event.prototype, e, {
                enumerable: !0,
                configurable: !0,
                get: se.isFunction(t) ? function () {
                    if (this.originalEvent) return t(this.originalEvent)
                } : function () {
                    if (this.originalEvent) return this.originalEvent[e]
                },
                set: function (t) {
                    Object.defineProperty(this, e, {enumerable: !0, configurable: !0, writable: !0, value: t})
                }
            })
        }, fix: function (e) {
            return e[se.expando] ? e : new se.Event(e)
        }, special: {
            load: {noBubble: !0}, focus: {
                trigger: function () {
                    if (this !== w() && this.focus) return this.focus(), !1
                }, delegateType: "focusin"
            }, blur: {
                trigger: function () {
                    if (this === w() && this.blur) return this.blur(), !1
                }, delegateType: "focusout"
            }, click: {
                trigger: function () {
                    if ("checkbox" === this.type && this.click && i(this, "input")) return this.click(), !1
                }, _default: function (e) {
                    return i(e.target, "a")
                }
            }, beforeunload: {
                postDispatch: function (e) {
                    void 0 !== e.result && e.originalEvent && (e.originalEvent.returnValue = e.result)
                }
            }
        }
    }, se.removeEvent = function (e, t, n) {
        e.removeEventListener && e.removeEventListener(t, n)
    }, se.Event = function (e, t) {
        return this instanceof se.Event ? (e && e.type ? (this.originalEvent = e, this.type = e.type, this.isDefaultPrevented = e.defaultPrevented || void 0 === e.defaultPrevented && !1 === e.returnValue ? x : b, this.target = e.target && 3 === e.target.nodeType ? e.target.parentNode : e.target, this.currentTarget = e.currentTarget, this.relatedTarget = e.relatedTarget) : this.type = e, t && se.extend(this, t), this.timeStamp = e && e.timeStamp || se.now(), void(this[se.expando] = !0)) : new se.Event(e, t)
    }, se.Event.prototype = {
        constructor: se.Event,
        isDefaultPrevented: b,
        isPropagationStopped: b,
        isImmediatePropagationStopped: b,
        isSimulated: !1,
        preventDefault: function () {
            var e = this.originalEvent;
            this.isDefaultPrevented = x, e && !this.isSimulated && e.preventDefault()
        },
        stopPropagation: function () {
            var e = this.originalEvent;
            this.isPropagationStopped = x, e && !this.isSimulated && e.stopPropagation()
        },
        stopImmediatePropagation: function () {
            var e = this.originalEvent;
            this.isImmediatePropagationStopped = x, e && !this.isSimulated && e.stopImmediatePropagation(), this.stopPropagation()
        }
    }, se.each({
        altKey: !0,
        bubbles: !0,
        cancelable: !0,
        changedTouches: !0,
        ctrlKey: !0,
        detail: !0,
        eventPhase: !0,
        metaKey: !0,
        pageX: !0,
        pageY: !0,
        shiftKey: !0,
        view: !0,
        char: !0,
        charCode: !0,
        key: !0,
        keyCode: !0,
        button: !0,
        buttons: !0,
        clientX: !0,
        clientY: !0,
        offsetX: !0,
        offsetY: !0,
        pointerId: !0,
        pointerType: !0,
        screenX: !0,
        screenY: !0,
        targetTouches: !0,
        toElement: !0,
        touches: !0,
        which: function (e) {
            var t = e.button;
            return null == e.which && _e.test(e.type) ? null != e.charCode ? e.charCode : e.keyCode : !e.which && void 0 !== t && ze.test(e.type) ? 1 & t ? 1 : 2 & t ? 3 : 4 & t ? 2 : 0 : e.which
        }
    }, se.event.addProp), se.each({
        mouseenter: "mouseover",
        mouseleave: "mouseout",
        pointerenter: "pointerover",
        pointerleave: "pointerout"
    }, function (e, t) {
        se.event.special[e] = {
            delegateType: t, bindType: t, handle: function (e) {
                var n, r = e.relatedTarget, i = e.handleObj;
                return r && (r === this || se.contains(this, r)) || (e.type = i.origType, n = i.handler.apply(this, arguments), e.type = t), n
            }
        }
    }), se.fn.extend({
        on: function (e, t, n, r) {
            return T(this, e, t, n, r)
        }, one: function (e, t, n, r) {
            return T(this, e, t, n, r, 1)
        }, off: function (e, t, n) {
            var r, i;
            if (e && e.preventDefault && e.handleObj) return r = e.handleObj, se(e.delegateTarget).off(r.namespace ? r.origType + "." + r.namespace : r.origType, r.selector, r.handler), this;
            if ("object" == typeof e) {
                for (i in e) this.off(i, t, e[i]);
                return this
            }
            return !1 !== t && "function" != typeof t || (n = t, t = void 0), !1 === n && (n = b), this.each(function () {
                se.event.remove(this, e, n, t)
            })
        }
    });
    var Ue = /<(?!area|br|col|embed|hr|img|input|link|meta|param)(([a-z][^\/\0>\x20\t\r\n\f]*)[^>]*)\/>/gi,
        Ve = /<script|<style|<link/i, Ge = /checked\s*(?:[^=]|=\s*.checked.)/i, Ye = /^true\/(.*)/,
        Qe = /^\s*<!(?:\[CDATA\[|--)|(?:\]\]|--)>\s*$/g;
    se.extend({
        htmlPrefilter: function (e) {
            return e.replace(Ue, "<$1></$2>")
        }, clone: function (e, t, n) {
            var r, i, o, a, s = e.cloneNode(!0), u = se.contains(e.ownerDocument, e);
            if (!(ae.noCloneChecked || 1 !== e.nodeType && 11 !== e.nodeType || se.isXMLDoc(e))) for (a = v(s), o = v(e), r = 0, i = o.length; r < i; r++) N(o[r], a[r]);
            if (t) if (n) for (o = o || v(e), a = a || v(s), r = 0, i = o.length; r < i; r++) S(o[r], a[r]); else S(e, s);
            return (a = v(s, "script")).length > 0 && m(a, !u && v(e, "script")), s
        }, cleanData: function (e) {
            for (var t, n, r, i = se.event.special, o = 0; void 0 !== (n = e[o]); o++) if (Se(n)) {
                if (t = n[Ne.expando]) {
                    if (t.events) for (r in t.events) i[r] ? se.event.remove(n, r) : se.removeEvent(n, r, t.handle);
                    n[Ne.expando] = void 0
                }
                n[De.expando] && (n[De.expando] = void 0)
            }
        }
    }), se.fn.extend({
        detach: function (e) {
            return j(this, e, !0)
        }, remove: function (e) {
            return j(this, e)
        }, text: function (e) {
            return ke(this, function (e) {
                return void 0 === e ? se.text(this) : this.empty().each(function () {
                    1 !== this.nodeType && 11 !== this.nodeType && 9 !== this.nodeType || (this.textContent = e)
                })
            }, null, e, arguments.length)
        }, append: function () {
            return D(this, arguments, function (e) {
                if (1 === this.nodeType || 11 === this.nodeType || 9 === this.nodeType) {
                    C(this, e).appendChild(e)
                }
            })
        }, prepend: function () {
            return D(this, arguments, function (e) {
                if (1 === this.nodeType || 11 === this.nodeType || 9 === this.nodeType) {
                    var t = C(this, e);
                    t.insertBefore(e, t.firstChild)
                }
            })
        }, before: function () {
            return D(this, arguments, function (e) {
                this.parentNode && this.parentNode.insertBefore(e, this)
            })
        }, after: function () {
            return D(this, arguments, function (e) {
                this.parentNode && this.parentNode.insertBefore(e, this.nextSibling)
            })
        }, empty: function () {
            for (var e, t = 0; null != (e = this[t]); t++) 1 === e.nodeType && (se.cleanData(v(e, !1)), e.textContent = "");
            return this
        }, clone: function (e, t) {
            return e = null != e && e, t = null == t ? e : t, this.map(function () {
                return se.clone(this, e, t)
            })
        }, html: function (e) {
            return ke(this, function (e) {
                var t = this[0] || {}, n = 0, r = this.length;
                if (void 0 === e && 1 === t.nodeType) return t.innerHTML;
                if ("string" == typeof e && !Ve.test(e) && !We[(Me.exec(e) || ["", ""])[1].toLowerCase()]) {
                    e = se.htmlPrefilter(e);
                    try {
                        for (; n < r; n++) 1 === (t = this[n] || {}).nodeType && (se.cleanData(v(t, !1)), t.innerHTML = e);
                        t = 0
                    } catch (e) {
                    }
                }
                t && this.empty().append(e)
            }, null, e, arguments.length)
        }, replaceWith: function () {
            var e = [];
            return D(this, arguments, function (t) {
                var n = this.parentNode;
                se.inArray(this, e) < 0 && (se.cleanData(v(this)), n && n.replaceChild(t, this))
            }, e)
        }
    }), se.each({
        appendTo: "append",
        prependTo: "prepend",
        insertBefore: "before",
        insertAfter: "after",
        replaceAll: "replaceWith"
    }, function (e, t) {
        se.fn[e] = function (e) {
            for (var n, r = [], i = se(e), o = i.length - 1, a = 0; a <= o; a++) n = a === o ? this : this.clone(!0), se(i[a])[t](n), Z.apply(r, n.get());
            return this.pushStack(r)
        }
    });
    var Je = /^margin/, Ke = new RegExp("^(" + qe + ")(?!px)[a-z%]+$", "i"), Ze = function (t) {
        var n = t.ownerDocument.defaultView;
        return n && n.opener || (n = e), n.getComputedStyle(t)
    };
    !function () {
        function t() {
            if (s) {
                s.style.cssText = "box-sizing:border-box;position:relative;display:block;margin:auto;border:1px;padding:1px;top:1%;width:50%", s.innerHTML = "", Be.appendChild(a);
                var t = e.getComputedStyle(s);
                n = "1%" !== t.top, o = "2px" === t.marginLeft, r = "4px" === t.width, s.style.marginRight = "50%", i = "4px" === t.marginRight, Be.removeChild(a), s = null
            }
        }

        var n, r, i, o, a = Y.createElement("div"), s = Y.createElement("div");
        s.style && (s.style.backgroundClip = "content-box", s.cloneNode(!0).style.backgroundClip = "", ae.clearCloneStyle = "content-box" === s.style.backgroundClip, a.style.cssText = "border:0;width:8px;height:0;top:0;left:-9999px;padding:0;margin-top:1px;position:absolute", a.appendChild(s), se.extend(ae, {
            pixelPosition: function () {
                return t(), n
            }, boxSizingReliable: function () {
                return t(), r
            }, pixelMarginRight: function () {
                return t(), i
            }, reliableMarginLeft: function () {
                return t(), o
            }
        }))
    }();
    var et = /^(none|table(?!-c[ea]).+)/, tt = /^--/,
        nt = {position: "absolute", visibility: "hidden", display: "block"},
        rt = {letterSpacing: "0", fontWeight: "400"}, it = ["Webkit", "Moz", "ms"], ot = Y.createElement("div").style;
    se.extend({
        cssHooks: {
            opacity: {
                get: function (e, t) {
                    if (t) {
                        var n = A(e, "opacity");
                        return "" === n ? "1" : n
                    }
                }
            }
        },
        cssNumber: {
            animationIterationCount: !0,
            columnCount: !0,
            fillOpacity: !0,
            flexGrow: !0,
            flexShrink: !0,
            fontWeight: !0,
            lineHeight: !0,
            opacity: !0,
            order: !0,
            orphans: !0,
            widows: !0,
            zIndex: !0,
            zoom: !0
        },
        cssProps: {float: "cssFloat"},
        style: function (e, t, n, r) {
            if (e && 3 !== e.nodeType && 8 !== e.nodeType && e.style) {
                var i, o, a, s = se.camelCase(t), u = tt.test(t), l = e.style;
                return u || (t = L(s)), a = se.cssHooks[t] || se.cssHooks[s], void 0 === n ? a && "get" in a && void 0 !== (i = a.get(e, !1, r)) ? i : l[t] : ("string" === (o = typeof n) && (i = Le.exec(n)) && i[1] && (n = d(e, t, i), o = "number"), void(null != n && n == n && ("number" === o && (n += i && i[3] || (se.cssNumber[s] ? "" : "px")), ae.clearCloneStyle || "" !== n || 0 !== t.indexOf("background") || (l[t] = "inherit"), a && "set" in a && void 0 === (n = a.set(e, n, r)) || (u ? l.setProperty(t, n) : l[t] = n))))
            }
        },
        css: function (e, t, n, r) {
            var i, o, a, s = se.camelCase(t);
            return tt.test(t) || (t = L(s)), (a = se.cssHooks[t] || se.cssHooks[s]) && "get" in a && (i = a.get(e, !0, n)), void 0 === i && (i = A(e, t, r)), "normal" === i && t in rt && (i = rt[t]), "" === n || n ? (o = parseFloat(i), !0 === n || isFinite(o) ? o || 0 : i) : i
        }
    }), se.each(["height", "width"], function (e, t) {
        se.cssHooks[t] = {
            get: function (e, n, r) {
                if (n) return !et.test(se.css(e, "display")) || e.getClientRects().length && e.getBoundingClientRect().width ? O(e, t, r) : Oe(e, nt, function () {
                    return O(e, t, r)
                })
            }, set: function (e, n, r) {
                var i, o = r && Ze(e), a = r && F(e, t, r, "border-box" === se.css(e, "boxSizing", !1, o), o);
                return a && (i = Le.exec(n)) && "px" !== (i[3] || "px") && (e.style[t] = n, n = se.css(e, t)), H(0, n, a)
            }
        }
    }), se.cssHooks.marginLeft = q(ae.reliableMarginLeft, function (e, t) {
        if (t) return (parseFloat(A(e, "marginLeft")) || e.getBoundingClientRect().left - Oe(e, {marginLeft: 0}, function () {
            return e.getBoundingClientRect().left
        })) + "px"
    }), se.each({margin: "", padding: "", border: "Width"}, function (e, t) {
        se.cssHooks[e + t] = {
            expand: function (n) {
                for (var r = 0, i = {}, o = "string" == typeof n ? n.split(" ") : [n]; r < 4; r++) i[e + He[r] + t] = o[r] || o[r - 2] || o[0];
                return i
            }
        }, Je.test(e) || (se.cssHooks[e + t].set = H)
    }), se.fn.extend({
        css: function (e, t) {
            return ke(this, function (e, t, n) {
                var r, i, o = {}, a = 0;
                if (Array.isArray(t)) {
                    for (r = Ze(e), i = t.length; a < i; a++) o[t[a]] = se.css(e, t[a], !1, r);
                    return o
                }
                return void 0 !== n ? se.style(e, t, n) : se.css(e, t)
            }, e, t, arguments.length > 1)
        }
    }), se.Tween = P, (P.prototype = {
        constructor: P, init: function (e, t, n, r, i, o) {
            this.elem = e, this.prop = n, this.easing = i || se.easing._default, this.options = t, this.start = this.now = this.cur(), this.end = r, this.unit = o || (se.cssNumber[n] ? "" : "px")
        }, cur: function () {
            var e = P.propHooks[this.prop];
            return e && e.get ? e.get(this) : P.propHooks._default.get(this)
        }, run: function (e) {
            var t, n = P.propHooks[this.prop];
            return this.options.duration ? this.pos = t = se.easing[this.easing](e, this.options.duration * e, 0, 1, this.options.duration) : this.pos = t = e, this.now = (this.end - this.start) * t + this.start, this.options.step && this.options.step.call(this.elem, this.now, this), n && n.set ? n.set(this) : P.propHooks._default.set(this), this
        }
    }).init.prototype = P.prototype, (P.propHooks = {
        _default: {
            get: function (e) {
                var t;
                return 1 !== e.elem.nodeType || null != e.elem[e.prop] && null == e.elem.style[e.prop] ? e.elem[e.prop] : (t = se.css(e.elem, e.prop, "")) && "auto" !== t ? t : 0
            }, set: function (e) {
                se.fx.step[e.prop] ? se.fx.step[e.prop](e) : 1 !== e.elem.nodeType || null == e.elem.style[se.cssProps[e.prop]] && !se.cssHooks[e.prop] ? e.elem[e.prop] = e.now : se.style(e.elem, e.prop, e.now + e.unit)
            }
        }
    }).scrollTop = P.propHooks.scrollLeft = {
        set: function (e) {
            e.elem.nodeType && e.elem.parentNode && (e.elem[e.prop] = e.now)
        }
    }, se.easing = {
        linear: function (e) {
            return e
        }, swing: function (e) {
            return .5 - Math.cos(e * Math.PI) / 2
        }, _default: "swing"
    }, se.fx = P.prototype.init, se.fx.step = {};
    var at, st, ut = /^(?:toggle|show|hide)$/, lt = /queueHooks$/;
    se.Animation = se.extend($, {
        tweeners: {
            "*": [function (e, t) {
                var n = this.createTween(e, t);
                return d(n.elem, e, Le.exec(t), n), n
            }]
        }, tweener: function (e, t) {
            se.isFunction(e) ? (t = e, e = ["*"]) : e = e.match(Te);
            for (var n, r = 0, i = e.length; r < i; r++) n = e[r], $.tweeners[n] = $.tweeners[n] || [], $.tweeners[n].unshift(t)
        }, prefilters: [function (e, t, n) {
            var r, i, o, a, s, u, l, c, f = "width" in t || "height" in t, p = this, d = {}, h = e.style,
                v = e.nodeType && Fe(e), m = Ne.get(e, "fxshow");
            n.queue || (null == (a = se._queueHooks(e, "fx")).unqueued && (a.unqueued = 0, s = a.empty.fire, a.empty.fire = function () {
                a.unqueued || s()
            }), a.unqueued++, p.always(function () {
                p.always(function () {
                    a.unqueued--, se.queue(e, "fx").length || a.empty.fire()
                })
            }));
            for (r in t) if (i = t[r], ut.test(i)) {
                if (delete t[r], o = o || "toggle" === i, i === (v ? "hide" : "show")) {
                    if ("show" !== i || !m || void 0 === m[r]) continue;
                    v = !0
                }
                d[r] = m && m[r] || se.style(e, r)
            }
            if ((u = !se.isEmptyObject(t)) || !se.isEmptyObject(d)) {
                f && 1 === e.nodeType && (n.overflow = [h.overflow, h.overflowX, h.overflowY], null == (l = m && m.display) && (l = Ne.get(e, "display")), "none" === (c = se.css(e, "display")) && (l ? c = l : (g([e], !0), l = e.style.display || l, c = se.css(e, "display"), g([e]))), ("inline" === c || "inline-block" === c && null != l) && "none" === se.css(e, "float") && (u || (p.done(function () {
                    h.display = l
                }), null == l && (c = h.display, l = "none" === c ? "" : c)), h.display = "inline-block")), n.overflow && (h.overflow = "hidden", p.always(function () {
                    h.overflow = n.overflow[0], h.overflowX = n.overflow[1], h.overflowY = n.overflow[2]
                })), u = !1;
                for (r in d) u || (m ? "hidden" in m && (v = m.hidden) : m = Ne.access(e, "fxshow", {display: l}), o && (m.hidden = !v), v && g([e], !0), p.done(function () {
                    v || g([e]), Ne.remove(e, "fxshow");
                    for (r in d) se.style(e, r, d[r])
                })), u = W(v ? m[r] : 0, r, p), r in m || (m[r] = u.start, v && (u.end = u.start, u.start = 0))
            }
        }], prefilter: function (e, t) {
            t ? $.prefilters.unshift(e) : $.prefilters.push(e)
        }
    }), se.speed = function (e, t, n) {
        var r = e && "object" == typeof e ? se.extend({}, e) : {
            complete: n || !n && t || se.isFunction(e) && e,
            duration: e,
            easing: n && t || t && !se.isFunction(t) && t
        };
        return se.fx.off ? r.duration = 0 : "number" != typeof r.duration && (r.duration in se.fx.speeds ? r.duration = se.fx.speeds[r.duration] : r.duration = se.fx.speeds._default), null != r.queue && !0 !== r.queue || (r.queue = "fx"), r.old = r.complete, r.complete = function () {
            se.isFunction(r.old) && r.old.call(this), r.queue && se.dequeue(this, r.queue)
        }, r
    }, se.fn.extend({
        fadeTo: function (e, t, n, r) {
            return this.filter(Fe).css("opacity", 0).show().end().animate({opacity: t}, e, n, r)
        }, animate: function (e, t, n, r) {
            var i = se.isEmptyObject(e), o = se.speed(t, n, r), a = function () {
                var t = $(this, se.extend({}, e), o);
                (i || Ne.get(this, "finish")) && t.stop(!0)
            };
            return a.finish = a, i || !1 === o.queue ? this.each(a) : this.queue(o.queue, a)
        }, stop: function (e, t, n) {
            var r = function (e) {
                var t = e.stop;
                delete e.stop, t(n)
            };
            return "string" != typeof e && (n = t, t = e, e = void 0), t && !1 !== e && this.queue(e || "fx", []), this.each(function () {
                var t = !0, i = null != e && e + "queueHooks", o = se.timers, a = Ne.get(this);
                if (i) a[i] && a[i].stop && r(a[i]); else for (i in a) a[i] && a[i].stop && lt.test(i) && r(a[i]);
                for (i = o.length; i--;) o[i].elem !== this || null != e && o[i].queue !== e || (o[i].anim.stop(n), t = !1, o.splice(i, 1));
                !t && n || se.dequeue(this, e)
            })
        }, finish: function (e) {
            return !1 !== e && (e = e || "fx"), this.each(function () {
                var t, n = Ne.get(this), r = n[e + "queue"], i = n[e + "queueHooks"], o = se.timers,
                    a = r ? r.length : 0;
                for (n.finish = !0, se.queue(this, e, []), i && i.stop && i.stop.call(this, !0), t = o.length; t--;) o[t].elem === this && o[t].queue === e && (o[t].anim.stop(!0), o.splice(t, 1));
                for (t = 0; t < a; t++) r[t] && r[t].finish && r[t].finish.call(this);
                delete n.finish
            })
        }
    }), se.each(["toggle", "show", "hide"], function (e, t) {
        var n = se.fn[t];
        se.fn[t] = function (e, r, i) {
            return null == e || "boolean" == typeof e ? n.apply(this, arguments) : this.animate(I(t, !0), e, r, i)
        }
    }), se.each({
        slideDown: I("show"),
        slideUp: I("hide"),
        slideToggle: I("toggle"),
        fadeIn: {opacity: "show"},
        fadeOut: {opacity: "hide"},
        fadeToggle: {opacity: "toggle"}
    }, function (e, t) {
        se.fn[e] = function (e, n, r) {
            return this.animate(t, e, n, r)
        }
    }), se.timers = [], se.fx.tick = function () {
        var e, t = 0, n = se.timers;
        for (at = se.now(); t < n.length; t++) (e = n[t])() || n[t] !== e || n.splice(t--, 1);
        n.length || se.fx.stop(), at = void 0
    }, se.fx.timer = function (e) {
        se.timers.push(e), se.fx.start()
    }, se.fx.interval = 13, se.fx.start = function () {
        st || (st = !0, R())
    }, se.fx.stop = function () {
        st = null
    }, se.fx.speeds = {slow: 600, fast: 200, _default: 400}, se.fn.delay = function (t, n) {
        return t = se.fx ? se.fx.speeds[t] || t : t, n = n || "fx", this.queue(n, function (n, r) {
            var i = e.setTimeout(n, t);
            r.stop = function () {
                e.clearTimeout(i)
            }
        })
    }, function () {
        var e = Y.createElement("input"), t = Y.createElement("select").appendChild(Y.createElement("option"));
        e.type = "checkbox", ae.checkOn = "" !== e.value, ae.optSelected = t.selected, (e = Y.createElement("input")).value = "t", e.type = "radio", ae.radioValue = "t" === e.value
    }();
    var ct, ft = se.expr.attrHandle;
    se.fn.extend({
        attr: function (e, t) {
            return ke(this, se.attr, e, t, arguments.length > 1)
        }, removeAttr: function (e) {
            return this.each(function () {
                se.removeAttr(this, e)
            })
        }
    }), se.extend({
        attr: function (e, t, n) {
            var r, i, o = e.nodeType;
            if (3 !== o && 8 !== o && 2 !== o) return void 0 === e.getAttribute ? se.prop(e, t, n) : (1 === o && se.isXMLDoc(e) || (i = se.attrHooks[t.toLowerCase()] || (se.expr.match.bool.test(t) ? ct : void 0)), void 0 !== n ? null === n ? void se.removeAttr(e, t) : i && "set" in i && void 0 !== (r = i.set(e, n, t)) ? r : (e.setAttribute(t, n + ""), n) : i && "get" in i && null !== (r = i.get(e, t)) ? r : null == (r = se.find.attr(e, t)) ? void 0 : r)
        }, attrHooks: {
            type: {
                set: function (e, t) {
                    if (!ae.radioValue && "radio" === t && i(e, "input")) {
                        var n = e.value;
                        return e.setAttribute("type", t), n && (e.value = n), t
                    }
                }
            }
        }, removeAttr: function (e, t) {
            var n, r = 0, i = t && t.match(Te);
            if (i && 1 === e.nodeType) for (; n = i[r++];) e.removeAttribute(n)
        }
    }), ct = {
        set: function (e, t, n) {
            return !1 === t ? se.removeAttr(e, n) : e.setAttribute(n, n), n
        }
    }, se.each(se.expr.match.bool.source.match(/\w+/g), function (e, t) {
        var n = ft[t] || se.find.attr;
        ft[t] = function (e, t, r) {
            var i, o, a = t.toLowerCase();
            return r || (o = ft[a], ft[a] = i, i = null != n(e, t, r) ? a : null, ft[a] = o), i
        }
    });
    var pt = /^(?:input|select|textarea|button)$/i, dt = /^(?:a|area)$/i;
    se.fn.extend({
        prop: function (e, t) {
            return ke(this, se.prop, e, t, arguments.length > 1)
        }, removeProp: function (e) {
            return this.each(function () {
                delete this[se.propFix[e] || e]
            })
        }
    }), se.extend({
        prop: function (e, t, n) {
            var r, i, o = e.nodeType;
            if (3 !== o && 8 !== o && 2 !== o) return 1 === o && se.isXMLDoc(e) || (t = se.propFix[t] || t, i = se.propHooks[t]), void 0 !== n ? i && "set" in i && void 0 !== (r = i.set(e, n, t)) ? r : e[t] = n : i && "get" in i && null !== (r = i.get(e, t)) ? r : e[t]
        }, propHooks: {
            tabIndex: {
                get: function (e) {
                    var t = se.find.attr(e, "tabindex");
                    return t ? parseInt(t, 10) : pt.test(e.nodeName) || dt.test(e.nodeName) && e.href ? 0 : -1
                }
            }
        }, propFix: {for: "htmlFor", class: "className"}
    }), ae.optSelected || (se.propHooks.selected = {
        get: function (e) {
            var t = e.parentNode;
            return t && t.parentNode && t.parentNode.selectedIndex, null
        }, set: function (e) {
            var t = e.parentNode;
            t && (t.selectedIndex, t.parentNode && t.parentNode.selectedIndex)
        }
    }), se.each(["tabIndex", "readOnly", "maxLength", "cellSpacing", "cellPadding", "rowSpan", "colSpan", "useMap", "frameBorder", "contentEditable"], function () {
        se.propFix[this.toLowerCase()] = this
    }), se.fn.extend({
        addClass: function (e) {
            var t, n, r, i, o, a, s, u = 0;
            if (se.isFunction(e)) return this.each(function (t) {
                se(this).addClass(e.call(this, t, _(this)))
            });
            if ("string" == typeof e && e) for (t = e.match(Te) || []; n = this[u++];) if (i = _(n), r = 1 === n.nodeType && " " + B(i) + " ") {
                for (a = 0; o = t[a++];) r.indexOf(" " + o + " ") < 0 && (r += o + " ");
                i !== (s = B(r)) && n.setAttribute("class", s)
            }
            return this
        }, removeClass: function (e) {
            var t, n, r, i, o, a, s, u = 0;
            if (se.isFunction(e)) return this.each(function (t) {
                se(this).removeClass(e.call(this, t, _(this)))
            });
            if (!arguments.length) return this.attr("class", "");
            if ("string" == typeof e && e) for (t = e.match(Te) || []; n = this[u++];) if (i = _(n), r = 1 === n.nodeType && " " + B(i) + " ") {
                for (a = 0; o = t[a++];) for (; r.indexOf(" " + o + " ") > -1;) r = r.replace(" " + o + " ", " ");
                i !== (s = B(r)) && n.setAttribute("class", s)
            }
            return this
        }, toggleClass: function (e, t) {
            var n = typeof e;
            return "boolean" == typeof t && "string" === n ? t ? this.addClass(e) : this.removeClass(e) : se.isFunction(e) ? this.each(function (n) {
                se(this).toggleClass(e.call(this, n, _(this), t), t)
            }) : this.each(function () {
                var t, r, i, o;
                if ("string" === n) for (r = 0, i = se(this), o = e.match(Te) || []; t = o[r++];) i.hasClass(t) ? i.removeClass(t) : i.addClass(t); else void 0 !== e && "boolean" !== n || ((t = _(this)) && Ne.set(this, "__className__", t), this.setAttribute && this.setAttribute("class", t || !1 === e ? "" : Ne.get(this, "__className__") || ""))
            })
        }, hasClass: function (e) {
            var t, n, r = 0;
            for (t = " " + e + " "; n = this[r++];) if (1 === n.nodeType && (" " + B(_(n)) + " ").indexOf(t) > -1) return !0;
            return !1
        }
    });
    var ht = /\r/g;
    se.fn.extend({
        val: function (e) {
            var t, n, r, i = this[0];
            return arguments.length ? (r = se.isFunction(e), this.each(function (n) {
                var i;
                1 === this.nodeType && (null == (i = r ? e.call(this, n, se(this).val()) : e) ? i = "" : "number" == typeof i ? i += "" : Array.isArray(i) && (i = se.map(i, function (e) {
                    return null == e ? "" : e + ""
                })), (t = se.valHooks[this.type] || se.valHooks[this.nodeName.toLowerCase()]) && "set" in t && void 0 !== t.set(this, i, "value") || (this.value = i))
            })) : i ? (t = se.valHooks[i.type] || se.valHooks[i.nodeName.toLowerCase()]) && "get" in t && void 0 !== (n = t.get(i, "value")) ? n : "string" == typeof(n = i.value) ? n.replace(ht, "") : null == n ? "" : n : void 0
        }
    }), se.extend({
        valHooks: {
            option: {
                get: function (e) {
                    var t = se.find.attr(e, "value");
                    return null != t ? t : B(se.text(e))
                }
            }, select: {
                get: function (e) {
                    var t, n, r, o = e.options, a = e.selectedIndex, s = "select-one" === e.type, u = s ? null : [],
                        l = s ? a + 1 : o.length;
                    for (r = a < 0 ? l : s ? a : 0; r < l; r++) if (((n = o[r]).selected || r === a) && !n.disabled && (!n.parentNode.disabled || !i(n.parentNode, "optgroup"))) {
                        if (t = se(n).val(), s) return t;
                        u.push(t)
                    }
                    return u
                }, set: function (e, t) {
                    for (var n, r, i = e.options, o = se.makeArray(t), a = i.length; a--;) r = i[a], (r.selected = se.inArray(se.valHooks.option.get(r), o) > -1) && (n = !0);
                    return n || (e.selectedIndex = -1), o
                }
            }
        }
    }), se.each(["radio", "checkbox"], function () {
        se.valHooks[this] = {
            set: function (e, t) {
                if (Array.isArray(t)) return e.checked = se.inArray(se(e).val(), t) > -1
            }
        }, ae.checkOn || (se.valHooks[this].get = function (e) {
            return null === e.getAttribute("value") ? "on" : e.value
        })
    });
    var gt = /^(?:focusinfocus|focusoutblur)$/;
    se.extend(se.event, {
        trigger: function (t, n, r, i) {
            var o, a, s, u, l, c, f, p = [r || Y], d = re.call(t, "type") ? t.type : t,
                h = re.call(t, "namespace") ? t.namespace.split(".") : [];
            if (a = s = r = r || Y, 3 !== r.nodeType && 8 !== r.nodeType && !gt.test(d + se.event.triggered) && (d.indexOf(".") > -1 && (h = d.split("."), d = h.shift(), h.sort()), l = d.indexOf(":") < 0 && "on" + d, t = t[se.expando] ? t : new se.Event(d, "object" == typeof t && t), t.isTrigger = i ? 2 : 3, t.namespace = h.join("."), t.rnamespace = t.namespace ? new RegExp("(^|\\.)" + h.join("\\.(?:.*\\.|)") + "(\\.|$)") : null, t.result = void 0, t.target || (t.target = r), n = null == n ? [t] : se.makeArray(n, [t]), f = se.event.special[d] || {}, i || !f.trigger || !1 !== f.trigger.apply(r, n))) {
                if (!i && !f.noBubble && !se.isWindow(r)) {
                    for (u = f.delegateType || d, gt.test(u + d) || (a = a.parentNode); a; a = a.parentNode) p.push(a), s = a;
                    s === (r.ownerDocument || Y) && p.push(s.defaultView || s.parentWindow || e)
                }
                for (o = 0; (a = p[o++]) && !t.isPropagationStopped();) t.type = o > 1 ? u : f.bindType || d, (c = (Ne.get(a, "events") || {})[t.type] && Ne.get(a, "handle")) && c.apply(a, n), (c = l && a[l]) && c.apply && Se(a) && (t.result = c.apply(a, n), !1 === t.result && t.preventDefault());
                return t.type = d, i || t.isDefaultPrevented() || f._default && !1 !== f._default.apply(p.pop(), n) || !Se(r) || l && se.isFunction(r[d]) && !se.isWindow(r) && ((s = r[l]) && (r[l] = null), se.event.triggered = d, r[d](), se.event.triggered = void 0, s && (r[l] = s)), t.result
            }
        }, simulate: function (e, t, n) {
            var r = se.extend(new se.Event, n, {type: e, isSimulated: !0});
            se.event.trigger(r, null, t)
        }
    }), se.fn.extend({
        trigger: function (e, t) {
            return this.each(function () {
                se.event.trigger(e, t, this)
            })
        }, triggerHandler: function (e, t) {
            var n = this[0];
            if (n) return se.event.trigger(e, t, n, !0)
        }
    }), se.each("blur focus focusin focusout resize scroll click dblclick mousedown mouseup mousemove mouseover mouseout mouseenter mouseleave change select submit keydown keypress keyup contextmenu".split(" "), function (e, t) {
        se.fn[t] = function (e, n) {
            return arguments.length > 0 ? this.on(t, null, e, n) : this.trigger(t)
        }
    }), se.fn.extend({
        hover: function (e, t) {
            return this.mouseenter(e).mouseleave(t || e)
        }
    }), ae.focusin = "onfocusin" in e, ae.focusin || se.each({focus: "focusin", blur: "focusout"}, function (e, t) {
        var n = function (e) {
            se.event.simulate(t, e.target, se.event.fix(e))
        };
        se.event.special[t] = {
            setup: function () {
                var r = this.ownerDocument || this, i = Ne.access(r, t);
                i || r.addEventListener(e, n, !0), Ne.access(r, t, (i || 0) + 1)
            }, teardown: function () {
                var r = this.ownerDocument || this, i = Ne.access(r, t) - 1;
                i ? Ne.access(r, t, i) : (r.removeEventListener(e, n, !0), Ne.remove(r, t))
            }
        }
    });
    var vt = e.location, mt = se.now(), yt = /\?/;
    se.parseXML = function (t) {
        var n;
        if (!t || "string" != typeof t) return null;
        try {
            n = (new e.DOMParser).parseFromString(t, "text/xml")
        } catch (e) {
            n = void 0
        }
        return n && !n.getElementsByTagName("parsererror").length || se.error("Invalid XML: " + t), n
    };
    var xt = /\[\]$/, bt = /\r?\n/g, wt = /^(?:submit|button|image|reset|file)$/i,
        Tt = /^(?:input|select|textarea|keygen)/i;
    se.param = function (e, t) {
        var n, r = [], i = function (e, t) {
            var n = se.isFunction(t) ? t() : t;
            r[r.length] = encodeURIComponent(e) + "=" + encodeURIComponent(null == n ? "" : n)
        };
        if (Array.isArray(e) || e.jquery && !se.isPlainObject(e)) se.each(e, function () {
            i(this.name, this.value)
        }); else for (n in e) z(n, e[n], t, i);
        return r.join("&")
    }, se.fn.extend({
        serialize: function () {
            return se.param(this.serializeArray())
        }, serializeArray: function () {
            return this.map(function () {
                var e = se.prop(this, "elements");
                return e ? se.makeArray(e) : this
            }).filter(function () {
                var e = this.type;
                return this.name && !se(this).is(":disabled") && Tt.test(this.nodeName) && !wt.test(e) && (this.checked || !Re.test(e))
            }).map(function (e, t) {
                var n = se(this).val();
                return null == n ? null : Array.isArray(n) ? se.map(n, function (e) {
                    return {name: t.name, value: e.replace(bt, "\r\n")}
                }) : {name: t.name, value: n.replace(bt, "\r\n")}
            }).get()
        }
    });
    var Ct = /%20/g, Et = /#.*$/, kt = /([?&])_=[^&]*/, St = /^(.*?):[ \t]*([^\r\n]*)$/gm, Nt = /^(?:GET|HEAD)$/,
        Dt = /^\/\//, jt = {}, At = {}, qt = "*/".concat("*"), Lt = Y.createElement("a");
    Lt.href = vt.href, se.extend({
        active: 0,
        lastModified: {},
        etag: {},
        ajaxSettings: {
            url: vt.href,
            type: "GET",
            isLocal: /^(?:about|app|app-storage|.+-extension|file|res|widget):$/.test(vt.protocol),
            global: !0,
            processData: !0,
            async: !0,
            contentType: "application/x-www-form-urlencoded; charset=UTF-8",
            accepts: {
                "*": qt,
                text: "text/plain",
                html: "text/html",
                xml: "application/xml, text/xml",
                json: "application/json, text/javascript"
            },
            contents: {xml: /\bxml\b/, html: /\bhtml/, json: /\bjson\b/},
            responseFields: {xml: "responseXML", text: "responseText", json: "responseJSON"},
            converters: {"* text": String, "text html": !0, "text json": JSON.parse, "text xml": se.parseXML},
            flatOptions: {url: !0, context: !0}
        },
        ajaxSetup: function (e, t) {
            return t ? V(V(e, se.ajaxSettings), t) : V(se.ajaxSettings, e)
        },
        ajaxPrefilter: X(jt),
        ajaxTransport: X(At),
        ajax: function (t, n) {
            function r(t, n, r, s) {
                var l, p, d, b, w, T = n;
                c || (c = !0, u && e.clearTimeout(u), i = void 0, a = s || "", C.readyState = t > 0 ? 4 : 0, l = t >= 200 && t < 300 || 304 === t, r && (b = function (e, t, n) {
                    for (var r, i, o, a, s = e.contents, u = e.dataTypes; "*" === u[0];) u.shift(), void 0 === r && (r = e.mimeType || t.getResponseHeader("Content-Type"));
                    if (r) for (i in s) if (s[i] && s[i].test(r)) {
                        u.unshift(i);
                        break
                    }
                    if (u[0] in n) o = u[0]; else {
                        for (i in n) {
                            if (!u[0] || e.converters[i + " " + u[0]]) {
                                o = i;
                                break
                            }
                            a || (a = i)
                        }
                        o = o || a
                    }
                    if (o) return o !== u[0] && u.unshift(o), n[o]
                }(h, C, r)), b = function (e, t, n, r) {
                    var i, o, a, s, u, l = {}, c = e.dataTypes.slice();
                    if (c[1]) for (a in e.converters) l[a.toLowerCase()] = e.converters[a];
                    for (o = c.shift(); o;) if (e.responseFields[o] && (n[e.responseFields[o]] = t), !u && r && e.dataFilter && (t = e.dataFilter(t, e.dataType)), u = o, o = c.shift()) if ("*" === o) o = u; else if ("*" !== u && u !== o) {
                        if (!(a = l[u + " " + o] || l["* " + o])) for (i in l) if ((s = i.split(" "))[1] === o && (a = l[u + " " + s[0]] || l["* " + s[0]])) {
                            !0 === a ? a = l[i] : !0 !== l[i] && (o = s[0], c.unshift(s[1]));
                            break
                        }
                        if (!0 !== a) if (a && e.throws) t = a(t); else try {
                            t = a(t)
                        } catch (e) {
                            return {state: "parsererror", error: a ? e : "No conversion from " + u + " to " + o}
                        }
                    }
                    return {state: "success", data: t}
                }(h, b, C, l), l ? (h.ifModified && ((w = C.getResponseHeader("Last-Modified")) && (se.lastModified[o] = w), (w = C.getResponseHeader("etag")) && (se.etag[o] = w)), 204 === t || "HEAD" === h.type ? T = "nocontent" : 304 === t ? T = "notmodified" : (T = b.state, p = b.data, d = b.error, l = !d)) : (d = T, !t && T || (T = "error", t < 0 && (t = 0))), C.status = t, C.statusText = (n || T) + "", l ? m.resolveWith(g, [p, T, C]) : m.rejectWith(g, [C, T, d]), C.statusCode(x), x = void 0, f && v.trigger(l ? "ajaxSuccess" : "ajaxError", [C, h, l ? p : d]), y.fireWith(g, [C, T]), f && (v.trigger("ajaxComplete", [C, h]), --se.active || se.event.trigger("ajaxStop")))
            }

            "object" == typeof t && (n = t, t = void 0), n = n || {};
            var i, o, a, s, u, l, c, f, p, d, h = se.ajaxSetup({}, n), g = h.context || h,
                v = h.context && (g.nodeType || g.jquery) ? se(g) : se.event, m = se.Deferred(),
                y = se.Callbacks("once memory"), x = h.statusCode || {}, b = {}, w = {}, T = "canceled", C = {
                    readyState: 0, getResponseHeader: function (e) {
                        var t;
                        if (c) {
                            if (!s) for (s = {}; t = St.exec(a);) s[t[1].toLowerCase()] = t[2];
                            t = s[e.toLowerCase()]
                        }
                        return null == t ? null : t
                    }, getAllResponseHeaders: function () {
                        return c ? a : null
                    }, setRequestHeader: function (e, t) {
                        return null == c && (e = w[e.toLowerCase()] = w[e.toLowerCase()] || e, b[e] = t), this
                    }, overrideMimeType: function (e) {
                        return null == c && (h.mimeType = e), this
                    }, statusCode: function (e) {
                        var t;
                        if (e) if (c) C.always(e[C.status]); else for (t in e) x[t] = [x[t], e[t]];
                        return this
                    }, abort: function (e) {
                        var t = e || T;
                        return i && i.abort(t), r(0, t), this
                    }
                };
            if (m.promise(C), h.url = ((t || h.url || vt.href) + "").replace(Dt, vt.protocol + "//"), h.type = n.method || n.type || h.method || h.type, h.dataTypes = (h.dataType || "*").toLowerCase().match(Te) || [""], null == h.crossDomain) {
                l = Y.createElement("a");
                try {
                    l.href = h.url, l.href = l.href, h.crossDomain = Lt.protocol + "//" + Lt.host != l.protocol + "//" + l.host
                } catch (e) {
                    h.crossDomain = !0
                }
            }
            if (h.data && h.processData && "string" != typeof h.data && (h.data = se.param(h.data, h.traditional)), U(jt, h, n, C), c) return C;
            (f = se.event && h.global) && 0 == se.active++ && se.event.trigger("ajaxStart"), h.type = h.type.toUpperCase(), h.hasContent = !Nt.test(h.type), o = h.url.replace(Et, ""), h.hasContent ? h.data && h.processData && 0 === (h.contentType || "").indexOf("application/x-www-form-urlencoded") && (h.data = h.data.replace(Ct, "+")) : (d = h.url.slice(o.length), h.data && (o += (yt.test(o) ? "&" : "?") + h.data, delete h.data), !1 === h.cache && (o = o.replace(kt, "$1"), d = (yt.test(o) ? "&" : "?") + "_=" + mt++ + d), h.url = o + d), h.ifModified && (se.lastModified[o] && C.setRequestHeader("If-Modified-Since", se.lastModified[o]), se.etag[o] && C.setRequestHeader("If-None-Match", se.etag[o])), (h.data && h.hasContent && !1 !== h.contentType || n.contentType) && C.setRequestHeader("Content-Type", h.contentType), C.setRequestHeader("Accept", h.dataTypes[0] && h.accepts[h.dataTypes[0]] ? h.accepts[h.dataTypes[0]] + ("*" !== h.dataTypes[0] ? ", " + qt + "; q=0.01" : "") : h.accepts["*"]);
            for (p in h.headers) C.setRequestHeader(p, h.headers[p]);
            if (h.beforeSend && (!1 === h.beforeSend.call(g, C, h) || c)) return C.abort();
            if (T = "abort", y.add(h.complete), C.done(h.success), C.fail(h.error), i = U(At, h, n, C)) {
                if (C.readyState = 1, f && v.trigger("ajaxSend", [C, h]), c) return C;
                h.async && h.timeout > 0 && (u = e.setTimeout(function () {
                    C.abort("timeout")
                }, h.timeout));
                try {
                    c = !1, i.send(b, r)
                } catch (e) {
                    if (c) throw e;
                    r(-1, e)
                }
            } else r(-1, "No Transport");
            return C
        },
        getJSON: function (e, t, n) {
            return se.get(e, t, n, "json")
        },
        getScript: function (e, t) {
            return se.get(e, void 0, t, "script")
        }
    }), se.each(["get", "post"], function (e, t) {
        se[t] = function (e, n, r, i) {
            return se.isFunction(n) && (i = i || r, r = n, n = void 0), se.ajax(se.extend({
                url: e,
                type: t,
                dataType: i,
                data: n,
                success: r
            }, se.isPlainObject(e) && e))
        }
    }), se._evalUrl = function (e) {
        return se.ajax({url: e, type: "GET", dataType: "script", cache: !0, async: !1, global: !1, throws: !0})
    }, se.fn.extend({
        wrapAll: function (e) {
            var t;
            return this[0] && (se.isFunction(e) && (e = e.call(this[0])), t = se(e, this[0].ownerDocument).eq(0).clone(!0), this[0].parentNode && t.insertBefore(this[0]), t.map(function () {
                for (var e = this; e.firstElementChild;) e = e.firstElementChild;
                return e
            }).append(this)), this
        }, wrapInner: function (e) {
            return se.isFunction(e) ? this.each(function (t) {
                se(this).wrapInner(e.call(this, t))
            }) : this.each(function () {
                var t = se(this), n = t.contents();
                n.length ? n.wrapAll(e) : t.append(e)
            })
        }, wrap: function (e) {
            var t = se.isFunction(e);
            return this.each(function (n) {
                se(this).wrapAll(t ? e.call(this, n) : e)
            })
        }, unwrap: function (e) {
            return this.parent(e).not("body").each(function () {
                se(this).replaceWith(this.childNodes)
            }), this
        }
    }), se.expr.pseudos.hidden = function (e) {
        return !se.expr.pseudos.visible(e)
    }, se.expr.pseudos.visible = function (e) {
        return !!(e.offsetWidth || e.offsetHeight || e.getClientRects().length)
    }, se.ajaxSettings.xhr = function () {
        try {
            return new e.XMLHttpRequest
        } catch (e) {
        }
    };
    var Ht = {0: 200, 1223: 204}, Ft = se.ajaxSettings.xhr();
    ae.cors = !!Ft && "withCredentials" in Ft, ae.ajax = Ft = !!Ft, se.ajaxTransport(function (t) {
        var n, r;
        if (ae.cors || Ft && !t.crossDomain) return {
            send: function (i, o) {
                var a, s = t.xhr();
                if (s.open(t.type, t.url, t.async, t.username, t.password), t.xhrFields) for (a in t.xhrFields) s[a] = t.xhrFields[a];
                t.mimeType && s.overrideMimeType && s.overrideMimeType(t.mimeType), t.crossDomain || i["X-Requested-With"] || (i["X-Requested-With"] = "XMLHttpRequest");
                for (a in i) s.setRequestHeader(a, i[a]);
                n = function (e) {
                    return function () {
                        n && (n = r = s.onload = s.onerror = s.onabort = s.onreadystatechange = null, "abort" === e ? s.abort() : "error" === e ? "number" != typeof s.status ? o(0, "error") : o(s.status, s.statusText) : o(Ht[s.status] || s.status, s.statusText, "text" !== (s.responseType || "text") || "string" != typeof s.responseText ? {binary: s.response} : {text: s.responseText}, s.getAllResponseHeaders()))
                    }
                }, s.onload = n(), r = s.onerror = n("error"), void 0 !== s.onabort ? s.onabort = r : s.onreadystatechange = function () {
                    4 === s.readyState && e.setTimeout(function () {
                        n && r()
                    })
                }, n = n("abort");
                try {
                    s.send(t.hasContent && t.data || null)
                } catch (e) {
                    if (n) throw e
                }
            }, abort: function () {
                n && n()
            }
        }
    }), se.ajaxPrefilter(function (e) {
        e.crossDomain && (e.contents.script = !1)
    }), se.ajaxSetup({
        accepts: {script: "text/javascript, application/javascript, application/ecmascript, application/x-ecmascript"},
        contents: {script: /\b(?:java|ecma)script\b/},
        converters: {
            "text script": function (e) {
                return se.globalEval(e), e
            }
        }
    }), se.ajaxPrefilter("script", function (e) {
        void 0 === e.cache && (e.cache = !1), e.crossDomain && (e.type = "GET")
    }), se.ajaxTransport("script", function (e) {
        if (e.crossDomain) {
            var t, n;
            return {
                send: function (r, i) {
                    t = se("<script>").prop({charset: e.scriptCharset, src: e.url}).on("load error", n = function (e) {
                        t.remove(), n = null, e && i("error" === e.type ? 404 : 200, e.type)
                    }), Y.head.appendChild(t[0])
                }, abort: function () {
                    n && n()
                }
            }
        }
    });
    var Ot = [], Pt = /(=)\?(?=&|$)|\?\?/;
    se.ajaxSetup({
        jsonp: "callback", jsonpCallback: function () {
            var e = Ot.pop() || se.expando + "_" + mt++;
            return this[e] = !0, e
        }
    }), se.ajaxPrefilter("json jsonp", function (t, n, r) {
        var i, o, a,
            s = !1 !== t.jsonp && (Pt.test(t.url) ? "url" : "string" == typeof t.data && 0 === (t.contentType || "").indexOf("application/x-www-form-urlencoded") && Pt.test(t.data) && "data");
        if (s || "jsonp" === t.dataTypes[0]) return i = t.jsonpCallback = se.isFunction(t.jsonpCallback) ? t.jsonpCallback() : t.jsonpCallback, s ? t[s] = t[s].replace(Pt, "$1" + i) : !1 !== t.jsonp && (t.url += (yt.test(t.url) ? "&" : "?") + t.jsonp + "=" + i), t.converters["script json"] = function () {
            return a || se.error(i + " was not called"), a[0]
        }, t.dataTypes[0] = "json", o = e[i], e[i] = function () {
            a = arguments
        }, r.always(function () {
            void 0 === o ? se(e).removeProp(i) : e[i] = o, t[i] && (t.jsonpCallback = n.jsonpCallback, Ot.push(i)), a && se.isFunction(o) && o(a[0]), a = o = void 0
        }), "script"
    }), ae.createHTMLDocument = function () {
        var e = Y.implementation.createHTMLDocument("").body;
        return e.innerHTML = "<form></form><form></form>", 2 === e.childNodes.length
    }(), se.parseHTML = function (e, t, n) {
        if ("string" != typeof e) return [];
        "boolean" == typeof t && (n = t, t = !1);
        var r, i, o;
        return t || (ae.createHTMLDocument ? (t = Y.implementation.createHTMLDocument(""), r = t.createElement("base"), r.href = Y.location.href, t.head.appendChild(r)) : t = Y), i = ve.exec(e), o = !n && [], i ? [t.createElement(i[1])] : (i = y([e], t, o), o && o.length && se(o).remove(), se.merge([], i.childNodes))
    }, se.fn.load = function (e, t, n) {
        var r, i, o, a = this, s = e.indexOf(" ");
        return s > -1 && (r = B(e.slice(s)), e = e.slice(0, s)), se.isFunction(t) ? (n = t, t = void 0) : t && "object" == typeof t && (i = "POST"), a.length > 0 && se.ajax({
            url: e,
            type: i || "GET",
            dataType: "html",
            data: t
        }).done(function (e) {
            o = arguments, a.html(r ? se("<div>").append(se.parseHTML(e)).find(r) : e)
        }).always(n && function (e, t) {
            a.each(function () {
                n.apply(this, o || [e.responseText, t, e])
            })
        }), this
    }, se.each(["ajaxStart", "ajaxStop", "ajaxComplete", "ajaxError", "ajaxSuccess", "ajaxSend"], function (e, t) {
        se.fn[t] = function (e) {
            return this.on(t, e)
        }
    }), se.expr.pseudos.animated = function (e) {
        return se.grep(se.timers, function (t) {
            return e === t.elem
        }).length
    }, se.offset = {
        setOffset: function (e, t, n) {
            var r, i, o, a, s, u, l = se.css(e, "position"), c = se(e), f = {};
            "static" === l && (e.style.position = "relative"), s = c.offset(), o = se.css(e, "top"), u = se.css(e, "left"), ("absolute" === l || "fixed" === l) && (o + u).indexOf("auto") > -1 ? (r = c.position(), a = r.top, i = r.left) : (a = parseFloat(o) || 0, i = parseFloat(u) || 0), se.isFunction(t) && (t = t.call(e, n, se.extend({}, s))), null != t.top && (f.top = t.top - s.top + a), null != t.left && (f.left = t.left - s.left + i), "using" in t ? t.using.call(e, f) : c.css(f)
        }
    }, se.fn.extend({
        offset: function (e) {
            if (arguments.length) return void 0 === e ? this : this.each(function (t) {
                se.offset.setOffset(this, e, t)
            });
            var t, n, r, i, o = this[0];
            return o ? o.getClientRects().length ? (r = o.getBoundingClientRect(), t = o.ownerDocument, n = t.documentElement, i = t.defaultView, {
                top: r.top + i.pageYOffset - n.clientTop,
                left: r.left + i.pageXOffset - n.clientLeft
            }) : {top: 0, left: 0} : void 0
        }, position: function () {
            if (this[0]) {
                var e, t, n = this[0], r = {top: 0, left: 0};
                return "fixed" === se.css(n, "position") ? t = n.getBoundingClientRect() : (e = this.offsetParent(), t = this.offset(), i(e[0], "html") || (r = e.offset()), r = {
                    top: r.top + se.css(e[0], "borderTopWidth", !0),
                    left: r.left + se.css(e[0], "borderLeftWidth", !0)
                }), {
                    top: t.top - r.top - se.css(n, "marginTop", !0),
                    left: t.left - r.left - se.css(n, "marginLeft", !0)
                }
            }
        }, offsetParent: function () {
            return this.map(function () {
                for (var e = this.offsetParent; e && "static" === se.css(e, "position");) e = e.offsetParent;
                return e || Be
            })
        }
    }), se.each({scrollLeft: "pageXOffset", scrollTop: "pageYOffset"}, function (e, t) {
        var n = "pageYOffset" === t;
        se.fn[e] = function (r) {
            return ke(this, function (e, r, i) {
                var o;
                return se.isWindow(e) ? o = e : 9 === e.nodeType && (o = e.defaultView), void 0 === i ? o ? o[t] : e[r] : void(o ? o.scrollTo(n ? o.pageXOffset : i, n ? i : o.pageYOffset) : e[r] = i)
            }, e, r, arguments.length)
        }
    }), se.each(["top", "left"], function (e, t) {
        se.cssHooks[t] = q(ae.pixelPosition, function (e, n) {
            if (n) return n = A(e, t), Ke.test(n) ? se(e).position()[t] + "px" : n
        })
    }), se.each({Height: "height", Width: "width"}, function (e, t) {
        se.each({padding: "inner" + e, content: t, "": "outer" + e}, function (n, r) {
            se.fn[r] = function (i, o) {
                var a = arguments.length && (n || "boolean" != typeof i),
                    s = n || (!0 === i || !0 === o ? "margin" : "border");
                return ke(this, function (t, n, i) {
                    var o;
                    return se.isWindow(t) ? 0 === r.indexOf("outer") ? t["inner" + e] : t.document.documentElement["client" + e] : 9 === t.nodeType ? (o = t.documentElement, Math.max(t.body["scroll" + e], o["scroll" + e], t.body["offset" + e], o["offset" + e], o["client" + e])) : void 0 === i ? se.css(t, n, s) : se.style(t, n, i, s)
                }, t, a ? i : void 0, a)
            }
        })
    }), se.fn.extend({
        bind: function (e, t, n) {
            return this.on(e, null, t, n)
        }, unbind: function (e, t) {
            return this.off(e, null, t)
        }, delegate: function (e, t, n, r) {
            return this.on(t, e, n, r)
        }, undelegate: function (e, t, n) {
            return 1 === arguments.length ? this.off(e, "**") : this.off(t, e || "**", n)
        }
    }), se.holdReady = function (e) {
        e ? se.readyWait++ : se.ready(!0)
    }, se.isArray = Array.isArray, se.parseJSON = JSON.parse, se.nodeName = i, "function" == typeof define && define.amd && define("jquery", [], function () {
        return se
    });
    var Rt = e.jQuery, Mt = e.$;
    return se.noConflict = function (t) {
        return e.$ === se && (e.$ = Mt), t && e.jQuery === se && (e.jQuery = Rt), se
    }, t || (e.jQuery = e.$ = se), se
});
!function (t, e, i, s) {
    function n(e, i) {
        this.settings = null, this.options = t.extend({}, n.Defaults, i), this.$element = t(e), this._handlers = {}, this._plugins = {}, this._supress = {}, this._current = null, this._speed = null, this._coordinates = [], this._breakpoint = null, this._width = null, this._items = [], this._clones = [], this._mergers = [], this._widths = [], this._invalidated = {}, this._pipe = [], this._drag = {
            time: null,
            target: null,
            pointer: null,
            stage: {start: null, current: null},
            direction: null
        }, this._states = {
            current: {},
            tags: {initializing: ["busy"], animating: ["busy"], dragging: ["interacting"]}
        }, t.each(["onResize", "onThrottledResize"], t.proxy(function (e, i) {
            this._handlers[i] = t.proxy(this[i], this)
        }, this)), t.each(n.Plugins, t.proxy(function (t, e) {
            this._plugins[t.charAt(0).toLowerCase() + t.slice(1)] = new e(this)
        }, this)), t.each(n.Workers, t.proxy(function (e, i) {
            this._pipe.push({filter: i.filter, run: t.proxy(i.run, this)})
        }, this)), this.setup(), this.initialize()
    }

    n.Defaults = {
        items: 3,
        loop: !1,
        center: !1,
        rewind: !1,
        mouseDrag: !0,
        touchDrag: !0,
        pullDrag: !0,
        freeDrag: !1,
        margin: 0,
        stagePadding: 0,
        merge: !1,
        mergeFit: !0,
        autoWidth: !1,
        startPosition: 0,
        rtl: !1,
        smartSpeed: 250,
        fluidSpeed: !1,
        dragEndSpeed: !1,
        responsive: {},
        responsiveRefreshRate: 200,
        responsiveBaseElement: e,
        fallbackEasing: "swing",
        info: !1,
        nestedItemSelector: !1,
        itemElement: "div",
        stageElement: "div",
        refreshClass: "owl-refresh",
        loadedClass: "owl-loaded",
        loadingClass: "owl-loading",
        rtlClass: "owl-rtl",
        responsiveClass: "owl-responsive",
        dragClass: "owl-drag",
        itemClass: "owl-item",
        stageClass: "owl-stage",
        stageOuterClass: "owl-stage-outer",
        grabClass: "owl-grab"
    }, n.Width = {Default: "default", Inner: "inner", Outer: "outer"}, n.Type = {
        Event: "event",
        State: "state"
    }, n.Plugins = {}, n.Workers = [{
        filter: ["width", "settings"], run: function () {
            this._width = this.$element.width()
        }
    }, {
        filter: ["width", "items", "settings"], run: function (t) {
            t.current = this._items && this._items[this.relative(this._current)]
        }
    }, {
        filter: ["items", "settings"], run: function () {
            this.$stage.children(".cloned").remove()
        }
    }, {
        filter: ["width", "items", "settings"], run: function (t) {
            var e = this.settings.margin || "", i = !this.settings.autoWidth, s = this.settings.rtl,
                n = {width: "auto", "margin-left": s ? e : "", "margin-right": s ? "" : e};
            !i && this.$stage.children().css(n), t.css = n
        }
    }, {
        filter: ["width", "items", "settings"], run: function (t) {
            var e = (this.width() / this.settings.items).toFixed(3) - this.settings.margin, i = null,
                s = this._items.length, n = !this.settings.autoWidth, o = [];
            for (t.items = {
                merge: !1,
                width: e
            }; s--;) i = this._mergers[s], i = this.settings.mergeFit && Math.min(i, this.settings.items) || i, t.items.merge = i > 1 || t.items.merge, o[s] = n ? e * i : this._items[s].width();
            this._widths = o
        }
    }, {
        filter: ["items", "settings"], run: function () {
            var e = [], i = this._items, s = this.settings, n = Math.max(2 * s.items, 4),
                o = 2 * Math.ceil(i.length / 2), r = s.loop && i.length ? s.rewind ? n : Math.max(n, o) : 0, a = "",
                h = "";
            for (r /= 2; r--;) e.push(this.normalize(e.length / 2, !0)), a += i[e[e.length - 1]][0].outerHTML, e.push(this.normalize(i.length - 1 - (e.length - 1) / 2, !0)), h = i[e[e.length - 1]][0].outerHTML + h;
            this._clones = e, t(a).addClass("cloned").appendTo(this.$stage), t(h).addClass("cloned").prependTo(this.$stage)
        }
    }, {
        filter: ["width", "items", "settings"], run: function () {
            for (var t = this.settings.rtl ? 1 : -1, e = this._clones.length + this._items.length, i = -1, s = 0, n = 0, o = []; ++i < e;) s = o[i - 1] || 0, n = this._widths[this.relative(i)] + this.settings.margin, o.push(s + n * t);
            this._coordinates = o
        }
    }, {
        filter: ["width", "items", "settings"], run: function () {
            var t = this.settings.stagePadding, e = this._coordinates, i = {
                width: Math.ceil(Math.abs(e[e.length - 1])) + 2 * t,
                "padding-left": t || "",
                "padding-right": t || ""
            };
            this.$stage.css(i)
        }
    }, {
        filter: ["width", "items", "settings"], run: function (t) {
            var e = this._coordinates.length, i = !this.settings.autoWidth, s = this.$stage.children();
            if (i && t.items.merge) for (; e--;) t.css.width = this._widths[this.relative(e)], s.eq(e).css(t.css); else i && (t.css.width = t.items.width, s.css(t.css))
        }
    }, {
        filter: ["items"], run: function () {
            this._coordinates.length < 1 && this.$stage.removeAttr("style")
        }
    }, {
        filter: ["width", "items", "settings"], run: function (t) {
            t.current = t.current ? this.$stage.children().index(t.current) : 0, t.current = Math.max(this.minimum(), Math.min(this.maximum(), t.current)), this.reset(t.current)
        }
    }, {
        filter: ["position"], run: function () {
            this.animate(this.coordinates(this._current))
        }
    }, {
        filter: ["width", "position", "items", "settings"], run: function () {
            var t, e, i, s, n = this.settings.rtl ? 1 : -1, o = 2 * this.settings.stagePadding,
                r = this.coordinates(this.current()) + o, a = r + this.width() * n, h = [];
            for (i = 0, s = this._coordinates.length; s > i; i++) t = this._coordinates[i - 1] || 0, e = Math.abs(this._coordinates[i]) + o * n, (this.op(t, "<=", r) && this.op(t, ">", a) || this.op(e, "<", r) && this.op(e, ">", a)) && h.push(i);
            this.$stage.children(".active").removeClass("active"), this.$stage.children(":eq(" + h.join("), :eq(") + ")").addClass("active"), this.settings.center && (this.$stage.children(".center").removeClass("center"), this.$stage.children().eq(this.current()).addClass("center"))
        }
    }], n.prototype.initialize = function () {
        if (this.enter("initializing"), this.trigger("initialize"), this.$element.toggleClass(this.settings.rtlClass, this.settings.rtl), this.settings.autoWidth && !this.is("pre-loading")) {
            var e, i, s;
            e = this.$element.find("img"), i = this.settings.nestedItemSelector ? "." + this.settings.nestedItemSelector : void 0, s = this.$element.children(i).width(), e.length && 0 >= s && this.preloadAutoWidthImages(e)
        }
        this.$element.addClass(this.options.loadingClass), this.$stage = t("<" + this.settings.stageElement + ' class="' + this.settings.stageClass + '"/>').wrap('<div class="' + this.settings.stageOuterClass + '"/>'), this.$element.append(this.$stage.parent()), this.replace(this.$element.children().not(this.$stage.parent())), this.$element.is(":visible") ? this.refresh() : this.invalidate("width"), this.$element.removeClass(this.options.loadingClass).addClass(this.options.loadedClass), this.registerEventHandlers(), this.leave("initializing"), this.trigger("initialized")
    }, n.prototype.setup = function () {
        var e = this.viewport(), i = this.options.responsive, s = -1, n = null;
        i ? (t.each(i, function (t) {
            e >= t && t > s && (s = Number(t))
        }), "function" == typeof(n = t.extend({}, this.options, i[s])).stagePadding && (n.stagePadding = n.stagePadding()), delete n.responsive, n.responsiveClass && this.$element.attr("class", this.$element.attr("class").replace(new RegExp("(" + this.options.responsiveClass + "-)\\S+\\s", "g"), "$1" + s))) : n = t.extend({}, this.options), this.trigger("change", {
            property: {
                name: "settings",
                value: n
            }
        }), this._breakpoint = s, this.settings = n, this.invalidate("settings"), this.trigger("changed", {
            property: {
                name: "settings",
                value: this.settings
            }
        })
    }, n.prototype.optionsLogic = function () {
        this.settings.autoWidth && (this.settings.stagePadding = !1, this.settings.merge = !1)
    }, n.prototype.prepare = function (e) {
        var i = this.trigger("prepare", {content: e});
        return i.data || (i.data = t("<" + this.settings.itemElement + "/>").addClass(this.options.itemClass).append(e)), this.trigger("prepared", {content: i.data}), i.data
    }, n.prototype.update = function () {
        for (var e = 0, i = this._pipe.length, s = t.proxy(function (t) {
            return this[t]
        }, this._invalidated), n = {}; i > e;) (this._invalidated.all || t.grep(this._pipe[e].filter, s).length > 0) && this._pipe[e].run(n), e++;
        this._invalidated = {}, !this.is("valid") && this.enter("valid")
    }, n.prototype.width = function (t) {
        switch (t = t || n.Width.Default) {
            case n.Width.Inner:
            case n.Width.Outer:
                return this._width;
            default:
                return this._width - 2 * this.settings.stagePadding + this.settings.margin
        }
    }, n.prototype.refresh = function () {
        this.enter("refreshing"), this.trigger("refresh"), this.setup(), this.optionsLogic(), this.$element.addClass(this.options.refreshClass), this.update(), this.$element.removeClass(this.options.refreshClass), this.leave("refreshing"), this.trigger("refreshed")
    }, n.prototype.onThrottledResize = function () {
        e.clearTimeout(this.resizeTimer), this.resizeTimer = e.setTimeout(this._handlers.onResize, this.settings.responsiveRefreshRate)
    }, n.prototype.onResize = function () {
        return !!this._items.length && (this._width !== this.$element.width() && (!!this.$element.is(":visible") && (this.enter("resizing"), this.trigger("resize").isDefaultPrevented() ? (this.leave("resizing"), !1) : (this.invalidate("width"), this.refresh(), this.leave("resizing"), void this.trigger("resized")))))
    }, n.prototype.registerEventHandlers = function () {
        t.support.transition && this.$stage.on(t.support.transition.end + ".owl.core", t.proxy(this.onTransitionEnd, this)), !1 !== this.settings.responsive && this.on(e, "resize", this._handlers.onThrottledResize), this.settings.mouseDrag && (this.$element.addClass(this.options.dragClass), this.$stage.on("mousedown.owl.core", t.proxy(this.onDragStart, this)), this.$stage.on("dragstart.owl.core selectstart.owl.core", function () {
            return !1
        })), this.settings.touchDrag && (this.$stage.on("touchstart.owl.core", t.proxy(this.onDragStart, this)), this.$stage.on("touchcancel.owl.core", t.proxy(this.onDragEnd, this)))
    }, n.prototype.onDragStart = function (e) {
        var s = null;
        3 !== e.which && (t.support.transform ? (s = this.$stage.css("transform").replace(/.*\(|\)| /g, "").split(","), s = {
            x: s[16 === s.length ? 12 : 4],
            y: s[16 === s.length ? 13 : 5]
        }) : (s = this.$stage.position(), s = {
            x: this.settings.rtl ? s.left + this.$stage.width() - this.width() + this.settings.margin : s.left,
            y: s.top
        }), this.is("animating") && (t.support.transform ? this.animate(s.x) : this.$stage.stop(), this.invalidate("position")), this.$element.toggleClass(this.options.grabClass, "mousedown" === e.type), this.speed(0), this._drag.time = (new Date).getTime(), this._drag.target = t(e.target), this._drag.stage.start = s, this._drag.stage.current = s, this._drag.pointer = this.pointer(e), t(i).on("mouseup.owl.core touchend.owl.core", t.proxy(this.onDragEnd, this)), t(i).one("mousemove.owl.core touchmove.owl.core", t.proxy(function (e) {
            var s = this.difference(this._drag.pointer, this.pointer(e));
            t(i).on("mousemove.owl.core touchmove.owl.core", t.proxy(this.onDragMove, this)), Math.abs(s.x) < Math.abs(s.y) && this.is("valid") || (e.preventDefault(), this.enter("dragging"), this.trigger("drag"))
        }, this)))
    }, n.prototype.onDragMove = function (t) {
        var e = null, i = null, s = null, n = this.difference(this._drag.pointer, this.pointer(t)),
            o = this.difference(this._drag.stage.start, n);
        this.is("dragging") && (t.preventDefault(), this.settings.loop ? (e = this.coordinates(this.minimum()), i = this.coordinates(this.maximum() + 1) - e, o.x = ((o.x - e) % i + i) % i + e) : (e = this.settings.rtl ? this.coordinates(this.maximum()) : this.coordinates(this.minimum()), i = this.settings.rtl ? this.coordinates(this.minimum()) : this.coordinates(this.maximum()), s = this.settings.pullDrag ? -1 * n.x / 5 : 0, o.x = Math.max(Math.min(o.x, e + s), i + s)), this._drag.stage.current = o, this.animate(o.x))
    }, n.prototype.onDragEnd = function (e) {
        var s = this.difference(this._drag.pointer, this.pointer(e)), n = this._drag.stage.current,
            o = s.x > 0 ^ this.settings.rtl ? "left" : "right";
        t(i).off(".owl.core"), this.$element.removeClass(this.options.grabClass), (0 !== s.x && this.is("dragging") || !this.is("valid")) && (this.speed(this.settings.dragEndSpeed || this.settings.smartSpeed), this.current(this.closest(n.x, 0 !== s.x ? o : this._drag.direction)), this.invalidate("position"), this.update(), this._drag.direction = o, (Math.abs(s.x) > 3 || (new Date).getTime() - this._drag.time > 300) && this._drag.target.one("click.owl.core", function () {
            return !1
        })), this.is("dragging") && (this.leave("dragging"), this.trigger("dragged"))
    }, n.prototype.closest = function (e, i) {
        var s = -1, n = this.width(), o = this.coordinates();
        return this.settings.freeDrag || t.each(o, t.proxy(function (t, r) {
            return "left" === i && e > r - 30 && r + 30 > e ? s = t : "right" === i && e > r - n - 30 && r - n + 30 > e ? s = t + 1 : this.op(e, "<", r) && this.op(e, ">", o[t + 1] || r - n) && (s = "left" === i ? t + 1 : t), -1 === s
        }, this)), this.settings.loop || (this.op(e, ">", o[this.minimum()]) ? s = e = this.minimum() : this.op(e, "<", o[this.maximum()]) && (s = e = this.maximum())), s
    }, n.prototype.animate = function (e) {
        var i = this.speed() > 0;
        this.is("animating") && this.onTransitionEnd(), i && (this.enter("animating"), this.trigger("translate")), t.support.transform3d && t.support.transition ? this.$stage.css({
            transform: "translate3d(" + e + "px,0px,0px)",
            transition: this.speed() / 1e3 + "s"
        }) : i ? this.$stage.animate({left: e + "px"}, this.speed(), this.settings.fallbackEasing, t.proxy(this.onTransitionEnd, this)) : this.$stage.css({left: e + "px"})
    }, n.prototype.is = function (t) {
        return this._states.current[t] && this._states.current[t] > 0
    }, n.prototype.current = function (t) {
        if (void 0 === t) return this._current;
        if (0 !== this._items.length) {
            if (t = this.normalize(t), this._current !== t) {
                var e = this.trigger("change", {property: {name: "position", value: t}});
                void 0 !== e.data && (t = this.normalize(e.data)), this._current = t, this.invalidate("position"), this.trigger("changed", {
                    property: {
                        name: "position",
                        value: this._current
                    }
                })
            }
            return this._current
        }
    }, n.prototype.invalidate = function (e) {
        return "string" === t.type(e) && (this._invalidated[e] = !0, this.is("valid") && this.leave("valid")), t.map(this._invalidated, function (t, e) {
            return e
        })
    }, n.prototype.reset = function (t) {
        void 0 !== (t = this.normalize(t)) && (this._speed = 0, this._current = t, this.suppress(["translate", "translated"]), this.animate(this.coordinates(t)), this.release(["translate", "translated"]))
    }, n.prototype.normalize = function (t, e) {
        var i = this._items.length, s = e ? 0 : this._clones.length;
        return !this.isNumeric(t) || 1 > i ? t = void 0 : (0 > t || t >= i + s) && (t = ((t - s / 2) % i + i) % i + s / 2), t
    }, n.prototype.relative = function (t) {
        return t -= this._clones.length / 2, this.normalize(t, !0)
    }, n.prototype.maximum = function (t) {
        var e, i, s, n = this.settings, o = this._coordinates.length;
        if (n.loop) o = this._clones.length / 2 + this._items.length - 1; else if (n.autoWidth || n.merge) {
            for (e = this._items.length, i = this._items[--e].width(), s = this.$element.width(); e-- && !((i += this._items[e].width() + this.settings.margin) > s);) ;
            o = e + 1
        } else o = n.center ? this._items.length - 1 : this._items.length - n.items;
        return t && (o -= this._clones.length / 2), Math.max(o, 0)
    }, n.prototype.minimum = function (t) {
        return t ? 0 : this._clones.length / 2
    }, n.prototype.items = function (t) {
        return void 0 === t ? this._items.slice() : (t = this.normalize(t, !0), this._items[t])
    }, n.prototype.mergers = function (t) {
        return void 0 === t ? this._mergers.slice() : (t = this.normalize(t, !0), this._mergers[t])
    }, n.prototype.clones = function (e) {
        var i = this._clones.length / 2, s = i + this._items.length, n = function (t) {
            return t % 2 == 0 ? s + t / 2 : i - (t + 1) / 2
        };
        return void 0 === e ? t.map(this._clones, function (t, e) {
            return n(e)
        }) : t.map(this._clones, function (t, i) {
            return t === e ? n(i) : null
        })
    }, n.prototype.speed = function (t) {
        return void 0 !== t && (this._speed = t), this._speed
    }, n.prototype.coordinates = function (e) {
        var i, s = 1, n = e - 1;
        return void 0 === e ? t.map(this._coordinates, t.proxy(function (t, e) {
            return this.coordinates(e)
        }, this)) : (this.settings.center ? (this.settings.rtl && (s = -1, n = e + 1), i = this._coordinates[e], i += (this.width() - i + (this._coordinates[n] || 0)) / 2 * s) : i = this._coordinates[n] || 0, i = Math.ceil(i))
    }, n.prototype.duration = function (t, e, i) {
        return 0 === i ? 0 : Math.min(Math.max(Math.abs(e - t), 1), 6) * Math.abs(i || this.settings.smartSpeed)
    }, n.prototype.to = function (t, e) {
        var i = this.current(), s = null, n = t - this.relative(i), o = (n > 0) - (0 > n), r = this._items.length,
            a = this.minimum(), h = this.maximum();
        this.settings.loop ? (!this.settings.rewind && Math.abs(n) > r / 2 && (n += -1 * o * r), t = i + n, (s = ((t - a) % r + r) % r + a) !== t && h >= s - n && s - n > 0 && (i = s - n, t = s, this.reset(i))) : this.settings.rewind ? (h += 1, t = (t % h + h) % h) : t = Math.max(a, Math.min(h, t)), this.speed(this.duration(i, t, e)), this.current(t), this.$element.is(":visible") && this.update()
    }, n.prototype.next = function (t) {
        t = t || !1, this.to(this.relative(this.current()) + 1, t)
    }, n.prototype.prev = function (t) {
        t = t || !1, this.to(this.relative(this.current()) - 1, t)
    }, n.prototype.onTransitionEnd = function (t) {
        return (void 0 === t || (t.stopPropagation(), (t.target || t.srcElement || t.originalTarget) === this.$stage.get(0))) && (this.leave("animating"), void this.trigger("translated"))
    }, n.prototype.viewport = function () {
        var s;
        if (this.options.responsiveBaseElement !== e) s = t(this.options.responsiveBaseElement).width(); else if (e.innerWidth) s = e.innerWidth; else {
            if (!i.documentElement || !i.documentElement.clientWidth) throw"Can not detect viewport width.";
            s = i.documentElement.clientWidth
        }
        return s
    }, n.prototype.replace = function (e) {
        this.$stage.empty(), this._items = [], e && (e = e instanceof jQuery ? e : t(e)), this.settings.nestedItemSelector && (e = e.find("." + this.settings.nestedItemSelector)), e.filter(function () {
            return 1 === this.nodeType
        }).each(t.proxy(function (t, e) {
            e = this.prepare(e), this.$stage.append(e), this._items.push(e), this._mergers.push(1 * e.find("[data-merge]").addBack("[data-merge]").attr("data-merge") || 1)
        }, this)), this.reset(this.isNumeric(this.settings.startPosition) ? this.settings.startPosition : 0), this.invalidate("items")
    }, n.prototype.add = function (e, i) {
        var s = this.relative(this._current);
        i = void 0 === i ? this._items.length : this.normalize(i, !0), e = e instanceof jQuery ? e : t(e), this.trigger("add", {
            content: e,
            position: i
        }), e = this.prepare(e), 0 === this._items.length || i === this._items.length ? (0 === this._items.length && this.$stage.append(e), 0 !== this._items.length && this._items[i - 1].after(e), this._items.push(e), this._mergers.push(1 * e.find("[data-merge]").addBack("[data-merge]").attr("data-merge") || 1)) : (this._items[i].before(e), this._items.splice(i, 0, e), this._mergers.splice(i, 0, 1 * e.find("[data-merge]").addBack("[data-merge]").attr("data-merge") || 1)), this._items[s] && this.reset(this._items[s].index()), this.invalidate("items"), this.trigger("added", {
            content: e,
            position: i
        })
    }, n.prototype.remove = function (t) {
        void 0 !== (t = this.normalize(t, !0)) && (this.trigger("remove", {
            content: this._items[t],
            position: t
        }), this._items[t].remove(), this._items.splice(t, 1), this._mergers.splice(t, 1), this.invalidate("items"), this.trigger("removed", {
            content: null,
            position: t
        }))
    }, n.prototype.preloadAutoWidthImages = function (e) {
        e.each(t.proxy(function (e, i) {
            this.enter("pre-loading"), i = t(i), t(new Image).one("load", t.proxy(function (t) {
                i.attr("src", t.target.src), i.css("opacity", 1), this.leave("pre-loading"), !this.is("pre-loading") && !this.is("initializing") && this.refresh()
            }, this)).attr("src", i.attr("src") || i.attr("data-src") || i.attr("data-src-retina"))
        }, this))
    }, n.prototype.destroy = function () {
        this.$element.off(".owl.core"), this.$stage.off(".owl.core"), t(i).off(".owl.core"), !1 !== this.settings.responsive && (e.clearTimeout(this.resizeTimer), this.off(e, "resize", this._handlers.onThrottledResize));
        for (var s in this._plugins) this._plugins[s].destroy();
        this.$stage.children(".cloned").remove(), this.$stage.unwrap(), this.$stage.children().contents().unwrap(), this.$stage.children().unwrap(), this.$element.removeClass(this.options.refreshClass).removeClass(this.options.loadingClass).removeClass(this.options.loadedClass).removeClass(this.options.rtlClass).removeClass(this.options.dragClass).removeClass(this.options.grabClass).attr("class", this.$element.attr("class").replace(new RegExp(this.options.responsiveClass + "-\\S+\\s", "g"), "")).removeData("owl.carousel")
    }, n.prototype.op = function (t, e, i) {
        var s = this.settings.rtl;
        switch (e) {
            case"<":
                return s ? t > i : i > t;
            case">":
                return s ? i > t : t > i;
            case">=":
                return s ? i >= t : t >= i;
            case"<=":
                return s ? t >= i : i >= t
        }
    }, n.prototype.on = function (t, e, i, s) {
        t.addEventListener ? t.addEventListener(e, i, s) : t.attachEvent && t.attachEvent("on" + e, i)
    }, n.prototype.off = function (t, e, i, s) {
        t.removeEventListener ? t.removeEventListener(e, i, s) : t.detachEvent && t.detachEvent("on" + e, i)
    }, n.prototype.trigger = function (e, i, s, o, r) {
        var a = {item: {count: this._items.length, index: this.current()}},
            h = t.camelCase(t.grep(["on", e, s], function (t) {
                return t
            }).join("-").toLowerCase()),
            l = t.Event([e, "owl", s || "carousel"].join(".").toLowerCase(), t.extend({relatedTarget: this}, a, i));
        return this._supress[e] || (t.each(this._plugins, function (t, e) {
            e.onTrigger && e.onTrigger(l)
        }), this.register({
            type: n.Type.Event,
            name: e
        }), this.$element.trigger(l), this.settings && "function" == typeof this.settings[h] && this.settings[h].call(this, l)), l
    }, n.prototype.enter = function (e) {
        t.each([e].concat(this._states.tags[e] || []), t.proxy(function (t, e) {
            void 0 === this._states.current[e] && (this._states.current[e] = 0), this._states.current[e]++
        }, this))
    }, n.prototype.leave = function (e) {
        t.each([e].concat(this._states.tags[e] || []), t.proxy(function (t, e) {
            this._states.current[e]--
        }, this))
    }, n.prototype.register = function (e) {
        if (e.type === n.Type.Event) {
            if (t.event.special[e.name] || (t.event.special[e.name] = {}), !t.event.special[e.name].owl) {
                var i = t.event.special[e.name]._default;
                t.event.special[e.name]._default = function (t) {
                    return !i || !i.apply || t.namespace && -1 !== t.namespace.indexOf("owl") ? t.namespace && t.namespace.indexOf("owl") > -1 : i.apply(this, arguments)
                }, t.event.special[e.name].owl = !0
            }
        } else e.type === n.Type.State && (this._states.tags[e.name] ? this._states.tags[e.name] = this._states.tags[e.name].concat(e.tags) : this._states.tags[e.name] = e.tags, this._states.tags[e.name] = t.grep(this._states.tags[e.name], t.proxy(function (i, s) {
            return t.inArray(i, this._states.tags[e.name]) === s
        }, this)))
    }, n.prototype.suppress = function (e) {
        t.each(e, t.proxy(function (t, e) {
            this._supress[e] = !0
        }, this))
    }, n.prototype.release = function (e) {
        t.each(e, t.proxy(function (t, e) {
            delete this._supress[e]
        }, this))
    }, n.prototype.pointer = function (t) {
        var i = {x: null, y: null};
        return t = t.originalEvent || t || e.event, (t = t.touches && t.touches.length ? t.touches[0] : t.changedTouches && t.changedTouches.length ? t.changedTouches[0] : t).pageX ? (i.x = t.pageX, i.y = t.pageY) : (i.x = t.clientX, i.y = t.clientY), i
    }, n.prototype.isNumeric = function (t) {
        return !isNaN(parseFloat(t))
    }, n.prototype.difference = function (t, e) {
        return {x: t.x - e.x, y: t.y - e.y}
    }, t.fn.owlCarousel = function (e) {
        var i = Array.prototype.slice.call(arguments, 1);
        return this.each(function () {
            var s = t(this), o = s.data("owl.carousel");
            o || (o = new n(this, "object" == typeof e && e), s.data("owl.carousel", o), t.each(["next", "prev", "to", "destroy", "refresh", "replace", "add", "remove"], function (e, i) {
                o.register({
                    type: n.Type.Event,
                    name: i
                }), o.$element.on(i + ".owl.carousel.core", t.proxy(function (t) {
                    t.namespace && t.relatedTarget !== this && (this.suppress([i]), o[i].apply(this, [].slice.call(arguments, 1)), this.release([i]))
                }, o))
            })), "string" == typeof e && "_" !== e.charAt(0) && o[e].apply(o, i)
        })
    }, t.fn.owlCarousel.Constructor = n
}(window.Zepto || window.jQuery, window, document), function (t, e, i, s) {
    var n = function (e) {
        this._core = e, this._interval = null, this._visible = null, this._handlers = {
            "initialized.owl.carousel": t.proxy(function (t) {
                t.namespace && this._core.settings.autoRefresh && this.watch()
            }, this)
        }, this._core.options = t.extend({}, n.Defaults, this._core.options), this._core.$element.on(this._handlers)
    };
    n.Defaults = {autoRefresh: !0, autoRefreshInterval: 500}, n.prototype.watch = function () {
        this._interval || (this._visible = this._core.$element.is(":visible"), this._interval = e.setInterval(t.proxy(this.refresh, this), this._core.settings.autoRefreshInterval))
    }, n.prototype.refresh = function () {
        this._core.$element.is(":visible") !== this._visible && (this._visible = !this._visible, this._core.$element.toggleClass("owl-hidden", !this._visible), this._visible && this._core.invalidate("width") && this._core.refresh())
    }, n.prototype.destroy = function () {
        var t, i;
        e.clearInterval(this._interval);
        for (t in this._handlers) this._core.$element.off(t, this._handlers[t]);
        for (i in Object.getOwnPropertyNames(this)) "function" != typeof this[i] && (this[i] = null)
    }, t.fn.owlCarousel.Constructor.Plugins.AutoRefresh = n
}(window.Zepto || window.jQuery, window, document), function (t, e, i, s) {
    var n = function (e) {
        this._core = e, this._loaded = [], this._handlers = {
            "initialized.owl.carousel change.owl.carousel resized.owl.carousel": t.proxy(function (e) {
                if (e.namespace && this._core.settings && this._core.settings.lazyLoad && (e.property && "position" == e.property.name || "initialized" == e.type)) for (var i = this._core.settings, s = i.center && Math.ceil(i.items / 2) || i.items, n = i.center && -1 * s || 0, o = (e.property && void 0 !== e.property.value ? e.property.value : this._core.current()) + n, r = this._core.clones().length, a = t.proxy(function (t, e) {
                    this.load(e)
                }, this); n++ < s;) this.load(r / 2 + this._core.relative(o)), r && t.each(this._core.clones(this._core.relative(o)), a), o++
            }, this)
        }, this._core.options = t.extend({}, n.Defaults, this._core.options), this._core.$element.on(this._handlers)
    };
    n.Defaults = {lazyLoad: !1}, n.prototype.load = function (i) {
        var s = this._core.$stage.children().eq(i), n = s && s.find(".owl-lazy");
        !n || t.inArray(s.get(0), this._loaded) > -1 || (n.each(t.proxy(function (i, s) {
            var n, o = t(s), r = e.devicePixelRatio > 1 && o.attr("data-src-retina") || o.attr("data-src");
            this._core.trigger("load", {
                element: o,
                url: r
            }, "lazy"), o.is("img") ? o.one("load.owl.lazy", t.proxy(function () {
                o.css("opacity", 1), this._core.trigger("loaded", {element: o, url: r}, "lazy")
            }, this)).attr("src", r) : (n = new Image, n.onload = t.proxy(function () {
                o.css({"background-image": "url(" + r + ")", opacity: "1"}), this._core.trigger("loaded", {
                    element: o,
                    url: r
                }, "lazy")
            }, this), n.src = r)
        }, this)), this._loaded.push(s.get(0)))
    }, n.prototype.destroy = function () {
        var t, e;
        for (t in this.handlers) this._core.$element.off(t, this.handlers[t]);
        for (e in Object.getOwnPropertyNames(this)) "function" != typeof this[e] && (this[e] = null)
    }, t.fn.owlCarousel.Constructor.Plugins.Lazy = n
}(window.Zepto || window.jQuery, window, document), function (t, e, i, s) {
    var n = function (e) {
        this._core = e, this._handlers = {
            "initialized.owl.carousel refreshed.owl.carousel": t.proxy(function (t) {
                t.namespace && this._core.settings.autoHeight && this.update()
            }, this), "changed.owl.carousel": t.proxy(function (t) {
                t.namespace && this._core.settings.autoHeight && "position" == t.property.name && this.update()
            }, this), "loaded.owl.lazy": t.proxy(function (t) {
                t.namespace && this._core.settings.autoHeight && t.element.closest("." + this._core.settings.itemClass).index() === this._core.current() && this.update()
            }, this)
        }, this._core.options = t.extend({}, n.Defaults, this._core.options), this._core.$element.on(this._handlers)
    };
    n.Defaults = {autoHeight: !1, autoHeightClass: "owl-height"}, n.prototype.update = function () {
        var e = this._core._current, i = e + this._core.settings.items,
            s = this._core.$stage.children().toArray().slice(e, i), n = [], o = 0;
        t.each(s, function (e, i) {
            n.push(t(i).height())
        }), o = Math.max.apply(null, n), this._core.$stage.parent().height(o).addClass(this._core.settings.autoHeightClass)
    }, n.prototype.destroy = function () {
        var t, e;
        for (t in this._handlers) this._core.$element.off(t, this._handlers[t]);
        for (e in Object.getOwnPropertyNames(this)) "function" != typeof this[e] && (this[e] = null)
    }, t.fn.owlCarousel.Constructor.Plugins.AutoHeight = n
}(window.Zepto || window.jQuery, window, document), function (t, e, i, s) {
    var n = function (e) {
        this._core = e, this._videos = {}, this._playing = null, this._handlers = {
            "initialized.owl.carousel": t.proxy(function (t) {
                t.namespace && this._core.register({type: "state", name: "playing", tags: ["interacting"]})
            }, this), "resize.owl.carousel": t.proxy(function (t) {
                t.namespace && this._core.settings.video && this.isInFullScreen() && t.preventDefault()
            }, this), "refreshed.owl.carousel": t.proxy(function (t) {
                t.namespace && this._core.is("resizing") && this._core.$stage.find(".cloned .owl-video-frame").remove()
            }, this), "changed.owl.carousel": t.proxy(function (t) {
                t.namespace && "position" === t.property.name && this._playing && this.stop()
            }, this), "prepared.owl.carousel": t.proxy(function (e) {
                if (e.namespace) {
                    var i = t(e.content).find(".owl-video");
                    i.length && (i.css("display", "none"), this.fetch(i, t(e.content)))
                }
            }, this)
        }, this._core.options = t.extend({}, n.Defaults, this._core.options), this._core.$element.on(this._handlers), this._core.$element.on("click.owl.video", ".owl-video-play-icon", t.proxy(function (t) {
            this.play(t)
        }, this))
    };
    n.Defaults = {video: !1, videoHeight: !1, videoWidth: !1}, n.prototype.fetch = function (t, e) {
        var i = t.attr("data-vimeo-id") ? "vimeo" : t.attr("data-vzaar-id") ? "vzaar" : "youtube",
            s = t.attr("data-vimeo-id") || t.attr("data-youtube-id") || t.attr("data-vzaar-id"),
            n = t.attr("data-width") || this._core.settings.videoWidth,
            o = t.attr("data-height") || this._core.settings.videoHeight, r = t.attr("href");
        if (!r) throw new Error("Missing video URL.");
        if ((s = r.match(/(http:|https:|)\/\/(player.|www.|app.)?(vimeo\.com|youtu(be\.com|\.be|be\.googleapis\.com)|vzaar\.com)\/(video\/|videos\/|embed\/|channels\/.+\/|groups\/.+\/|watch\?v=|v\/)?([A-Za-z0-9._%-]*)(\&\S+)?/))[3].indexOf("youtu") > -1) i = "youtube"; else if (s[3].indexOf("vimeo") > -1) i = "vimeo"; else {
            if (!(s[3].indexOf("vzaar") > -1)) throw new Error("Video URL not supported.");
            i = "vzaar"
        }
        s = s[6], this._videos[r] = {
            type: i,
            id: s,
            width: n,
            height: o
        }, e.attr("data-video", r), this.thumbnail(t, this._videos[r])
    }, n.prototype.thumbnail = function (e, i) {
        var s, n, o, r = i.width && i.height ? 'style="width:' + i.width + "px;height:" + i.height + 'px;"' : "",
            a = e.find("img"), h = "src", l = "", c = this._core.settings, p = function (t) {
                n = '<div class="owl-video-play-icon"></div>', s = c.lazyLoad ? '<div class="owl-video-tn ' + l + '" ' + h + '="' + t + '"></div>' : '<div class="owl-video-tn" style="opacity:1;background-image:url(' + t + ')"></div>', e.after(s), e.after(n)
            };
        return e.wrap('<div class="owl-video-wrapper"' + r + "></div>"), this._core.settings.lazyLoad && (h = "data-src", l = "owl-lazy"), a.length ? (p(a.attr(h)), a.remove(), !1) : void("youtube" === i.type ? (o = "//img.youtube.com/vi/" + i.id + "/hqdefault.jpg", p(o)) : "vimeo" === i.type ? t.ajax({
            type: "GET",
            url: "//vimeo.com/api/v2/video/" + i.id + ".json",
            jsonp: "callback",
            dataType: "jsonp",
            success: function (t) {
                o = t[0].thumbnail_large, p(o)
            }
        }) : "vzaar" === i.type && t.ajax({
            type: "GET",
            url: "//vzaar.com/api/videos/" + i.id + ".json",
            jsonp: "callback",
            dataType: "jsonp",
            success: function (t) {
                o = t.framegrab_url, p(o)
            }
        }))
    }, n.prototype.stop = function () {
        this._core.trigger("stop", null, "video"), this._playing.find(".owl-video-frame").remove(), this._playing.removeClass("owl-video-playing"), this._playing = null, this._core.leave("playing"), this._core.trigger("stopped", null, "video")
    }, n.prototype.play = function (e) {
        var i, s = t(e.target).closest("." + this._core.settings.itemClass), n = this._videos[s.attr("data-video")],
            o = n.width || "100%", r = n.height || this._core.$stage.height();
        this._playing || (this._core.enter("playing"), this._core.trigger("play", null, "video"), s = this._core.items(this._core.relative(s.index())), this._core.reset(s.index()), "youtube" === n.type ? i = '<iframe width="' + o + '" height="' + r + '" src="//www.youtube.com/embed/' + n.id + "?autoplay=1&v=" + n.id + '" frameborder="0" allowfullscreen></iframe>' : "vimeo" === n.type ? i = '<iframe src="//player.vimeo.com/video/' + n.id + '?autoplay=1" width="' + o + '" height="' + r + '" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>' : "vzaar" === n.type && (i = '<iframe frameborder="0"height="' + r + '"width="' + o + '" allowfullscreen mozallowfullscreen webkitAllowFullScreen src="//view.vzaar.com/' + n.id + '/player?autoplay=true"></iframe>'), t('<div class="owl-video-frame">' + i + "</div>").insertAfter(s.find(".owl-video")), this._playing = s.addClass("owl-video-playing"))
    }, n.prototype.isInFullScreen = function () {
        var e = i.fullscreenElement || i.mozFullScreenElement || i.webkitFullscreenElement;
        return e && t(e).parent().hasClass("owl-video-frame")
    }, n.prototype.destroy = function () {
        var t, e;
        this._core.$element.off("click.owl.video");
        for (t in this._handlers) this._core.$element.off(t, this._handlers[t]);
        for (e in Object.getOwnPropertyNames(this)) "function" != typeof this[e] && (this[e] = null)
    }, t.fn.owlCarousel.Constructor.Plugins.Video = n
}(window.Zepto || window.jQuery, window, document), function (t, e, i, s) {
    var n = function (e) {
        this.core = e, this.core.options = t.extend({}, n.Defaults, this.core.options), this.swapping = !0, this.previous = void 0, this.next = void 0, this.handlers = {
            "change.owl.carousel": t.proxy(function (t) {
                t.namespace && "position" == t.property.name && (this.previous = this.core.current(), this.next = t.property.value)
            }, this), "drag.owl.carousel dragged.owl.carousel translated.owl.carousel": t.proxy(function (t) {
                t.namespace && (this.swapping = "translated" == t.type)
            }, this), "translate.owl.carousel": t.proxy(function (t) {
                t.namespace && this.swapping && (this.core.options.animateOut || this.core.options.animateIn) && this.swap()
            }, this)
        }, this.core.$element.on(this.handlers)
    };
    n.Defaults = {animateOut: !1, animateIn: !1}, n.prototype.swap = function () {
        if (1 === this.core.settings.items && t.support.animation && t.support.transition) {
            this.core.speed(0);
            var e, i = t.proxy(this.clear, this), s = this.core.$stage.children().eq(this.previous),
                n = this.core.$stage.children().eq(this.next), o = this.core.settings.animateIn,
                r = this.core.settings.animateOut;
            this.core.current() !== this.previous && (r && (e = this.core.coordinates(this.previous) - this.core.coordinates(this.next), s.one(t.support.animation.end, i).css({left: e + "px"}).addClass("animated owl-animated-out").addClass(r)), o && n.one(t.support.animation.end, i).addClass("animated owl-animated-in").addClass(o))
        }
    }, n.prototype.clear = function (e) {
        t(e.target).css({left: ""}).removeClass("animated owl-animated-out owl-animated-in").removeClass(this.core.settings.animateIn).removeClass(this.core.settings.animateOut), this.core.onTransitionEnd()
    }, n.prototype.destroy = function () {
        var t, e;
        for (t in this.handlers) this.core.$element.off(t, this.handlers[t]);
        for (e in Object.getOwnPropertyNames(this)) "function" != typeof this[e] && (this[e] = null)
    }, t.fn.owlCarousel.Constructor.Plugins.Animate = n
}(window.Zepto || window.jQuery, window, document), function (t, e, i, s) {
    var n = function (e) {
        this._core = e, this._timeout = null, this._paused = !1, this._handlers = {
            "changed.owl.carousel": t.proxy(function (t) {
                t.namespace && "settings" === t.property.name ? this._core.settings.autoplay ? this.play() : this.stop() : t.namespace && "position" === t.property.name && this._core.settings.autoplay && this._setAutoPlayInterval()
            }, this), "initialized.owl.carousel": t.proxy(function (t) {
                t.namespace && this._core.settings.autoplay && this.play()
            }, this), "play.owl.autoplay": t.proxy(function (t, e, i) {
                t.namespace && this.play(e, i)
            }, this), "stop.owl.autoplay": t.proxy(function (t) {
                t.namespace && this.stop()
            }, this), "mouseover.owl.autoplay": t.proxy(function () {
                this._core.settings.autoplayHoverPause && this._core.is("rotating") && this.pause()
            }, this), "mouseleave.owl.autoplay": t.proxy(function () {
                this._core.settings.autoplayHoverPause && this._core.is("rotating") && this.play()
            }, this), "touchstart.owl.core": t.proxy(function () {
                this._core.settings.autoplayHoverPause && this._core.is("rotating") && this.pause()
            }, this), "touchend.owl.core": t.proxy(function () {
                this._core.settings.autoplayHoverPause && this.play()
            }, this)
        }, this._core.$element.on(this._handlers), this._core.options = t.extend({}, n.Defaults, this._core.options)
    };
    n.Defaults = {
        autoplay: !1,
        autoplayTimeout: 5e3,
        autoplayHoverPause: !1,
        autoplaySpeed: !1
    }, n.prototype.play = function (t, e) {
        this._paused = !1, this._core.is("rotating") || (this._core.enter("rotating"), this._setAutoPlayInterval())
    }, n.prototype._getNextTimeout = function (s, n) {
        return this._timeout && e.clearTimeout(this._timeout), e.setTimeout(t.proxy(function () {
            this._paused || this._core.is("busy") || this._core.is("interacting") || i.hidden || this._core.next(n || this._core.settings.autoplaySpeed)
        }, this), s || this._core.settings.autoplayTimeout)
    }, n.prototype._setAutoPlayInterval = function () {
        this._timeout = this._getNextTimeout()
    }, n.prototype.stop = function () {
        this._core.is("rotating") && (e.clearTimeout(this._timeout), this._core.leave("rotating"))
    }, n.prototype.pause = function () {
        this._core.is("rotating") && (this._paused = !0)
    }, n.prototype.destroy = function () {
        var t, e;
        this.stop();
        for (t in this._handlers) this._core.$element.off(t, this._handlers[t]);
        for (e in Object.getOwnPropertyNames(this)) "function" != typeof this[e] && (this[e] = null)
    }, t.fn.owlCarousel.Constructor.Plugins.autoplay = n
}(window.Zepto || window.jQuery, window, document), function (t, e, i, s) {
    "use strict";
    var n = function (e) {
        this._core = e, this._initialized = !1, this._pages = [], this._controls = {}, this._templates = [], this.$element = this._core.$element, this._overrides = {
            next: this._core.next,
            prev: this._core.prev,
            to: this._core.to
        }, this._handlers = {
            "prepared.owl.carousel": t.proxy(function (e) {
                e.namespace && this._core.settings.dotsData && this._templates.push('<div class="' + this._core.settings.dotClass + '">' + t(e.content).find("[data-dot]").addBack("[data-dot]").attr("data-dot") + "</div>")
            }, this), "added.owl.carousel": t.proxy(function (t) {
                t.namespace && this._core.settings.dotsData && this._templates.splice(t.position, 0, this._templates.pop())
            }, this), "remove.owl.carousel": t.proxy(function (t) {
                t.namespace && this._core.settings.dotsData && this._templates.splice(t.position, 1)
            }, this), "changed.owl.carousel": t.proxy(function (t) {
                t.namespace && "position" == t.property.name && this.draw()
            }, this), "initialized.owl.carousel": t.proxy(function (t) {
                t.namespace && !this._initialized && (this._core.trigger("initialize", null, "navigation"), this.initialize(), this.update(), this.draw(), this._initialized = !0, this._core.trigger("initialized", null, "navigation"))
            }, this), "refreshed.owl.carousel": t.proxy(function (t) {
                t.namespace && this._initialized && (this._core.trigger("refresh", null, "navigation"), this.update(), this.draw(), this._core.trigger("refreshed", null, "navigation"))
            }, this)
        }, this._core.options = t.extend({}, n.Defaults, this._core.options), this.$element.on(this._handlers)
    };
    n.Defaults = {
        nav: !1,
        navText: ["prev", "next"],
        navSpeed: !1,
        navElement: "div",
        navContainer: !1,
        navContainerClass: "owl-nav",
        navClass: ["owl-prev", "owl-next"],
        slideBy: 1,
        dotClass: "owl-dot",
        dotsClass: "owl-dots",
        dots: !0,
        dotsEach: !1,
        dotsData: !1,
        dotsSpeed: !1,
        dotsContainer: !1
    }, n.prototype.initialize = function () {
        var e, i = this._core.settings;
        this._controls.$relative = (i.navContainer ? t(i.navContainer) : t("<div>").addClass(i.navContainerClass).appendTo(this.$element)).addClass("disabled"), this._controls.$previous = t("<" + i.navElement + ">").addClass(i.navClass[0]).html(i.navText[0]).prependTo(this._controls.$relative).on("click", t.proxy(function (t) {
            this.prev(i.navSpeed)
        }, this)), this._controls.$next = t("<" + i.navElement + ">").addClass(i.navClass[1]).html(i.navText[1]).appendTo(this._controls.$relative).on("click", t.proxy(function (t) {
            this.next(i.navSpeed)
        }, this)), i.dotsData || (this._templates = [t("<div>").addClass(i.dotClass).append(t("<span>")).prop("outerHTML")]), this._controls.$absolute = (i.dotsContainer ? t(i.dotsContainer) : t("<div>").addClass(i.dotsClass).appendTo(this.$element)).addClass("disabled"), this._controls.$absolute.on("click", "div", t.proxy(function (e) {
            var s = t(e.target).parent().is(this._controls.$absolute) ? t(e.target).index() : t(e.target).parent().index();
            e.preventDefault(), this.to(s, i.dotsSpeed)
        }, this));
        for (e in this._overrides) this._core[e] = t.proxy(this[e], this)
    }, n.prototype.destroy = function () {
        var t, e, i, s;
        for (t in this._handlers) this.$element.off(t, this._handlers[t]);
        for (e in this._controls) this._controls[e].remove();
        for (s in this.overides) this._core[s] = this._overrides[s];
        for (i in Object.getOwnPropertyNames(this)) "function" != typeof this[i] && (this[i] = null)
    }, n.prototype.update = function () {
        var t, e, i = this._core.clones().length / 2, s = i + this._core.items().length, n = this._core.maximum(!0),
            o = this._core.settings, r = o.center || o.autoWidth || o.dotsData ? 1 : o.dotsEach || o.items;
        if ("page" !== o.slideBy && (o.slideBy = Math.min(o.slideBy, o.items)), o.dots || "page" == o.slideBy) for (this._pages = [], t = i, e = 0, 0; s > t; t++) {
            if (e >= r || 0 === e) {
                if (this._pages.push({start: Math.min(n, t - i), end: t - i + r - 1}), Math.min(n, t - i) === n) break;
                e = 0, 0
            }
            e += this._core.mergers(this._core.relative(t))
        }
    }, n.prototype.draw = function () {
        var e, i = this._core.settings, s = this._core.items().length <= i.items,
            n = this._core.relative(this._core.current()), o = i.loop || i.rewind;
        this._controls.$relative.toggleClass("disabled", !i.nav || s), i.nav && (this._controls.$previous.toggleClass("disabled", !o && n <= this._core.minimum(!0)), this._controls.$next.toggleClass("disabled", !o && n >= this._core.maximum(!0))), this._controls.$absolute.toggleClass("disabled", !i.dots || s), i.dots && (e = this._pages.length - this._controls.$absolute.children().length, i.dotsData && 0 !== e ? this._controls.$absolute.html(this._templates.join("")) : e > 0 ? this._controls.$absolute.append(new Array(e + 1).join(this._templates[0])) : 0 > e && this._controls.$absolute.children().slice(e).remove(), this._controls.$absolute.find(".active").removeClass("active"), this._controls.$absolute.children().eq(t.inArray(this.current(), this._pages)).addClass("active"))
    }, n.prototype.onTrigger = function (e) {
        var i = this._core.settings;
        e.page = {
            index: t.inArray(this.current(), this._pages),
            count: this._pages.length,
            size: i && (i.center || i.autoWidth || i.dotsData ? 1 : i.dotsEach || i.items)
        }
    }, n.prototype.current = function () {
        var e = this._core.relative(this._core.current());
        return t.grep(this._pages, t.proxy(function (t, i) {
            return t.start <= e && t.end >= e
        }, this)).pop()
    }, n.prototype.getPosition = function (e) {
        var i, s, n = this._core.settings;
        return "page" == n.slideBy ? (i = t.inArray(this.current(), this._pages), s = this._pages.length, e ? ++i : --i, i = this._pages[(i % s + s) % s].start) : (i = this._core.relative(this._core.current()), s = this._core.items().length, e ? i += n.slideBy : i -= n.slideBy), i
    }, n.prototype.next = function (e) {
        t.proxy(this._overrides.to, this._core)(this.getPosition(!0), e)
    }, n.prototype.prev = function (e) {
        t.proxy(this._overrides.to, this._core)(this.getPosition(!1), e)
    }, n.prototype.to = function (e, i, s) {
        var n;
        !s && this._pages.length ? (n = this._pages.length, t.proxy(this._overrides.to, this._core)(this._pages[(e % n + n) % n].start, i)) : t.proxy(this._overrides.to, this._core)(e, i)
    }, t.fn.owlCarousel.Constructor.Plugins.Navigation = n
}(window.Zepto || window.jQuery, window, document), function (t, e, i, s) {
    "use strict";
    var n = function (i) {
        this._core = i, this._hashes = {}, this.$element = this._core.$element, this._handlers = {
            "initialized.owl.carousel": t.proxy(function (i) {
                i.namespace && "URLHash" === this._core.settings.startPosition && t(e).trigger("hashchange.owl.navigation")
            }, this), "prepared.owl.carousel": t.proxy(function (e) {
                if (e.namespace) {
                    var i = t(e.content).find("[data-hash]").addBack("[data-hash]").attr("data-hash");
                    if (!i) return;
                    this._hashes[i] = e.content
                }
            }, this), "changed.owl.carousel": t.proxy(function (i) {
                if (i.namespace && "position" === i.property.name) {
                    var s = this._core.items(this._core.relative(this._core.current())),
                        n = t.map(this._hashes, function (t, e) {
                            return t === s ? e : null
                        }).join();
                    if (!n || e.location.hash.slice(1) === n) return;
                    e.location.hash = n
                }
            }, this)
        }, this._core.options = t.extend({}, n.Defaults, this._core.options), this.$element.on(this._handlers), t(e).on("hashchange.owl.navigation", t.proxy(function (t) {
            var i = e.location.hash.substring(1), s = this._core.$stage.children(),
                n = this._hashes[i] && s.index(this._hashes[i]);
            void 0 !== n && n !== this._core.current() && this._core.to(this._core.relative(n), !1, !0)
        }, this))
    };
    n.Defaults = {URLhashListener: !1}, n.prototype.destroy = function () {
        var i, s;
        t(e).off("hashchange.owl.navigation");
        for (i in this._handlers) this._core.$element.off(i, this._handlers[i]);
        for (s in Object.getOwnPropertyNames(this)) "function" != typeof this[s] && (this[s] = null)
    }, t.fn.owlCarousel.Constructor.Plugins.Hash = n
}(window.Zepto || window.jQuery, window, document), function (t, e, i, s) {
    function n(e, i) {
        var n = !1, o = e.charAt(0).toUpperCase() + e.slice(1);
        return t.each((e + " " + a.join(o + " ") + o).split(" "), function (t, e) {
            return r[e] !== s ? (n = !i || e, !1) : void 0
        }), n
    }

    function o(t) {
        return n(t, !0)
    }

    var r = t("<support>").get(0).style, a = "Webkit Moz O ms".split(" "), h = {
        end: {
            WebkitTransition: "webkitTransitionEnd",
            MozTransition: "transitionend",
            OTransition: "oTransitionEnd",
            transition: "transitionend"
        }
    }, l = {
        end: {
            WebkitAnimation: "webkitAnimationEnd",
            MozAnimation: "animationend",
            OAnimation: "oAnimationEnd",
            animation: "animationend"
        }
    }, c = function () {
        return !!n("transform")
    }, p = function () {
        return !!n("perspective")
    }, d = function () {
        return !!n("animation")
    };
    (function () {
        return !!n("transition")
    })() && (t.support.transition = new String(o("transition")), t.support.transition.end = h.end[t.support.transition]), d() && (t.support.animation = new String(o("animation")), t.support.animation.end = l.end[t.support.animation]), c() && (t.support.transform = new String(o("transform")), t.support.transform3d = p())
}(window.Zepto || window.jQuery, window, document);
var bootstrap = function (t, e, n) {
    "use strict";

    function i(t, e) {
        for (var n = 0; n < e.length; n++) {
            var i = e[n];
            i.enumerable = i.enumerable || !1, i.configurable = !0, "value" in i && (i.writable = !0), Object.defineProperty(t, i.key, i)
        }
    }

    e = e && e.hasOwnProperty("default") ? e.default : e, n = n && n.hasOwnProperty("default") ? n.default : n;
    var s = function () {
        function t(t) {
            return {}.toString.call(t).match(/\s([a-zA-Z]+)/)[1].toLowerCase()
        }

        var n = !1, i = {
            WebkitTransition: "webkitTransitionEnd",
            MozTransition: "transitionend",
            OTransition: "oTransitionEnd otransitionend",
            transition: "transitionend"
        }, s = {
            TRANSITION_END: "bsTransitionEnd", getUID: function (t) {
                do {
                    t += ~~(1e6 * Math.random())
                } while (document.getElementById(t));
                return t
            }, getSelectorFromElement: function (t) {
                var n = t.getAttribute("data-target");
                n && "#" !== n || (n = t.getAttribute("href") || "");
                try {
                    return e(document).find(n).length > 0 ? n : null
                } catch (t) {
                    return null
                }
            }, reflow: function (t) {
                return t.offsetHeight
            }, triggerTransitionEnd: function (t) {
                e(t).trigger(n.end)
            }, supportsTransitionEnd: function () {
                return Boolean(n)
            }, isElement: function (t) {
                return (t[0] || t).nodeType
            }, typeCheckConfig: function (e, n, i) {
                for (var o in i) if (Object.prototype.hasOwnProperty.call(i, o)) {
                    var r = i[o], a = n[o], l = a && s.isElement(a) ? "element" : t(a);
                    if (!new RegExp(r).test(l)) throw new Error(e.toUpperCase() + ': Option "' + o + '" provided type "' + l + '" but expected type "' + r + '".')
                }
            }
        };
        return n = function () {
            if (window.QUnit) return !1;
            var t = document.createElement("bootstrap");
            for (var e in i) if (void 0 !== t.style[e]) return {end: i[e]};
            return !1
        }(), e.fn.emulateTransitionEnd = function (t) {
            var n = this, i = !1;
            return e(this).one(s.TRANSITION_END, function () {
                i = !0
            }), setTimeout(function () {
                i || s.triggerTransitionEnd(n)
            }, t), this
        }, s.supportsTransitionEnd() && (e.event.special[s.TRANSITION_END] = {
            bindType: n.end,
            delegateType: n.end,
            handle: function (t) {
                if (e(t.target).is(this)) return t.handleObj.handler.apply(this, arguments)
            }
        }), s
    }(), o = function (t, e, n) {
        return e && i(t.prototype, e), n && i(t, n), t
    }, r = function () {
        var t = e.fn.alert, n = "close.bs.alert", i = "closed.bs.alert", r = "click.bs.alert.data-api", a = "alert",
            l = "fade", h = "show", c = function () {
                function t(t) {
                    this._element = t
                }

                var r = t.prototype;
                return r.close = function (t) {
                    t = t || this._element;
                    var e = this._getRootElement(t);
                    this._triggerCloseEvent(e).isDefaultPrevented() || this._removeElement(e)
                }, r.dispose = function () {
                    e.removeData(this._element, "bs.alert"), this._element = null
                }, r._getRootElement = function (t) {
                    var n = s.getSelectorFromElement(t), i = !1;
                    return n && (i = e(n)[0]), i || (i = e(t).closest("." + a)[0]), i
                }, r._triggerCloseEvent = function (t) {
                    var i = e.Event(n);
                    return e(t).trigger(i), i
                }, r._removeElement = function (t) {
                    var n = this;
                    e(t).removeClass(h), s.supportsTransitionEnd() && e(t).hasClass(l) ? e(t).one(s.TRANSITION_END, function (e) {
                        return n._destroyElement(t, e)
                    }).emulateTransitionEnd(150) : this._destroyElement(t)
                }, r._destroyElement = function (t) {
                    e(t).detach().trigger(i).remove()
                }, t._jQueryInterface = function (n) {
                    return this.each(function () {
                        var i = e(this), s = i.data("bs.alert");
                        s || (s = new t(this), i.data("bs.alert", s)), "close" === n && s[n](this)
                    })
                }, t._handleDismiss = function (t) {
                    return function (e) {
                        e && e.preventDefault(), t.close(this)
                    }
                }, o(t, null, [{
                    key: "VERSION", get: function () {
                        return "4.0.0-beta.2"
                    }
                }]), t
            }();
        return e(document).on(r, '[data-dismiss="alert"]', c._handleDismiss(new c)), e.fn.alert = c._jQueryInterface, e.fn.alert.Constructor = c, e.fn.alert.noConflict = function () {
            return e.fn.alert = t, c._jQueryInterface
        }, c
    }(), a = function () {
        var t = e.fn.button, n = "active", i = "btn", s = "focus", r = '[data-toggle^="button"]',
            a = '[data-toggle="buttons"]', l = "input", h = ".active", c = ".btn", u = "click.bs.button.data-api",
            d = "focus.bs.button.data-api blur.bs.button.data-api", f = function () {
                function t(t) {
                    this._element = t
                }

                var i = t.prototype;
                return i.toggle = function () {
                    var t = !0, i = !0, s = e(this._element).closest(a)[0];
                    if (s) {
                        var o = e(this._element).find(l)[0];
                        if (o) {
                            if ("radio" === o.type) if (o.checked && e(this._element).hasClass(n)) t = !1; else {
                                var r = e(s).find(h)[0];
                                r && e(r).removeClass(n)
                            }
                            if (t) {
                                if (o.hasAttribute("disabled") || s.hasAttribute("disabled") || o.classList.contains("disabled") || s.classList.contains("disabled")) return;
                                o.checked = !e(this._element).hasClass(n), e(o).trigger("change")
                            }
                            o.focus(), i = !1
                        }
                    }
                    i && this._element.setAttribute("aria-pressed", !e(this._element).hasClass(n)), t && e(this._element).toggleClass(n)
                }, i.dispose = function () {
                    e.removeData(this._element, "bs.button"), this._element = null
                }, t._jQueryInterface = function (n) {
                    return this.each(function () {
                        var i = e(this).data("bs.button");
                        i || (i = new t(this), e(this).data("bs.button", i)), "toggle" === n && i[n]()
                    })
                }, o(t, null, [{
                    key: "VERSION", get: function () {
                        return "4.0.0-beta.2"
                    }
                }]), t
            }();
        return e(document).on(u, r, function (t) {
            t.preventDefault();
            var n = t.target;
            e(n).hasClass(i) || (n = e(n).closest(c)), f._jQueryInterface.call(e(n), "toggle")
        }).on(d, r, function (t) {
            var n = e(t.target).closest(c)[0];
            e(n).toggleClass(s, /^focus(in)?$/.test(t.type))
        }), e.fn.button = f._jQueryInterface, e.fn.button.Constructor = f, e.fn.button.noConflict = function () {
            return e.fn.button = t, f._jQueryInterface
        }, f
    }(), l = function () {
        var t = "bs.carousel", n = "." + t, i = e.fn.carousel,
            r = {interval: 5e3, keyboard: !0, slide: !1, pause: "hover", wrap: !0}, a = {
                interval: "(number|boolean)",
                keyboard: "boolean",
                slide: "(boolean|string)",
                pause: "(string|boolean)",
                wrap: "boolean"
            }, l = "next", h = "prev", c = "left", u = "right", d = {
                SLIDE: "slide" + n,
                SLID: "slid" + n,
                KEYDOWN: "keydown" + n,
                MOUSEENTER: "mouseenter" + n,
                MOUSELEAVE: "mouseleave" + n,
                TOUCHEND: "touchend" + n,
                LOAD_DATA_API: "load.bs.carousel.data-api",
                CLICK_DATA_API: "click.bs.carousel.data-api"
            }, f = "carousel", g = "active", _ = "slide", m = "carousel-item-right", p = "carousel-item-left",
            v = "carousel-item-next", E = "carousel-item-prev", b = {
                ACTIVE: ".active",
                ACTIVE_ITEM: ".active.carousel-item",
                ITEM: ".carousel-item",
                NEXT_PREV: ".carousel-item-next, .carousel-item-prev",
                INDICATORS: ".carousel-indicators",
                DATA_SLIDE: "[data-slide], [data-slide-to]",
                DATA_RIDE: '[data-ride="carousel"]'
            }, T = function () {
                function i(t, n) {
                    this._items = null, this._interval = null, this._activeElement = null, this._isPaused = !1, this._isSliding = !1, this.touchTimeout = null, this._config = this._getConfig(n), this._element = e(t)[0], this._indicatorsElement = e(this._element).find(b.INDICATORS)[0], this._addEventListeners()
                }

                var T = i.prototype;
                return T.next = function () {
                    this._isSliding || this._slide(l)
                }, T.nextWhenVisible = function () {
                    !document.hidden && e(this._element).is(":visible") && "hidden" !== e(this._element).css("visibility") && this.next()
                }, T.prev = function () {
                    this._isSliding || this._slide(h)
                }, T.pause = function (t) {
                    t || (this._isPaused = !0), e(this._element).find(b.NEXT_PREV)[0] && s.supportsTransitionEnd() && (s.triggerTransitionEnd(this._element), this.cycle(!0)), clearInterval(this._interval), this._interval = null
                }, T.cycle = function (t) {
                    t || (this._isPaused = !1), this._interval && (clearInterval(this._interval), this._interval = null), this._config.interval && !this._isPaused && (this._interval = setInterval((document.visibilityState ? this.nextWhenVisible : this.next).bind(this), this._config.interval))
                }, T.to = function (t) {
                    var n = this;
                    this._activeElement = e(this._element).find(b.ACTIVE_ITEM)[0];
                    var i = this._getItemIndex(this._activeElement);
                    if (!(t > this._items.length - 1 || t < 0)) if (this._isSliding) e(this._element).one(d.SLID, function () {
                        return n.to(t)
                    }); else {
                        if (i === t) return this.pause(), void this.cycle();
                        var s = t > i ? l : h;
                        this._slide(s, this._items[t])
                    }
                }, T.dispose = function () {
                    e(this._element).off(n), e.removeData(this._element, t), this._items = null, this._config = null, this._element = null, this._interval = null, this._isPaused = null, this._isSliding = null, this._activeElement = null, this._indicatorsElement = null
                }, T._getConfig = function (t) {
                    return t = e.extend({}, r, t), s.typeCheckConfig("carousel", t, a), t
                }, T._addEventListeners = function () {
                    var t = this;
                    this._config.keyboard && e(this._element).on(d.KEYDOWN, function (e) {
                        return t._keydown(e)
                    }), "hover" === this._config.pause && (e(this._element).on(d.MOUSEENTER, function (e) {
                        return t.pause(e)
                    }).on(d.MOUSELEAVE, function (e) {
                        return t.cycle(e)
                    }), "ontouchstart" in document.documentElement && e(this._element).on(d.TOUCHEND, function () {
                        t.pause(), t.touchTimeout && clearTimeout(t.touchTimeout), t.touchTimeout = setTimeout(function (e) {
                            return t.cycle(e)
                        }, 500 + t._config.interval)
                    }))
                }, T._keydown = function (t) {
                    if (!/input|textarea/i.test(t.target.tagName)) switch (t.which) {
                        case 37:
                            t.preventDefault(), this.prev();
                            break;
                        case 39:
                            t.preventDefault(), this.next();
                            break;
                        default:
                            return
                    }
                }, T._getItemIndex = function (t) {
                    return this._items = e.makeArray(e(t).parent().find(b.ITEM)), this._items.indexOf(t)
                }, T._getItemByDirection = function (t, e) {
                    var n = t === l, i = t === h, s = this._getItemIndex(e), o = this._items.length - 1;
                    if ((i && 0 === s || n && s === o) && !this._config.wrap) return e;
                    var r = (s + (t === h ? -1 : 1)) % this._items.length;
                    return -1 === r ? this._items[this._items.length - 1] : this._items[r]
                }, T._triggerSlideEvent = function (t, n) {
                    var i = this._getItemIndex(t), s = this._getItemIndex(e(this._element).find(b.ACTIVE_ITEM)[0]),
                        o = e.Event(d.SLIDE, {relatedTarget: t, direction: n, from: s, to: i});
                    return e(this._element).trigger(o), o
                }, T._setActiveIndicatorElement = function (t) {
                    if (this._indicatorsElement) {
                        e(this._indicatorsElement).find(b.ACTIVE).removeClass(g);
                        var n = this._indicatorsElement.children[this._getItemIndex(t)];
                        n && e(n).addClass(g)
                    }
                }, T._slide = function (t, n) {
                    var i, o, r, a = this, h = e(this._element).find(b.ACTIVE_ITEM)[0], f = this._getItemIndex(h),
                        T = n || h && this._getItemByDirection(t, h), y = this._getItemIndex(T),
                        C = Boolean(this._interval);
                    if (t === l ? (i = p, o = v, r = c) : (i = m, o = E, r = u), T && e(T).hasClass(g)) this._isSliding = !1; else if (!this._triggerSlideEvent(T, r).isDefaultPrevented() && h && T) {
                        this._isSliding = !0, C && this.pause(), this._setActiveIndicatorElement(T);
                        var A = e.Event(d.SLID, {relatedTarget: T, direction: r, from: f, to: y});
                        s.supportsTransitionEnd() && e(this._element).hasClass(_) ? (e(T).addClass(o), s.reflow(T), e(h).addClass(i), e(T).addClass(i), e(h).one(s.TRANSITION_END, function () {
                            e(T).removeClass(i + " " + o).addClass(g), e(h).removeClass(g + " " + o + " " + i), a._isSliding = !1, setTimeout(function () {
                                return e(a._element).trigger(A)
                            }, 0)
                        }).emulateTransitionEnd(600)) : (e(h).removeClass(g), e(T).addClass(g), this._isSliding = !1, e(this._element).trigger(A)), C && this.cycle()
                    }
                }, i._jQueryInterface = function (n) {
                    return this.each(function () {
                        var s = e(this).data(t), o = e.extend({}, r, e(this).data());
                        "object" == typeof n && e.extend(o, n);
                        var a = "string" == typeof n ? n : o.slide;
                        if (s || (s = new i(this, o), e(this).data(t, s)), "number" == typeof n) s.to(n); else if ("string" == typeof a) {
                            if (void 0 === s[a]) throw new Error('No method named "' + a + '"');
                            s[a]()
                        } else o.interval && (s.pause(), s.cycle())
                    })
                }, i._dataApiClickHandler = function (n) {
                    var o = s.getSelectorFromElement(this);
                    if (o) {
                        var r = e(o)[0];
                        if (r && e(r).hasClass(f)) {
                            var a = e.extend({}, e(r).data(), e(this).data()), l = this.getAttribute("data-slide-to");
                            l && (a.interval = !1), i._jQueryInterface.call(e(r), a), l && e(r).data(t).to(l), n.preventDefault()
                        }
                    }
                }, o(i, null, [{
                    key: "VERSION", get: function () {
                        return "4.0.0-beta.2"
                    }
                }, {
                    key: "Default", get: function () {
                        return r
                    }
                }]), i
            }();
        return e(document).on(d.CLICK_DATA_API, b.DATA_SLIDE, T._dataApiClickHandler), e(window).on(d.LOAD_DATA_API, function () {
            e(b.DATA_RIDE).each(function () {
                var t = e(this);
                T._jQueryInterface.call(t, t.data())
            })
        }), e.fn.carousel = T._jQueryInterface, e.fn.carousel.Constructor = T, e.fn.carousel.noConflict = function () {
            return e.fn.carousel = i, T._jQueryInterface
        }, T
    }(), h = function () {
        var t = "bs.collapse", n = e.fn.collapse, i = {toggle: !0, parent: ""},
            r = {toggle: "boolean", parent: "(string|element)"}, a = "show.bs.collapse", l = "shown.bs.collapse",
            h = "hide.bs.collapse", c = "hidden.bs.collapse", u = "click.bs.collapse.data-api", d = "show",
            f = "collapse", g = "collapsing", _ = "collapsed", m = "width", p = "height",
            v = {ACTIVES: ".show, .collapsing", DATA_TOGGLE: '[data-toggle="collapse"]'}, E = function () {
                function n(t, n) {
                    this._isTransitioning = !1, this._element = t, this._config = this._getConfig(n), this._triggerArray = e.makeArray(e('[data-toggle="collapse"][href="#' + t.id + '"],[data-toggle="collapse"][data-target="#' + t.id + '"]'));
                    for (var i = e(v.DATA_TOGGLE), o = 0; o < i.length; o++) {
                        var r = i[o], a = s.getSelectorFromElement(r);
                        null !== a && e(a).filter(t).length > 0 && this._triggerArray.push(r)
                    }
                    this._parent = this._config.parent ? this._getParent() : null, this._config.parent || this._addAriaAndCollapsedClass(this._element, this._triggerArray), this._config.toggle && this.toggle()
                }

                var u = n.prototype;
                return u.toggle = function () {
                    e(this._element).hasClass(d) ? this.hide() : this.show()
                }, u.show = function () {
                    var i = this;
                    if (!this._isTransitioning && !e(this._element).hasClass(d)) {
                        var o, r;
                        if (this._parent && ((o = e.makeArray(e(this._parent).children().children(v.ACTIVES))).length || (o = null)), !(o && (r = e(o).data(t)) && r._isTransitioning)) {
                            var h = e.Event(a);
                            if (e(this._element).trigger(h), !h.isDefaultPrevented()) {
                                o && (n._jQueryInterface.call(e(o), "hide"), r || e(o).data(t, null));
                                var c = this._getDimension();
                                e(this._element).removeClass(f).addClass(g), this._element.style[c] = 0, this._triggerArray.length && e(this._triggerArray).removeClass(_).attr("aria-expanded", !0), this.setTransitioning(!0);
                                var u = function () {
                                    e(i._element).removeClass(g).addClass(f).addClass(d), i._element.style[c] = "", i.setTransitioning(!1), e(i._element).trigger(l)
                                };
                                if (s.supportsTransitionEnd()) {
                                    var m = "scroll" + (c[0].toUpperCase() + c.slice(1));
                                    e(this._element).one(s.TRANSITION_END, u).emulateTransitionEnd(600), this._element.style[c] = this._element[m] + "px"
                                } else u()
                            }
                        }
                    }
                }, u.hide = function () {
                    var t = this;
                    if (!this._isTransitioning && e(this._element).hasClass(d)) {
                        var n = e.Event(h);
                        if (e(this._element).trigger(n), !n.isDefaultPrevented()) {
                            var i = this._getDimension();
                            if (this._element.style[i] = this._element.getBoundingClientRect()[i] + "px", s.reflow(this._element), e(this._element).addClass(g).removeClass(f).removeClass(d), this._triggerArray.length) for (var o = 0; o < this._triggerArray.length; o++) {
                                var r = this._triggerArray[o], a = s.getSelectorFromElement(r);
                                null !== a && (e(a).hasClass(d) || e(r).addClass(_).attr("aria-expanded", !1))
                            }
                            this.setTransitioning(!0);
                            var l = function () {
                                t.setTransitioning(!1), e(t._element).removeClass(g).addClass(f).trigger(c)
                            };
                            this._element.style[i] = "", s.supportsTransitionEnd() ? e(this._element).one(s.TRANSITION_END, l).emulateTransitionEnd(600) : l()
                        }
                    }
                }, u.setTransitioning = function (t) {
                    this._isTransitioning = t
                }, u.dispose = function () {
                    e.removeData(this._element, t), this._config = null, this._parent = null, this._element = null, this._triggerArray = null, this._isTransitioning = null
                }, u._getConfig = function (t) {
                    return t = e.extend({}, i, t), t.toggle = Boolean(t.toggle), s.typeCheckConfig("collapse", t, r), t
                }, u._getDimension = function () {
                    return e(this._element).hasClass(m) ? m : p
                }, u._getParent = function () {
                    var t = this, i = null;
                    s.isElement(this._config.parent) ? (i = this._config.parent, void 0 !== this._config.parent.jquery && (i = this._config.parent[0])) : i = e(this._config.parent)[0];
                    var o = '[data-toggle="collapse"][data-parent="' + this._config.parent + '"]';
                    return e(i).find(o).each(function (e, i) {
                        t._addAriaAndCollapsedClass(n._getTargetFromElement(i), [i])
                    }), i
                }, u._addAriaAndCollapsedClass = function (t, n) {
                    if (t) {
                        var i = e(t).hasClass(d);
                        n.length && e(n).toggleClass(_, !i).attr("aria-expanded", i)
                    }
                }, n._getTargetFromElement = function (t) {
                    var n = s.getSelectorFromElement(t);
                    return n ? e(n)[0] : null
                }, n._jQueryInterface = function (s) {
                    return this.each(function () {
                        var o = e(this), r = o.data(t), a = e.extend({}, i, o.data(), "object" == typeof s && s);
                        if (!r && a.toggle && /show|hide/.test(s) && (a.toggle = !1), r || (r = new n(this, a), o.data(t, r)), "string" == typeof s) {
                            if (void 0 === r[s]) throw new Error('No method named "' + s + '"');
                            r[s]()
                        }
                    })
                }, o(n, null, [{
                    key: "VERSION", get: function () {
                        return "4.0.0-beta.2"
                    }
                }, {
                    key: "Default", get: function () {
                        return i
                    }
                }]), n
            }();
        return e(document).on(u, v.DATA_TOGGLE, function (n) {
            "A" === n.currentTarget.tagName && n.preventDefault();
            var i = e(this), o = s.getSelectorFromElement(this);
            e(o).each(function () {
                var n = e(this), s = n.data(t) ? "toggle" : i.data();
                E._jQueryInterface.call(n, s)
            })
        }), e.fn.collapse = E._jQueryInterface, e.fn.collapse.Constructor = E, e.fn.collapse.noConflict = function () {
            return e.fn.collapse = n, E._jQueryInterface
        }, E
    }(), c = function () {
        if (void 0 === n) throw new Error("Bootstrap dropdown require Popper.js (https://popper.js.org)");
        var t = "bs.dropdown", i = "." + t, r = e.fn.dropdown, a = new RegExp("38|40|27"), l = {
                HIDE: "hide" + i,
                HIDDEN: "hidden" + i,
                SHOW: "show" + i,
                SHOWN: "shown" + i,
                CLICK: "click" + i,
                CLICK_DATA_API: "click.bs.dropdown.data-api",
                KEYDOWN_DATA_API: "keydown.bs.dropdown.data-api",
                KEYUP_DATA_API: "keyup.bs.dropdown.data-api"
            }, h = "disabled", c = "show", u = "dropup", d = "dropdown-menu-right", f = "dropdown-menu-left",
            g = '[data-toggle="dropdown"]', _ = ".dropdown form", m = ".dropdown-menu", p = ".navbar-nav",
            v = ".dropdown-menu .dropdown-item:not(.disabled)", E = "top-start", b = "top-end", T = "bottom-start",
            y = "bottom-end", C = {offset: 0, flip: !0}, A = {offset: "(number|string|function)", flip: "boolean"},
            w = function () {
                function r(t, e) {
                    this._element = t, this._popper = null, this._config = this._getConfig(e), this._menu = this._getMenuElement(), this._inNavbar = this._detectNavbar(), this._addEventListeners()
                }

                var _ = r.prototype;
                return _.toggle = function () {
                    if (!this._element.disabled && !e(this._element).hasClass(h)) {
                        var t = r._getParentFromElement(this._element), i = e(this._menu).hasClass(c);
                        if (r._clearMenus(), !i) {
                            var s = {relatedTarget: this._element}, o = e.Event(l.SHOW, s);
                            if (e(t).trigger(o), !o.isDefaultPrevented()) {
                                var a = this._element;
                                e(t).hasClass(u) && (e(this._menu).hasClass(f) || e(this._menu).hasClass(d)) && (a = t), this._popper = new n(a, this._menu, this._getPopperConfig()), "ontouchstart" in document.documentElement && !e(t).closest(p).length && e("body").children().on("mouseover", null, e.noop), this._element.focus(), this._element.setAttribute("aria-expanded", !0), e(this._menu).toggleClass(c), e(t).toggleClass(c).trigger(e.Event(l.SHOWN, s))
                            }
                        }
                    }
                }, _.dispose = function () {
                    e.removeData(this._element, t), e(this._element).off(i), this._element = null, this._menu = null, null !== this._popper && this._popper.destroy(), this._popper = null
                }, _.update = function () {
                    this._inNavbar = this._detectNavbar(), null !== this._popper && this._popper.scheduleUpdate()
                }, _._addEventListeners = function () {
                    var t = this;
                    e(this._element).on(l.CLICK, function (e) {
                        e.preventDefault(), e.stopPropagation(), t.toggle()
                    })
                }, _._getConfig = function (t) {
                    return t = e.extend({}, this.constructor.Default, e(this._element).data(), t), s.typeCheckConfig("dropdown", t, this.constructor.DefaultType), t
                }, _._getMenuElement = function () {
                    if (!this._menu) {
                        var t = r._getParentFromElement(this._element);
                        this._menu = e(t).find(m)[0]
                    }
                    return this._menu
                }, _._getPlacement = function () {
                    var t = e(this._element).parent(), n = T;
                    return t.hasClass(u) ? (n = E, e(this._menu).hasClass(d) && (n = b)) : e(this._menu).hasClass(d) && (n = y), n
                }, _._detectNavbar = function () {
                    return e(this._element).closest(".navbar").length > 0
                }, _._getPopperConfig = function () {
                    var t = this, n = {};
                    "function" == typeof this._config.offset ? n.fn = function (n) {
                        return n.offsets = e.extend({}, n.offsets, t._config.offset(n.offsets) || {}), n
                    } : n.offset = this._config.offset;
                    var i = {
                        placement: this._getPlacement(),
                        modifiers: {offset: n, flip: {enabled: this._config.flip}}
                    };
                    return this._inNavbar && (i.modifiers.applyStyle = {enabled: !this._inNavbar}), i
                }, r._jQueryInterface = function (n) {
                    return this.each(function () {
                        var i = e(this).data(t);
                        if (i || (i = new r(this, "object" == typeof n ? n : null), e(this).data(t, i)), "string" == typeof n) {
                            if (void 0 === i[n]) throw new Error('No method named "' + n + '"');
                            i[n]()
                        }
                    })
                }, r._clearMenus = function (n) {
                    if (!n || 3 !== n.which && ("keyup" !== n.type || 9 === n.which)) for (var i = e.makeArray(e(g)), s = 0; s < i.length; s++) {
                        var o = r._getParentFromElement(i[s]), a = e(i[s]).data(t), h = {relatedTarget: i[s]};
                        if (a) {
                            var u = a._menu;
                            if (e(o).hasClass(c) && !(n && ("click" === n.type && /input|textarea/i.test(n.target.tagName) || "keyup" === n.type && 9 === n.which) && e.contains(o, n.target))) {
                                var d = e.Event(l.HIDE, h);
                                e(o).trigger(d), d.isDefaultPrevented() || ("ontouchstart" in document.documentElement && e("body").children().off("mouseover", null, e.noop), i[s].setAttribute("aria-expanded", "false"), e(u).removeClass(c), e(o).removeClass(c).trigger(e.Event(l.HIDDEN, h)))
                            }
                        }
                    }
                }, r._getParentFromElement = function (t) {
                    var n, i = s.getSelectorFromElement(t);
                    return i && (n = e(i)[0]), n || t.parentNode
                }, r._dataApiKeydownHandler = function (t) {
                    if (!(!a.test(t.which) || /button/i.test(t.target.tagName) && 32 === t.which || /input|textarea/i.test(t.target.tagName) || (t.preventDefault(), t.stopPropagation(), this.disabled || e(this).hasClass(h)))) {
                        var n = r._getParentFromElement(this), i = e(n).hasClass(c);
                        if ((i || 27 === t.which && 32 === t.which) && (!i || 27 !== t.which && 32 !== t.which)) {
                            var s = e(n).find(v).get();
                            if (s.length) {
                                var o = s.indexOf(t.target);
                                38 === t.which && o > 0 && o--, 40 === t.which && o < s.length - 1 && o++, o < 0 && (o = 0), s[o].focus()
                            }
                        } else {
                            if (27 === t.which) {
                                var l = e(n).find(g)[0];
                                e(l).trigger("focus")
                            }
                            e(this).trigger("click")
                        }
                    }
                }, o(r, null, [{
                    key: "VERSION", get: function () {
                        return "4.0.0-beta.2"
                    }
                }, {
                    key: "Default", get: function () {
                        return C
                    }
                }, {
                    key: "DefaultType", get: function () {
                        return A
                    }
                }]), r
            }();
        return e(document).on(l.KEYDOWN_DATA_API, g, w._dataApiKeydownHandler).on(l.KEYDOWN_DATA_API, m, w._dataApiKeydownHandler).on(l.CLICK_DATA_API + " " + l.KEYUP_DATA_API, w._clearMenus).on(l.CLICK_DATA_API, g, function (t) {
            t.preventDefault(), t.stopPropagation(), w._jQueryInterface.call(e(this), "toggle")
        }).on(l.CLICK_DATA_API, _, function (t) {
            t.stopPropagation()
        }), e.fn.dropdown = w._jQueryInterface, e.fn.dropdown.Constructor = w, e.fn.dropdown.noConflict = function () {
            return e.fn.dropdown = r, w._jQueryInterface
        }, w
    }(), u = function () {
        var t = e.fn.modal, n = {backdrop: !0, keyboard: !0, focus: !0, show: !0},
            i = {backdrop: "(boolean|string)", keyboard: "boolean", focus: "boolean", show: "boolean"},
            r = "hide.bs.modal", a = "hidden.bs.modal", l = "show.bs.modal", h = "shown.bs.modal",
            c = "focusin.bs.modal", u = "resize.bs.modal", d = "click.dismiss.bs.modal", f = "keydown.dismiss.bs.modal",
            g = "mouseup.dismiss.bs.modal", _ = "mousedown.dismiss.bs.modal", m = "click.bs.modal.data-api",
            p = "modal-scrollbar-measure", v = "modal-backdrop", E = "modal-open", b = "fade", T = "show", y = {
                DIALOG: ".modal-dialog",
                DATA_TOGGLE: '[data-toggle="modal"]',
                DATA_DISMISS: '[data-dismiss="modal"]',
                FIXED_CONTENT: ".fixed-top, .fixed-bottom, .is-fixed, .sticky-top",
                STICKY_CONTENT: ".sticky-top",
                NAVBAR_TOGGLER: ".navbar-toggler"
            }, C = function () {
                function t(t, n) {
                    this._config = this._getConfig(n), this._element = t, this._dialog = e(t).find(y.DIALOG)[0], this._backdrop = null, this._isShown = !1, this._isBodyOverflowing = !1, this._ignoreBackdropClick = !1, this._originalBodyPadding = 0, this._scrollbarWidth = 0
                }

                var m = t.prototype;
                return m.toggle = function (t) {
                    return this._isShown ? this.hide() : this.show(t)
                }, m.show = function (t) {
                    var n = this;
                    if (!this._isTransitioning && !this._isShown) {
                        s.supportsTransitionEnd() && e(this._element).hasClass(b) && (this._isTransitioning = !0);
                        var i = e.Event(l, {relatedTarget: t});
                        e(this._element).trigger(i), this._isShown || i.isDefaultPrevented() || (this._isShown = !0, this._checkScrollbar(), this._setScrollbar(), this._adjustDialog(), e(document.body).addClass(E), this._setEscapeEvent(), this._setResizeEvent(), e(this._element).on(d, y.DATA_DISMISS, function (t) {
                            return n.hide(t)
                        }), e(this._dialog).on(_, function () {
                            e(n._element).one(g, function (t) {
                                e(t.target).is(n._element) && (n._ignoreBackdropClick = !0)
                            })
                        }), this._showBackdrop(function () {
                            return n._showElement(t)
                        }))
                    }
                }, m.hide = function (t) {
                    var n = this;
                    if (t && t.preventDefault(), !this._isTransitioning && this._isShown) {
                        var i = e.Event(r);
                        if (e(this._element).trigger(i), this._isShown && !i.isDefaultPrevented()) {
                            this._isShown = !1;
                            var o = s.supportsTransitionEnd() && e(this._element).hasClass(b);
                            o && (this._isTransitioning = !0), this._setEscapeEvent(), this._setResizeEvent(), e(document).off(c), e(this._element).removeClass(T), e(this._element).off(d), e(this._dialog).off(_), o ? e(this._element).one(s.TRANSITION_END, function (t) {
                                return n._hideModal(t)
                            }).emulateTransitionEnd(300) : this._hideModal()
                        }
                    }
                }, m.dispose = function () {
                    e.removeData(this._element, "bs.modal"), e(window, document, this._element, this._backdrop).off(".bs.modal"), this._config = null, this._element = null, this._dialog = null, this._backdrop = null, this._isShown = null, this._isBodyOverflowing = null, this._ignoreBackdropClick = null, this._scrollbarWidth = null
                }, m.handleUpdate = function () {
                    this._adjustDialog()
                }, m._getConfig = function (t) {
                    return t = e.extend({}, n, t), s.typeCheckConfig("modal", t, i), t
                }, m._showElement = function (t) {
                    var n = this, i = s.supportsTransitionEnd() && e(this._element).hasClass(b);
                    this._element.parentNode && this._element.parentNode.nodeType === Node.ELEMENT_NODE || document.body.appendChild(this._element), this._element.style.display = "block", this._element.removeAttribute("aria-hidden"), this._element.scrollTop = 0, i && s.reflow(this._element), e(this._element).addClass(T), this._config.focus && this._enforceFocus();
                    var o = e.Event(h, {relatedTarget: t}), r = function () {
                        n._config.focus && n._element.focus(), n._isTransitioning = !1, e(n._element).trigger(o)
                    };
                    i ? e(this._dialog).one(s.TRANSITION_END, r).emulateTransitionEnd(300) : r()
                }, m._enforceFocus = function () {
                    var t = this;
                    e(document).off(c).on(c, function (n) {
                        document === n.target || t._element === n.target || e(t._element).has(n.target).length || t._element.focus()
                    })
                }, m._setEscapeEvent = function () {
                    var t = this;
                    this._isShown && this._config.keyboard ? e(this._element).on(f, function (e) {
                        27 === e.which && (e.preventDefault(), t.hide())
                    }) : this._isShown || e(this._element).off(f)
                }, m._setResizeEvent = function () {
                    var t = this;
                    this._isShown ? e(window).on(u, function (e) {
                        return t.handleUpdate(e)
                    }) : e(window).off(u)
                }, m._hideModal = function () {
                    var t = this;
                    this._element.style.display = "none", this._element.setAttribute("aria-hidden", !0), this._isTransitioning = !1, this._showBackdrop(function () {
                        e(document.body).removeClass(E), t._resetAdjustments(), t._resetScrollbar(), e(t._element).trigger(a)
                    })
                }, m._removeBackdrop = function () {
                    this._backdrop && (e(this._backdrop).remove(), this._backdrop = null)
                }, m._showBackdrop = function (t) {
                    var n = this, i = e(this._element).hasClass(b) ? b : "";
                    if (this._isShown && this._config.backdrop) {
                        var o = s.supportsTransitionEnd() && i;
                        if (this._backdrop = document.createElement("div"), this._backdrop.className = v, i && e(this._backdrop).addClass(i), e(this._backdrop).appendTo(document.body), e(this._element).on(d, function (t) {
                                n._ignoreBackdropClick ? n._ignoreBackdropClick = !1 : t.target === t.currentTarget && ("static" === n._config.backdrop ? n._element.focus() : n.hide())
                            }), o && s.reflow(this._backdrop), e(this._backdrop).addClass(T), !t) return;
                        if (!o) return void t();
                        e(this._backdrop).one(s.TRANSITION_END, t).emulateTransitionEnd(150)
                    } else if (!this._isShown && this._backdrop) {
                        e(this._backdrop).removeClass(T);
                        var r = function () {
                            n._removeBackdrop(), t && t()
                        };
                        s.supportsTransitionEnd() && e(this._element).hasClass(b) ? e(this._backdrop).one(s.TRANSITION_END, r).emulateTransitionEnd(150) : r()
                    } else t && t()
                }, m._adjustDialog = function () {
                    var t = this._element.scrollHeight > document.documentElement.clientHeight;
                    !this._isBodyOverflowing && t && (this._element.style.paddingLeft = this._scrollbarWidth + "px"), this._isBodyOverflowing && !t && (this._element.style.paddingRight = this._scrollbarWidth + "px")
                }, m._resetAdjustments = function () {
                    this._element.style.paddingLeft = "", this._element.style.paddingRight = ""
                }, m._checkScrollbar = function () {
                    var t = document.body.getBoundingClientRect();
                    this._isBodyOverflowing = t.left + t.right < window.innerWidth, this._scrollbarWidth = this._getScrollbarWidth()
                }, m._setScrollbar = function () {
                    var t = this;
                    if (this._isBodyOverflowing) {
                        e(y.FIXED_CONTENT).each(function (n, i) {
                            var s = e(i)[0].style.paddingRight, o = e(i).css("padding-right");
                            e(i).data("padding-right", s).css("padding-right", parseFloat(o) + t._scrollbarWidth + "px")
                        }), e(y.STICKY_CONTENT).each(function (n, i) {
                            var s = e(i)[0].style.marginRight, o = e(i).css("margin-right");
                            e(i).data("margin-right", s).css("margin-right", parseFloat(o) - t._scrollbarWidth + "px")
                        }), e(y.NAVBAR_TOGGLER).each(function (n, i) {
                            var s = e(i)[0].style.marginRight, o = e(i).css("margin-right");
                            e(i).data("margin-right", s).css("margin-right", parseFloat(o) + t._scrollbarWidth + "px")
                        });
                        var n = document.body.style.paddingRight, i = e("body").css("padding-right");
                        e("body").data("padding-right", n).css("padding-right", parseFloat(i) + this._scrollbarWidth + "px")
                    }
                }, m._resetScrollbar = function () {
                    e(y.FIXED_CONTENT).each(function (t, n) {
                        var i = e(n).data("padding-right");
                        void 0 !== i && e(n).css("padding-right", i).removeData("padding-right")
                    }), e(y.STICKY_CONTENT + ", " + y.NAVBAR_TOGGLER).each(function (t, n) {
                        var i = e(n).data("margin-right");
                        void 0 !== i && e(n).css("margin-right", i).removeData("margin-right")
                    });
                    var t = e("body").data("padding-right");
                    void 0 !== t && e("body").css("padding-right", t).removeData("padding-right")
                }, m._getScrollbarWidth = function () {
                    var t = document.createElement("div");
                    t.className = p, document.body.appendChild(t);
                    var e = t.getBoundingClientRect().width - t.clientWidth;
                    return document.body.removeChild(t), e
                }, t._jQueryInterface = function (n, i) {
                    return this.each(function () {
                        var s = e(this).data("bs.modal"),
                            o = e.extend({}, t.Default, e(this).data(), "object" == typeof n && n);
                        if (s || (s = new t(this, o), e(this).data("bs.modal", s)), "string" == typeof n) {
                            if (void 0 === s[n]) throw new Error('No method named "' + n + '"');
                            s[n](i)
                        } else o.show && s.show(i)
                    })
                }, o(t, null, [{
                    key: "VERSION", get: function () {
                        return "4.0.0-beta.2"
                    }
                }, {
                    key: "Default", get: function () {
                        return n
                    }
                }]), t
            }();
        return e(document).on(m, y.DATA_TOGGLE, function (t) {
            var n, i = this, o = s.getSelectorFromElement(this);
            o && (n = e(o)[0]);
            var r = e(n).data("bs.modal") ? "toggle" : e.extend({}, e(n).data(), e(this).data());
            "A" !== this.tagName && "AREA" !== this.tagName || t.preventDefault();
            var h = e(n).one(l, function (t) {
                t.isDefaultPrevented() || h.one(a, function () {
                    e(i).is(":visible") && i.focus()
                })
            });
            C._jQueryInterface.call(e(n), r, this)
        }), e.fn.modal = C._jQueryInterface, e.fn.modal.Constructor = C, e.fn.modal.noConflict = function () {
            return e.fn.modal = t, C._jQueryInterface
        }, C
    }(), d = function () {
        if (void 0 === n) throw new Error("Bootstrap tooltips require Popper.js (https://popper.js.org)");
        var t = ".bs.tooltip", i = e.fn.tooltip, r = new RegExp("(^|\\s)bs-tooltip\\S+", "g"), a = {
                animation: "boolean",
                template: "string",
                title: "(string|element|function)",
                trigger: "string",
                delay: "(number|object)",
                html: "boolean",
                selector: "(string|boolean)",
                placement: "(string|function)",
                offset: "(number|string)",
                container: "(string|element|boolean)",
                fallbackPlacement: "(string|array)"
            }, l = {AUTO: "auto", TOP: "top", RIGHT: "right", BOTTOM: "bottom", LEFT: "left"}, h = {
                animation: !0,
                template: '<div class="tooltip" role="tooltip"><div class="arrow"></div><div class="tooltip-inner"></div></div>',
                trigger: "hover focus",
                title: "",
                delay: 0,
                html: !1,
                selector: !1,
                placement: "top",
                offset: 0,
                container: !1,
                fallbackPlacement: "flip"
            }, c = "show", u = "out", d = {
                HIDE: "hide" + t,
                HIDDEN: "hidden" + t,
                SHOW: "show" + t,
                SHOWN: "shown" + t,
                INSERTED: "inserted" + t,
                CLICK: "click" + t,
                FOCUSIN: "focusin" + t,
                FOCUSOUT: "focusout" + t,
                MOUSEENTER: "mouseenter" + t,
                MOUSELEAVE: "mouseleave" + t
            }, f = "fade", g = "show", _ = ".tooltip-inner", m = ".arrow", p = "hover", v = "focus", E = "click",
            b = "manual", T = function () {
                function i(t, e) {
                    this._isEnabled = !0, this._timeout = 0, this._hoverState = "", this._activeTrigger = {}, this._popper = null, this.element = t, this.config = this._getConfig(e), this.tip = null, this._setListeners()
                }

                var T = i.prototype;
                return T.enable = function () {
                    this._isEnabled = !0
                }, T.disable = function () {
                    this._isEnabled = !1
                }, T.toggleEnabled = function () {
                    this._isEnabled = !this._isEnabled
                }, T.toggle = function (t) {
                    if (this._isEnabled) if (t) {
                        var n = this.constructor.DATA_KEY, i = e(t.currentTarget).data(n);
                        i || (i = new this.constructor(t.currentTarget, this._getDelegateConfig()), e(t.currentTarget).data(n, i)), i._activeTrigger.click = !i._activeTrigger.click, i._isWithActiveTrigger() ? i._enter(null, i) : i._leave(null, i)
                    } else {
                        if (e(this.getTipElement()).hasClass(g)) return void this._leave(null, this);
                        this._enter(null, this)
                    }
                }, T.dispose = function () {
                    clearTimeout(this._timeout), e.removeData(this.element, this.constructor.DATA_KEY), e(this.element).off(this.constructor.EVENT_KEY), e(this.element).closest(".modal").off("hide.bs.modal"), this.tip && e(this.tip).remove(), this._isEnabled = null, this._timeout = null, this._hoverState = null, this._activeTrigger = null, null !== this._popper && this._popper.destroy(), this._popper = null, this.element = null, this.config = null, this.tip = null
                }, T.show = function () {
                    var t = this;
                    if ("none" === e(this.element).css("display")) throw new Error("Please use show on visible elements");
                    var o = e.Event(this.constructor.Event.SHOW);
                    if (this.isWithContent() && this._isEnabled) {
                        e(this.element).trigger(o);
                        var r = e.contains(this.element.ownerDocument.documentElement, this.element);
                        if (o.isDefaultPrevented() || !r) return;
                        var a = this.getTipElement(), l = s.getUID(this.constructor.NAME);
                        a.setAttribute("id", l), this.element.setAttribute("aria-describedby", l), this.setContent(), this.config.animation && e(a).addClass(f);
                        var h = "function" == typeof this.config.placement ? this.config.placement.call(this, a, this.element) : this.config.placement,
                            c = this._getAttachment(h);
                        this.addAttachmentClass(c);
                        var d = !1 === this.config.container ? document.body : e(this.config.container);
                        e(a).data(this.constructor.DATA_KEY, this), e.contains(this.element.ownerDocument.documentElement, this.tip) || e(a).appendTo(d), e(this.element).trigger(this.constructor.Event.INSERTED), this._popper = new n(this.element, a, {
                            placement: c,
                            modifiers: {
                                offset: {offset: this.config.offset},
                                flip: {behavior: this.config.fallbackPlacement},
                                arrow: {element: m}
                            },
                            onCreate: function (e) {
                                e.originalPlacement !== e.placement && t._handlePopperPlacementChange(e)
                            },
                            onUpdate: function (e) {
                                t._handlePopperPlacementChange(e)
                            }
                        }), e(a).addClass(g), "ontouchstart" in document.documentElement && e("body").children().on("mouseover", null, e.noop);
                        var _ = function () {
                            t.config.animation && t._fixTransition();
                            var n = t._hoverState;
                            t._hoverState = null, e(t.element).trigger(t.constructor.Event.SHOWN), n === u && t._leave(null, t)
                        };
                        s.supportsTransitionEnd() && e(this.tip).hasClass(f) ? e(this.tip).one(s.TRANSITION_END, _).emulateTransitionEnd(i._TRANSITION_DURATION) : _()
                    }
                }, T.hide = function (t) {
                    var n = this, i = this.getTipElement(), o = e.Event(this.constructor.Event.HIDE), r = function () {
                        n._hoverState !== c && i.parentNode && i.parentNode.removeChild(i), n._cleanTipClass(), n.element.removeAttribute("aria-describedby"), e(n.element).trigger(n.constructor.Event.HIDDEN), null !== n._popper && n._popper.destroy(), t && t()
                    };
                    e(this.element).trigger(o), o.isDefaultPrevented() || (e(i).removeClass(g), "ontouchstart" in document.documentElement && e("body").children().off("mouseover", null, e.noop), this._activeTrigger[E] = !1, this._activeTrigger[v] = !1, this._activeTrigger[p] = !1, s.supportsTransitionEnd() && e(this.tip).hasClass(f) ? e(i).one(s.TRANSITION_END, r).emulateTransitionEnd(150) : r(), this._hoverState = "")
                }, T.update = function () {
                    null !== this._popper && this._popper.scheduleUpdate()
                }, T.isWithContent = function () {
                    return Boolean(this.getTitle())
                }, T.addAttachmentClass = function (t) {
                    e(this.getTipElement()).addClass("bs-tooltip-" + t)
                }, T.getTipElement = function () {
                    return this.tip = this.tip || e(this.config.template)[0], this.tip
                }, T.setContent = function () {
                    var t = e(this.getTipElement());
                    this.setElementContent(t.find(_), this.getTitle()), t.removeClass(f + " " + g)
                }, T.setElementContent = function (t, n) {
                    var i = this.config.html;
                    "object" == typeof n && (n.nodeType || n.jquery) ? i ? e(n).parent().is(t) || t.empty().append(n) : t.text(e(n).text()) : t[i ? "html" : "text"](n)
                }, T.getTitle = function () {
                    var t = this.element.getAttribute("data-original-title");
                    return t || (t = "function" == typeof this.config.title ? this.config.title.call(this.element) : this.config.title), t
                }, T._getAttachment = function (t) {
                    return l[t.toUpperCase()]
                }, T._setListeners = function () {
                    var t = this;
                    this.config.trigger.split(" ").forEach(function (n) {
                        if ("click" === n) e(t.element).on(t.constructor.Event.CLICK, t.config.selector, function (e) {
                            return t.toggle(e)
                        }); else if (n !== b) {
                            var i = n === p ? t.constructor.Event.MOUSEENTER : t.constructor.Event.FOCUSIN,
                                s = n === p ? t.constructor.Event.MOUSELEAVE : t.constructor.Event.FOCUSOUT;
                            e(t.element).on(i, t.config.selector, function (e) {
                                return t._enter(e)
                            }).on(s, t.config.selector, function (e) {
                                return t._leave(e)
                            })
                        }
                        e(t.element).closest(".modal").on("hide.bs.modal", function () {
                            return t.hide()
                        })
                    }), this.config.selector ? this.config = e.extend({}, this.config, {
                        trigger: "manual",
                        selector: ""
                    }) : this._fixTitle()
                }, T._fixTitle = function () {
                    var t = typeof this.element.getAttribute("data-original-title");
                    (this.element.getAttribute("title") || "string" !== t) && (this.element.setAttribute("data-original-title", this.element.getAttribute("title") || ""), this.element.setAttribute("title", ""))
                }, T._enter = function (t, n) {
                    var i = this.constructor.DATA_KEY;
                    (n = n || e(t.currentTarget).data(i)) || (n = new this.constructor(t.currentTarget, this._getDelegateConfig()), e(t.currentTarget).data(i, n)), t && (n._activeTrigger["focusin" === t.type ? v : p] = !0), e(n.getTipElement()).hasClass(g) || n._hoverState === c ? n._hoverState = c : (clearTimeout(n._timeout), n._hoverState = c, n.config.delay && n.config.delay.show ? n._timeout = setTimeout(function () {
                        n._hoverState === c && n.show()
                    }, n.config.delay.show) : n.show())
                }, T._leave = function (t, n) {
                    var i = this.constructor.DATA_KEY;
                    (n = n || e(t.currentTarget).data(i)) || (n = new this.constructor(t.currentTarget, this._getDelegateConfig()), e(t.currentTarget).data(i, n)), t && (n._activeTrigger["focusout" === t.type ? v : p] = !1), n._isWithActiveTrigger() || (clearTimeout(n._timeout), n._hoverState = u, n.config.delay && n.config.delay.hide ? n._timeout = setTimeout(function () {
                        n._hoverState === u && n.hide()
                    }, n.config.delay.hide) : n.hide())
                }, T._isWithActiveTrigger = function () {
                    for (var t in this._activeTrigger) if (this._activeTrigger[t]) return !0;
                    return !1
                }, T._getConfig = function (t) {
                    return "number" == typeof(t = e.extend({}, this.constructor.Default, e(this.element).data(), t)).delay && (t.delay = {
                        show: t.delay,
                        hide: t.delay
                    }), "number" == typeof t.title && (t.title = t.title.toString()), "number" == typeof t.content && (t.content = t.content.toString()), s.typeCheckConfig("tooltip", t, this.constructor.DefaultType), t
                }, T._getDelegateConfig = function () {
                    var t = {};
                    if (this.config) for (var e in this.config) this.constructor.Default[e] !== this.config[e] && (t[e] = this.config[e]);
                    return t
                }, T._cleanTipClass = function () {
                    var t = e(this.getTipElement()), n = t.attr("class").match(r);
                    null !== n && n.length > 0 && t.removeClass(n.join(""))
                }, T._handlePopperPlacementChange = function (t) {
                    this._cleanTipClass(), this.addAttachmentClass(this._getAttachment(t.placement))
                }, T._fixTransition = function () {
                    var t = this.getTipElement(), n = this.config.animation;
                    null === t.getAttribute("x-placement") && (e(t).removeClass(f), this.config.animation = !1, this.hide(), this.show(), this.config.animation = n)
                }, i._jQueryInterface = function (t) {
                    return this.each(function () {
                        var n = e(this).data("bs.tooltip"), s = "object" == typeof t && t;
                        if ((n || !/dispose|hide/.test(t)) && (n || (n = new i(this, s), e(this).data("bs.tooltip", n)), "string" == typeof t)) {
                            if (void 0 === n[t]) throw new Error('No method named "' + t + '"');
                            n[t]()
                        }
                    })
                }, o(i, null, [{
                    key: "VERSION", get: function () {
                        return "4.0.0-beta.2"
                    }
                }, {
                    key: "Default", get: function () {
                        return h
                    }
                }, {
                    key: "NAME", get: function () {
                        return "tooltip"
                    }
                }, {
                    key: "DATA_KEY", get: function () {
                        return "bs.tooltip"
                    }
                }, {
                    key: "Event", get: function () {
                        return d
                    }
                }, {
                    key: "EVENT_KEY", get: function () {
                        return t
                    }
                }, {
                    key: "DefaultType", get: function () {
                        return a
                    }
                }]), i
            }();
        return e.fn.tooltip = T._jQueryInterface, e.fn.tooltip.Constructor = T, e.fn.tooltip.noConflict = function () {
            return e.fn.tooltip = i, T._jQueryInterface
        }, T
    }(), f = function () {
        var t = ".bs.popover", n = e.fn.popover, i = new RegExp("(^|\\s)bs-popover\\S+", "g"),
            s = e.extend({}, d.Default, {
                placement: "right",
                trigger: "click",
                content: "",
                template: '<div class="popover" role="tooltip"><div class="arrow"></div><h3 class="popover-header"></h3><div class="popover-body"></div></div>'
            }), r = e.extend({}, d.DefaultType, {content: "(string|element|function)"}), a = "fade", l = "show",
            h = ".popover-header", c = ".popover-body", u = {
                HIDE: "hide" + t,
                HIDDEN: "hidden" + t,
                SHOW: "show" + t,
                SHOWN: "shown" + t,
                INSERTED: "inserted" + t,
                CLICK: "click" + t,
                FOCUSIN: "focusin" + t,
                FOCUSOUT: "focusout" + t,
                MOUSEENTER: "mouseenter" + t,
                MOUSELEAVE: "mouseleave" + t
            }, f = function (n) {
                function d() {
                    return n.apply(this, arguments) || this
                }

                !function (t, e) {
                    t.prototype = Object.create(e.prototype), t.prototype.constructor = t, t.__proto__ = e
                }(d, n);
                var f = d.prototype;
                return f.isWithContent = function () {
                    return this.getTitle() || this._getContent()
                }, f.addAttachmentClass = function (t) {
                    e(this.getTipElement()).addClass("bs-popover-" + t)
                }, f.getTipElement = function () {
                    return this.tip = this.tip || e(this.config.template)[0], this.tip
                }, f.setContent = function () {
                    var t = e(this.getTipElement());
                    this.setElementContent(t.find(h), this.getTitle()), this.setElementContent(t.find(c), this._getContent()), t.removeClass(a + " " + l)
                }, f._getContent = function () {
                    return this.element.getAttribute("data-content") || ("function" == typeof this.config.content ? this.config.content.call(this.element) : this.config.content)
                }, f._cleanTipClass = function () {
                    var t = e(this.getTipElement()), n = t.attr("class").match(i);
                    null !== n && n.length > 0 && t.removeClass(n.join(""))
                }, d._jQueryInterface = function (t) {
                    return this.each(function () {
                        var n = e(this).data("bs.popover"), i = "object" == typeof t ? t : null;
                        if ((n || !/destroy|hide/.test(t)) && (n || (n = new d(this, i), e(this).data("bs.popover", n)), "string" == typeof t)) {
                            if (void 0 === n[t]) throw new Error('No method named "' + t + '"');
                            n[t]()
                        }
                    })
                }, o(d, null, [{
                    key: "VERSION", get: function () {
                        return "4.0.0-beta.2"
                    }
                }, {
                    key: "Default", get: function () {
                        return s
                    }
                }, {
                    key: "NAME", get: function () {
                        return "popover"
                    }
                }, {
                    key: "DATA_KEY", get: function () {
                        return "bs.popover"
                    }
                }, {
                    key: "Event", get: function () {
                        return u
                    }
                }, {
                    key: "EVENT_KEY", get: function () {
                        return t
                    }
                }, {
                    key: "DefaultType", get: function () {
                        return r
                    }
                }]), d
            }(d);
        return e.fn.popover = f._jQueryInterface, e.fn.popover.Constructor = f, e.fn.popover.noConflict = function () {
            return e.fn.popover = n, f._jQueryInterface
        }, f
    }(), g = function () {
        var t = "scrollspy", n = e.fn[t], i = {offset: 10, method: "auto", target: ""},
            r = {offset: "number", method: "string", target: "(string|element)"}, a = {
                ACTIVATE: "activate.bs.scrollspy",
                SCROLL: "scroll.bs.scrollspy",
                LOAD_DATA_API: "load.bs.scrollspy.data-api"
            }, l = "dropdown-item", h = "active", c = {
                DATA_SPY: '[data-spy="scroll"]',
                ACTIVE: ".active",
                NAV_LIST_GROUP: ".nav, .list-group",
                NAV_LINKS: ".nav-link",
                NAV_ITEMS: ".nav-item",
                LIST_ITEMS: ".list-group-item",
                DROPDOWN: ".dropdown",
                DROPDOWN_ITEMS: ".dropdown-item",
                DROPDOWN_TOGGLE: ".dropdown-toggle"
            }, u = "offset", d = "position", f = function () {
                function n(t, n) {
                    var i = this;
                    this._element = t, this._scrollElement = "BODY" === t.tagName ? window : t, this._config = this._getConfig(n), this._selector = this._config.target + " " + c.NAV_LINKS + "," + this._config.target + " " + c.LIST_ITEMS + "," + this._config.target + " " + c.DROPDOWN_ITEMS, this._offsets = [], this._targets = [], this._activeTarget = null, this._scrollHeight = 0, e(this._scrollElement).on(a.SCROLL, function (t) {
                        return i._process(t)
                    }), this.refresh(), this._process()
                }

                var f = n.prototype;
                return f.refresh = function () {
                    var t = this, n = this._scrollElement !== this._scrollElement.window ? d : u,
                        i = "auto" === this._config.method ? n : this._config.method,
                        o = i === d ? this._getScrollTop() : 0;
                    this._offsets = [], this._targets = [], this._scrollHeight = this._getScrollHeight(), e.makeArray(e(this._selector)).map(function (t) {
                        var n, r = s.getSelectorFromElement(t);
                        if (r && (n = e(r)[0]), n) {
                            var a = n.getBoundingClientRect();
                            if (a.width || a.height) return [e(n)[i]().top + o, r]
                        }
                        return null
                    }).filter(function (t) {
                        return t
                    }).sort(function (t, e) {
                        return t[0] - e[0]
                    }).forEach(function (e) {
                        t._offsets.push(e[0]), t._targets.push(e[1])
                    })
                }, f.dispose = function () {
                    e.removeData(this._element, "bs.scrollspy"), e(this._scrollElement).off(".bs.scrollspy"), this._element = null, this._scrollElement = null, this._config = null, this._selector = null, this._offsets = null, this._targets = null, this._activeTarget = null, this._scrollHeight = null
                }, f._getConfig = function (n) {
                    if ("string" != typeof(n = e.extend({}, i, n)).target) {
                        var o = e(n.target).attr("id");
                        o || (o = s.getUID(t), e(n.target).attr("id", o)), n.target = "#" + o
                    }
                    return s.typeCheckConfig(t, n, r), n
                }, f._getScrollTop = function () {
                    return this._scrollElement === window ? this._scrollElement.pageYOffset : this._scrollElement.scrollTop
                }, f._getScrollHeight = function () {
                    return this._scrollElement.scrollHeight || Math.max(document.body.scrollHeight, document.documentElement.scrollHeight)
                }, f._getOffsetHeight = function () {
                    return this._scrollElement === window ? window.innerHeight : this._scrollElement.getBoundingClientRect().height
                }, f._process = function () {
                    var t = this._getScrollTop() + this._config.offset, e = this._getScrollHeight(),
                        n = this._config.offset + e - this._getOffsetHeight();
                    if (this._scrollHeight !== e && this.refresh(), t >= n) {
                        var i = this._targets[this._targets.length - 1];
                        this._activeTarget !== i && this._activate(i)
                    } else {
                        if (this._activeTarget && t < this._offsets[0] && this._offsets[0] > 0) return this._activeTarget = null, void this._clear();
                        for (var s = this._offsets.length; s--;) this._activeTarget !== this._targets[s] && t >= this._offsets[s] && (void 0 === this._offsets[s + 1] || t < this._offsets[s + 1]) && this._activate(this._targets[s])
                    }
                }, f._activate = function (t) {
                    this._activeTarget = t, this._clear();
                    var n = this._selector.split(",");
                    n = n.map(function (e) {
                        return e + '[data-target="' + t + '"],' + e + '[href="' + t + '"]'
                    });
                    var i = e(n.join(","));
                    i.hasClass(l) ? (i.closest(c.DROPDOWN).find(c.DROPDOWN_TOGGLE).addClass(h), i.addClass(h)) : (i.addClass(h), i.parents(c.NAV_LIST_GROUP).prev(c.NAV_LINKS + ", " + c.LIST_ITEMS).addClass(h), i.parents(c.NAV_LIST_GROUP).prev(c.NAV_ITEMS).children(c.NAV_LINKS).addClass(h)), e(this._scrollElement).trigger(a.ACTIVATE, {relatedTarget: t})
                }, f._clear = function () {
                    e(this._selector).filter(c.ACTIVE).removeClass(h)
                }, n._jQueryInterface = function (t) {
                    return this.each(function () {
                        var i = e(this).data("bs.scrollspy");
                        if (i || (i = new n(this, "object" == typeof t && t), e(this).data("bs.scrollspy", i)), "string" == typeof t) {
                            if (void 0 === i[t]) throw new Error('No method named "' + t + '"');
                            i[t]()
                        }
                    })
                }, o(n, null, [{
                    key: "VERSION", get: function () {
                        return "4.0.0-beta.2"
                    }
                }, {
                    key: "Default", get: function () {
                        return i
                    }
                }]), n
            }();
        return e(window).on(a.LOAD_DATA_API, function () {
            for (var t = e.makeArray(e(c.DATA_SPY)), n = t.length; n--;) {
                var i = e(t[n]);
                f._jQueryInterface.call(i, i.data())
            }
        }), e.fn[t] = f._jQueryInterface, e.fn[t].Constructor = f, e.fn[t].noConflict = function () {
            return e.fn[t] = n, f._jQueryInterface
        }, f
    }(), _ = function () {
        var t = e.fn.tab, n = "hide.bs.tab", i = "hidden.bs.tab", r = "show.bs.tab", a = "shown.bs.tab",
            l = "click.bs.tab.data-api", h = "dropdown-menu", c = "active", u = "disabled", d = "fade", f = "show",
            g = ".dropdown", _ = ".nav, .list-group", m = ".active", p = "> li > .active",
            v = '[data-toggle="tab"], [data-toggle="pill"], [data-toggle="list"]', E = ".dropdown-toggle",
            b = "> .dropdown-menu .active", T = function () {
                function t(t) {
                    this._element = t
                }

                var l = t.prototype;
                return l.show = function () {
                    var t = this;
                    if (!(this._element.parentNode && this._element.parentNode.nodeType === Node.ELEMENT_NODE && e(this._element).hasClass(c) || e(this._element).hasClass(u))) {
                        var o, l, h = e(this._element).closest(_)[0], d = s.getSelectorFromElement(this._element);
                        if (h) {
                            var f = "UL" === h.nodeName ? p : m;
                            l = (l = e.makeArray(e(h).find(f)))[l.length - 1]
                        }
                        var g = e.Event(n, {relatedTarget: this._element}), v = e.Event(r, {relatedTarget: l});
                        if (l && e(l).trigger(g), e(this._element).trigger(v), !v.isDefaultPrevented() && !g.isDefaultPrevented()) {
                            d && (o = e(d)[0]), this._activate(this._element, h);
                            var E = function () {
                                var n = e.Event(i, {relatedTarget: t._element}), s = e.Event(a, {relatedTarget: l});
                                e(l).trigger(n), e(t._element).trigger(s)
                            };
                            o ? this._activate(o, o.parentNode, E) : E()
                        }
                    }
                }, l.dispose = function () {
                    e.removeData(this._element, "bs.tab"), this._element = null
                }, l._activate = function (t, n, i) {
                    var o = this, r = ("UL" === n.nodeName ? e(n).find(p) : e(n).children(m))[0],
                        a = i && s.supportsTransitionEnd() && r && e(r).hasClass(d), l = function () {
                            return o._transitionComplete(t, r, a, i)
                        };
                    r && a ? e(r).one(s.TRANSITION_END, l).emulateTransitionEnd(150) : l(), r && e(r).removeClass(f)
                }, l._transitionComplete = function (t, n, i, o) {
                    if (n) {
                        e(n).removeClass(c);
                        var r = e(n.parentNode).find(b)[0];
                        r && e(r).removeClass(c), "tab" === n.getAttribute("role") && n.setAttribute("aria-selected", !1)
                    }
                    if (e(t).addClass(c), "tab" === t.getAttribute("role") && t.setAttribute("aria-selected", !0), i ? (s.reflow(t), e(t).addClass(f)) : e(t).removeClass(d), t.parentNode && e(t.parentNode).hasClass(h)) {
                        var a = e(t).closest(g)[0];
                        a && e(a).find(E).addClass(c), t.setAttribute("aria-expanded", !0)
                    }
                    o && o()
                }, t._jQueryInterface = function (n) {
                    return this.each(function () {
                        var i = e(this), s = i.data("bs.tab");
                        if (s || (s = new t(this), i.data("bs.tab", s)), "string" == typeof n) {
                            if (void 0 === s[n]) throw new Error('No method named "' + n + '"');
                            s[n]()
                        }
                    })
                }, o(t, null, [{
                    key: "VERSION", get: function () {
                        return "4.0.0-beta.2"
                    }
                }]), t
            }();
        return e(document).on(l, v, function (t) {
            t.preventDefault(), T._jQueryInterface.call(e(this), "show")
        }), e.fn.tab = T._jQueryInterface, e.fn.tab.Constructor = T, e.fn.tab.noConflict = function () {
            return e.fn.tab = t, T._jQueryInterface
        }, T
    }();
    return function () {
        if (void 0 === e) throw new Error("Bootstrap's JavaScript requires jQuery. jQuery must be included before Bootstrap's JavaScript.");
        var t = e.fn.jquery.split(" ")[0].split(".");
        if (t[0] < 2 && t[1] < 9 || 1 === t[0] && 9 === t[1] && t[2] < 1 || t[0] >= 4) throw new Error("Bootstrap's JavaScript requires at least jQuery v1.9.1 but less than v4.0.0")
    }(), t.Util = s, t.Alert = r, t.Button = a, t.Carousel = l, t.Collapse = h, t.Dropdown = c, t.Modal = u, t.Popover = f, t.Scrollspy = g, t.Tab = _, t.Tooltip = d, t
}({}, $, Popper);
var $jscomp = {
    scope: {}, findInternal: function (t, a, e) {
        t instanceof String && (t = String(t));
        for (var n = t.length, s = 0; s < n; s++) {
            var r = t[s];
            if (a.call(e, r, s, t)) return {i: s, v: r}
        }
        return {i: -1, v: void 0}
    }
};
$jscomp.defineProperty = "function" == typeof Object.defineProperties ? Object.defineProperty : function (t, a, e) {
    if (e.get || e.set) throw new TypeError("ES3 does not support getters and setters.");
    t != Array.prototype && t != Object.prototype && (t[a] = e.value)
}, $jscomp.getGlobal = function (t) {
    return "undefined" != typeof window && window === t ? t : "undefined" != typeof global && null != global ? global : t
}, $jscomp.global = $jscomp.getGlobal(this), $jscomp.polyfill = function (t, a, e, n) {
    if (a) {
        for (e = $jscomp.global, t = t.split("."), n = 0; n < t.length - 1; n++) {
            var s = t[n];
            s in e || (e[s] = {}), e = e[s]
        }
        (a = a(n = e[t = t[t.length - 1]])) != n && null != a && $jscomp.defineProperty(e, t, {
            configurable: !0,
            writable: !0,
            value: a
        })
    }
}, $jscomp.polyfill("Array.prototype.find", function (t) {
    return t || function (t, a) {
        return $jscomp.findInternal(this, t, a).v
    }
}, "es6-impl", "es3"), function (t, a, e) {
    "function" == typeof define && define.amd ? define(["jquery"], t) : "object" == typeof exports ? module.exports = t(require("jquery")) : t(a || e)
}(function (t) {
    var a = function (a, e, n) {
        var s = {
            invalid: [], getCaret: function () {
                try {
                    var t, e = 0, n = a.get(0), r = document.selection, o = n.selectionStart;
                    return r && -1 === navigator.appVersion.indexOf("MSIE 10") ? ((t = r.createRange()).moveStart("character", -s.val().length), e = t.text.length) : (o || "0" === o) && (e = o), e
                } catch (t) {
                }
            }, setCaret: function (t) {
                try {
                    if (a.is(":focus")) {
                        var e, n = a.get(0);
                        n.setSelectionRange ? n.setSelectionRange(t, t) : ((e = n.createTextRange()).collapse(!0), e.moveEnd("character", t), e.moveStart("character", t), e.select())
                    }
                } catch (t) {
                }
            }, events: function () {
                a.on("keydown.mask", function (t) {
                    a.data("mask-keycode", t.keyCode || t.which), a.data("mask-previus-value", a.val()), a.data("mask-previus-caret-pos", s.getCaret()), s.maskDigitPosMapOld = s.maskDigitPosMap
                }).on(t.jMaskGlobals.useInput ? "input.mask" : "keyup.mask", s.behaviour).on("paste.mask drop.mask", function () {
                    setTimeout(function () {
                        a.keydown().keyup()
                    }, 100)
                }).on("change.mask", function () {
                    a.data("changed", !0)
                }).on("blur.mask", function () {
                    i === s.val() || a.data("changed") || a.trigger("change"), a.data("changed", !1)
                }).on("blur.mask", function () {
                    i = s.val()
                }).on("focus.mask", function (a) {
                    !0 === n.selectOnFocus && t(a.target).select()
                }).on("focusout.mask", function () {
                    n.clearIfNotMatch && !r.test(s.val()) && s.val("")
                })
            }, getRegexMask: function () {
                for (var t, a, n, s, r = [], i = 0; i < e.length; i++) (t = o.translation[e.charAt(i)]) ? (a = t.pattern.toString().replace(/.{1}$|^.{1}/g, ""), n = t.optional, (t = t.recursive) ? (r.push(e.charAt(i)), s = {
                    digit: e.charAt(i),
                    pattern: a
                }) : r.push(n || t ? a + "?" : a)) : r.push(e.charAt(i).replace(/[-\/\\^$*+?.()|[\]{}]/g, "\\$&"));
                return r = r.join(""), s && (r = r.replace(new RegExp("(" + s.digit + "(.*" + s.digit + ")?)"), "($1)?").replace(new RegExp(s.digit, "g"), s.pattern)), new RegExp(r)
            }, destroyEvents: function () {
                a.off("input keydown keyup paste drop blur focusout ".split(" ").join(".mask "))
            }, val: function (t) {
                var e = a.is("input") ? "val" : "text";
                return 0 < arguments.length ? (a[e]() !== t && a[e](t), e = a) : e = a[e](), e
            }, calculateCaretPosition: function () {
                var t = a.data("mask-previus-value") || "", e = s.getMasked(), n = s.getCaret();
                if (t !== e) {
                    var r, o = a.data("mask-previus-caret-pos") || 0, i = (e = e.length, t.length), l = t = 0, c = 0,
                        u = 0;
                    for (r = n; r < e && s.maskDigitPosMap[r]; r++) l++;
                    for (r = n - 1; 0 <= r && s.maskDigitPosMap[r]; r--) t++;
                    for (r = n - 1; 0 <= r; r--) s.maskDigitPosMap[r] && c++;
                    for (r = o - 1; 0 <= r; r--) s.maskDigitPosMapOld[r] && u++;
                    n > i ? n = 10 * e : o >= n && o !== i ? s.maskDigitPosMapOld[n] || (o = n, n = n - (u - c) - t, s.maskDigitPosMap[n] && (n = o)) : n > o && (n = n + (c - u) + l)
                }
                return n
            }, behaviour: function (e) {
                e = e || window.event, s.invalid = [];
                var n = a.data("mask-keycode");
                if (-1 === t.inArray(n, o.byPassKeys)) {
                    n = s.getMasked();
                    var r = s.getCaret();
                    return setTimeout(function () {
                        s.setCaret(s.calculateCaretPosition())
                    }, 10), s.val(n), s.setCaret(r), s.callbacks(e)
                }
            }, getMasked: function (t, a) {
                var r, i, l = [], c = void 0 === a ? s.val() : a + "", u = 0, f = e.length, p = 0, d = c.length, v = 1,
                    k = "push", h = -1, g = 0, m = [];
                n.reverse ? (k = "unshift", v = -1, r = 0, u = f - 1, p = d - 1, i = function () {
                    return -1 < u && -1 < p
                }) : (r = f - 1, i = function () {
                    return u < f && p < d
                });
                for (var y; i();) {
                    var M = e.charAt(u), b = c.charAt(p), j = o.translation[M];
                    j ? (b.match(j.pattern) ? (l[k](b), j.recursive && (-1 === h ? h = u : u === r && u !== h && (u = h - v), r === h && (u -= v)), u += v) : b === y ? (g--, y = void 0) : j.optional ? (u += v, p -= v) : j.fallback ? (l[k](j.fallback), u += v, p -= v) : s.invalid.push({
                        p: p,
                        v: b,
                        e: j.pattern
                    }), p += v) : (t || l[k](M), b === M ? (m.push(p), p += v) : (y = M, m.push(p + g), g++), u += v)
                }
                return c = e.charAt(r), f !== d + 1 || o.translation[c] || l.push(c), l = l.join(""), s.mapMaskdigitPositions(l, m, d), l
            }, mapMaskdigitPositions: function (t, a, e) {
                for (t = n.reverse ? t.length - e : 0, s.maskDigitPosMap = {}, e = 0; e < a.length; e++) s.maskDigitPosMap[a[e] + t] = 1
            }, callbacks: function (t) {
                var r = s.val(), o = r !== i, l = [r, t, a, n], c = function (t, a, e) {
                    "function" == typeof n[t] && a && n[t].apply(this, e)
                };
                c("onChange", !0 === o, l), c("onKeyPress", !0 === o, l), c("onComplete", r.length === e.length, l), c("onInvalid", 0 < s.invalid.length, [r, t, a, s.invalid, n])
            }
        };
        a = t(a);
        var r, o = this, i = s.val();
        e = "function" == typeof e ? e(s.val(), void 0, a, n) : e, o.mask = e, o.options = n, o.remove = function () {
            var t = s.getCaret();
            return s.destroyEvents(), s.val(o.getCleanVal()), s.setCaret(t), a
        }, o.getCleanVal = function () {
            return s.getMasked(!0)
        }, o.getMaskedVal = function (t) {
            return s.getMasked(!1, t)
        }, o.init = function (i) {
            if (i = i || !1, n = n || {}, o.clearIfNotMatch = t.jMaskGlobals.clearIfNotMatch, o.byPassKeys = t.jMaskGlobals.byPassKeys, o.translation = t.extend({}, t.jMaskGlobals.translation, n.translation), o = t.extend(!0, {}, o, n), r = s.getRegexMask(), i) s.events(), s.val(s.getMasked()); else {
                n.placeholder && a.attr("placeholder", n.placeholder), a.data("mask") && a.attr("autocomplete", "off"), i = 0;
                for (var l = !0; i < e.length; i++) {
                    var c = o.translation[e.charAt(i)];
                    if (c && c.recursive) {
                        l = !1;
                        break
                    }
                }
                l && a.attr("maxlength", e.length), s.destroyEvents(), s.events(), i = s.getCaret(), s.val(s.getMasked()), s.setCaret(i)
            }
        }, o.init(!a.is("input"))
    };
    t.maskWatchers = {};
    var e = function () {
        var e = t(this), s = {}, r = e.attr("data-mask");
        if (e.attr("data-mask-reverse") && (s.reverse = !0), e.attr("data-mask-clearifnotmatch") && (s.clearIfNotMatch = !0), "true" === e.attr("data-mask-selectonfocus") && (s.selectOnFocus = !0), n(e, r, s)) return e.data("mask", new a(this, r, s))
    }, n = function (a, e, n) {
        n = n || {};
        var s = t(a).data("mask"), r = JSON.stringify;
        a = t(a).val() || t(a).text();
        try {
            return "function" == typeof e && (e = e(a)), "object" != typeof s || r(s.options) !== r(n) || s.mask !== e
        } catch (t) {
        }
    }, s = function (t) {
        var a, e = document.createElement("div");
        return t = "on" + t, (a = t in e) || (e.setAttribute(t, "return;"), a = "function" == typeof e[t]), a
    };
    t.fn.mask = function (e, s) {
        s = s || {};
        var r = this.selector, o = (i = t.jMaskGlobals).watchInterval, i = s.watchInputs || i.watchInputs,
            l = function () {
                if (n(this, e, s)) return t(this).data("mask", new a(this, e, s))
            };
        return t(this).each(l), r && "" !== r && i && (clearInterval(t.maskWatchers[r]), t.maskWatchers[r] = setInterval(function () {
            t(document).find(r).each(l)
        }, o)), this
    }, t.fn.masked = function (t) {
        return this.data("mask").getMaskedVal(t)
    }, t.fn.unmask = function () {
        return clearInterval(t.maskWatchers[this.selector]), delete t.maskWatchers[this.selector], this.each(function () {
            var a = t(this).data("mask");
            a && a.remove().removeData("mask")
        })
    }, t.fn.cleanVal = function () {
        return this.data("mask").getCleanVal()
    }, t.applyDataMask = function (a) {
        ((a = a || t.jMaskGlobals.maskElements) instanceof t ? a : t(a)).filter(t.jMaskGlobals.dataMaskAttr).each(e)
    }, s = {
        maskElements: "input,td,span,div",
        dataMaskAttr: "*[data-mask]",
        dataMask: !0,
        watchInterval: 300,
        watchInputs: !0,
        useInput: !/Chrome\/[2-4][0-9]|SamsungBrowser/.test(window.navigator.userAgent) && s("input"),
        watchDataMask: !1,
        byPassKeys: [9, 16, 17, 18, 36, 37, 38, 39, 40, 91],
        translation: {
            0: {pattern: /\d/},
            9: {pattern: /\d/, optional: !0},
            "#": {pattern: /\d/, recursive: !0},
            A: {pattern: /[a-zA-Z0-9]/},
            S: {pattern: /[a-zA-Z]/}
        }
    }, t.jMaskGlobals = t.jMaskGlobals || {}, (s = t.jMaskGlobals = t.extend(!0, {}, s, t.jMaskGlobals)).dataMask && t.applyDataMask(), setInterval(function () {
        t.jMaskGlobals.watchDataMask && t.applyDataMask()
    }, s.watchInterval)
}, window.jQuery, window.Zepto);
!function (t) {
    "function" == typeof define && define.amd ? define(["jquery"], t) : "object" == typeof module && module.exports ? module.exports = t(require("jquery")) : t(jQuery)
}(function (t) {
    t.extend(t.fn, {
        validate: function (e) {
            if (this.length) {
                var i = t.data(this[0], "validator");
                return i || (this.attr("novalidate", "novalidate"), i = new t.validator(e, this[0]), t.data(this[0], "validator", i), i.settings.onsubmit && (this.on("click.validate", ":submit", function (e) {
                    i.submitButton = e.currentTarget, t(this).hasClass("cancel") && (i.cancelSubmit = !0), void 0 !== t(this).attr("formnovalidate") && (i.cancelSubmit = !0)
                }), this.on("submit.validate", function (e) {
                    function s() {
                        var s, n;
                        return i.submitButton && (i.settings.submitHandler || i.formSubmitted) && (s = t("<input type='hidden'/>").attr("name", i.submitButton.name).val(t(i.submitButton).val()).appendTo(i.currentForm)), !i.settings.submitHandler || (n = i.settings.submitHandler.call(i, i.currentForm, e), s && s.remove(), void 0 !== n && n)
                    }

                    return i.settings.debug && e.preventDefault(), i.cancelSubmit ? (i.cancelSubmit = !1, s()) : i.form() ? i.pendingRequest ? (i.formSubmitted = !0, !1) : s() : (i.focusInvalid(), !1)
                })), i)
            }
            e && e.debug && window.console && console.warn("Nothing selected, can't validate, returning nothing.")
        }, valid: function () {
            var e, i, s;
            return t(this[0]).is("form") ? e = this.validate().form() : (s = [], e = !0, i = t(this[0].form).validate(), this.each(function () {
                (e = i.element(this) && e) || (s = s.concat(i.errorList))
            }), i.errorList = s), e
        }, rules: function (e, i) {
            var s, n, r, a, o, l, h = this[0];
            if (null != h && (!h.form && h.hasAttribute("contenteditable") && (h.form = this.closest("form")[0], h.name = this.attr("name")), null != h.form)) {
                if (e) switch (s = t.data(h.form, "validator").settings, n = s.rules, r = t.validator.staticRules(h), e) {
                    case"add":
                        t.extend(r, t.validator.normalizeRule(i)), delete r.messages, n[h.name] = r, i.messages && (s.messages[h.name] = t.extend(s.messages[h.name], i.messages));
                        break;
                    case"remove":
                        return i ? (l = {}, t.each(i.split(/\s/), function (t, e) {
                            l[e] = r[e], delete r[e]
                        }), l) : (delete n[h.name], r)
                }
                return (a = t.validator.normalizeRules(t.extend({}, t.validator.classRules(h), t.validator.attributeRules(h), t.validator.dataRules(h), t.validator.staticRules(h)), h)).required && (o = a.required, delete a.required, a = t.extend({required: o}, a)), a.remote && (o = a.remote, delete a.remote, a = t.extend(a, {remote: o})), a
            }
        }
    }), t.extend(t.expr.pseudos || t.expr[":"], {
        blank: function (e) {
            return !t.trim("" + t(e).val())
        }, filled: function (e) {
            var i = t(e).val();
            return null !== i && !!t.trim("" + i)
        }, unchecked: function (e) {
            return !t(e).prop("checked")
        }
    }), t.validator = function (e, i) {
        this.settings = t.extend(!0, {}, t.validator.defaults, e), this.currentForm = i, this.init()
    }, t.validator.format = function (e, i) {
        return 1 === arguments.length ? function () {
            var i = t.makeArray(arguments);
            return i.unshift(e), t.validator.format.apply(this, i)
        } : void 0 === i ? e : (arguments.length > 2 && i.constructor !== Array && (i = t.makeArray(arguments).slice(1)), i.constructor !== Array && (i = [i]), t.each(i, function (t, i) {
            e = e.replace(new RegExp("\\{" + t + "\\}", "g"), function () {
                return i
            })
        }), e)
    }, t.extend(t.validator, {
        defaults: {
            messages: {},
            groups: {},
            rules: {},
            errorClass: "error",
            pendingClass: "pending",
            validClass: "valid",
            errorElement: "label",
            focusCleanup: !1,
            focusInvalid: !0,
            errorContainer: t([]),
            errorLabelContainer: t([]),
            onsubmit: !0,
            ignore: ":hidden",
            ignoreTitle: !1,
            onfocusin: function (t) {
                this.lastActive = t, this.settings.focusCleanup && (this.settings.unhighlight && this.settings.unhighlight.call(this, t, this.settings.errorClass, this.settings.validClass), this.hideThese(this.errorsFor(t)))
            },
            onfocusout: function (t) {
                this.checkable(t) || !(t.name in this.submitted) && this.optional(t) || this.element(t)
            },
            onkeyup: function (e, i) {
                9 === i.which && "" === this.elementValue(e) || -1 !== t.inArray(i.keyCode, [16, 17, 18, 20, 35, 36, 37, 38, 39, 40, 45, 144, 225]) || (e.name in this.submitted || e.name in this.invalid) && this.element(e)
            },
            onclick: function (t) {
                t.name in this.submitted ? this.element(t) : t.parentNode.name in this.submitted && this.element(t.parentNode)
            },
            highlight: function (e, i, s) {
                "radio" === e.type ? this.findByName(e.name).addClass(i).removeClass(s) : t(e).addClass(i).removeClass(s)
            },
            unhighlight: function (e, i, s) {
                "radio" === e.type ? this.findByName(e.name).removeClass(i).addClass(s) : t(e).removeClass(i).addClass(s)
            }
        },
        setDefaults: function (e) {
            t.extend(t.validator.defaults, e)
        },
        messages: {
            required: "This field is required.",
            remote: "Please fix this field.",
            email: "Please enter a valid email address.",
            url: "Please enter a valid URL.",
            date: "Please enter a valid date.",
            dateISO: "Please enter a valid date (ISO).",
            number: "Please enter a valid number.",
            digits: "Please enter only digits.",
            equalTo: "Please enter the same value again.",
            maxlength: t.validator.format("Please enter no more than {0} characters."),
            minlength: t.validator.format("Please enter at least {0} characters."),
            rangelength: t.validator.format("Please enter a value between {0} and {1} characters long."),
            range: t.validator.format("Please enter a value between {0} and {1}."),
            max: t.validator.format("Please enter a value less than or equal to {0}."),
            min: t.validator.format("Please enter a value greater than or equal to {0}."),
            step: t.validator.format("Please enter a multiple of {0}.")
        },
        autoCreateRanges: !1,
        prototype: {
            init: function () {
                function e(e) {
                    !this.form && this.hasAttribute("contenteditable") && (this.form = t(this).closest("form")[0], this.name = t(this).attr("name"));
                    var i = t.data(this.form, "validator"), s = "on" + e.type.replace(/^validate/, ""), n = i.settings;
                    n[s] && !t(this).is(n.ignore) && n[s].call(i, this, e)
                }

                this.labelContainer = t(this.settings.errorLabelContainer), this.errorContext = this.labelContainer.length && this.labelContainer || t(this.currentForm), this.containers = t(this.settings.errorContainer).add(this.settings.errorLabelContainer), this.submitted = {}, this.valueCache = {}, this.pendingRequest = 0, this.pending = {}, this.invalid = {}, this.reset();
                var i, s = this.groups = {};
                t.each(this.settings.groups, function (e, i) {
                    "string" == typeof i && (i = i.split(/\s/)), t.each(i, function (t, i) {
                        s[i] = e
                    })
                }), i = this.settings.rules, t.each(i, function (e, s) {
                    i[e] = t.validator.normalizeRule(s)
                }), t(this.currentForm).on("focusin.validate focusout.validate keyup.validate", ":text, [type='password'], [type='file'], select, textarea, [type='number'], [type='search'], [type='tel'], [type='url'], [type='email'], [type='datetime'], [type='date'], [type='month'], [type='week'], [type='time'], [type='datetime-local'], [type='range'], [type='color'], [type='radio'], [type='checkbox'], [contenteditable], [type='button']", e).on("click.validate", "select, option, [type='radio'], [type='checkbox']", e), this.settings.invalidHandler && t(this.currentForm).on("invalid-form.validate", this.settings.invalidHandler)
            }, form: function () {
                return this.checkForm(), t.extend(this.submitted, this.errorMap), this.invalid = t.extend({}, this.errorMap), this.valid() || t(this.currentForm).triggerHandler("invalid-form", [this]), this.showErrors(), this.valid()
            }, checkForm: function () {
                this.prepareForm();
                for (var t = 0, e = this.currentElements = this.elements(); e[t]; t++) this.check(e[t]);
                return this.valid()
            }, element: function (e) {
                var i, s, n = this.clean(e), r = this.validationTargetFor(n), a = this, o = !0;
                return void 0 === r ? delete this.invalid[n.name] : (this.prepareElement(r), this.currentElements = t(r), (s = this.groups[r.name]) && t.each(this.groups, function (t, e) {
                    e === s && t !== r.name && (n = a.validationTargetFor(a.clean(a.findByName(t)))) && n.name in a.invalid && (a.currentElements.push(n), o = a.check(n) && o)
                }), i = !1 !== this.check(r), o = o && i, this.invalid[r.name] = !i, this.numberOfInvalids() || (this.toHide = this.toHide.add(this.containers)), this.showErrors(), t(e).attr("aria-invalid", !i)), o
            }, showErrors: function (e) {
                if (e) {
                    var i = this;
                    t.extend(this.errorMap, e), this.errorList = t.map(this.errorMap, function (t, e) {
                        return {message: t, element: i.findByName(e)[0]}
                    }), this.successList = t.grep(this.successList, function (t) {
                        return !(t.name in e)
                    })
                }
                this.settings.showErrors ? this.settings.showErrors.call(this, this.errorMap, this.errorList) : this.defaultShowErrors()
            }, resetForm: function () {
                t.fn.resetForm && t(this.currentForm).resetForm(), this.invalid = {}, this.submitted = {}, this.prepareForm(), this.hideErrors();
                var e = this.elements().removeData("previousValue").removeAttr("aria-invalid");
                this.resetElements(e)
            }, resetElements: function (t) {
                var e;
                if (this.settings.unhighlight) for (e = 0; t[e]; e++) this.settings.unhighlight.call(this, t[e], this.settings.errorClass, ""), this.findByName(t[e].name).removeClass(this.settings.validClass); else t.removeClass(this.settings.errorClass).removeClass(this.settings.validClass)
            }, numberOfInvalids: function () {
                return this.objectLength(this.invalid)
            }, objectLength: function (t) {
                var e, i = 0;
                for (e in t) void 0 !== t[e] && null !== t[e] && !1 !== t[e] && i++;
                return i
            }, hideErrors: function () {
                this.hideThese(this.toHide)
            }, hideThese: function (t) {
                t.not(this.containers).text(""), this.addWrapper(t).hide()
            }, valid: function () {
                return 0 === this.size()
            }, size: function () {
                return this.errorList.length
            }, focusInvalid: function () {
                if (this.settings.focusInvalid) try {
                    t(this.findLastActive() || this.errorList.length && this.errorList[0].element || []).filter(":visible").focus().trigger("focusin")
                } catch (t) {
                }
            }, findLastActive: function () {
                var e = this.lastActive;
                return e && 1 === t.grep(this.errorList, function (t) {
                    return t.element.name === e.name
                }).length && e
            }, elements: function () {
                var e = this, i = {};
                return t(this.currentForm).find("input, select, textarea, [contenteditable]").not(":submit, :reset, :image, :disabled").not(this.settings.ignore).filter(function () {
                    var s = this.name || t(this).attr("name");
                    return !s && e.settings.debug && window.console && console.error("%o has no name assigned", this), this.hasAttribute("contenteditable") && (this.form = t(this).closest("form")[0], this.name = s), !(s in i || !e.objectLength(t(this).rules()) || (i[s] = !0, 0))
                })
            }, clean: function (e) {
                return t(e)[0]
            }, errors: function () {
                var e = this.settings.errorClass.split(" ").join(".");
                return t(this.settings.errorElement + "." + e, this.errorContext)
            }, resetInternals: function () {
                this.successList = [], this.errorList = [], this.errorMap = {}, this.toShow = t([]), this.toHide = t([])
            }, reset: function () {
                this.resetInternals(), this.currentElements = t([])
            }, prepareForm: function () {
                this.reset(), this.toHide = this.errors().add(this.containers)
            }, prepareElement: function (t) {
                this.reset(), this.toHide = this.errorsFor(t)
            }, elementValue: function (e) {
                var i, s, n = t(e), r = e.type;
                return "radio" === r || "checkbox" === r ? this.findByName(e.name).filter(":checked").val() : "number" === r && void 0 !== e.validity ? e.validity.badInput ? "NaN" : n.val() : (i = e.hasAttribute("contenteditable") ? n.text() : n.val(), "file" === r ? "C:\\fakepath\\" === i.substr(0, 12) ? i.substr(12) : (s = i.lastIndexOf("/")) >= 0 ? i.substr(s + 1) : (s = i.lastIndexOf("\\")) >= 0 ? i.substr(s + 1) : i : "string" == typeof i ? i.replace(/\r/g, "") : i)
            }, check: function (e) {
                e = this.validationTargetFor(this.clean(e));
                var i, s, n, r, a = t(e).rules(), o = t.map(a, function (t, e) {
                    return e
                }).length, l = !1, h = this.elementValue(e);
                if ("function" == typeof a.normalizer ? r = a.normalizer : "function" == typeof this.settings.normalizer && (r = this.settings.normalizer), r) {
                    if ("string" != typeof(h = r.call(e, h))) throw new TypeError("The normalizer should return a string value.");
                    delete a.normalizer
                }
                for (s in a) {
                    n = {method: s, parameters: a[s]};
                    try {
                        if ("dependency-mismatch" === (i = t.validator.methods[s].call(this, h, e, n.parameters)) && 1 === o) {
                            l = !0;
                            continue
                        }
                        if (l = !1, "pending" === i) return void(this.toHide = this.toHide.not(this.errorsFor(e)));
                        if (!i) return this.formatAndAdd(e, n), !1
                    } catch (t) {
                        throw this.settings.debug && window.console && console.log("Exception occurred when checking element " + e.id + ", check the '" + n.method + "' method.", t), t instanceof TypeError && (t.message += ".  Exception occurred when checking element " + e.id + ", check the '" + n.method + "' method."), t
                    }
                }
                if (!l) return this.objectLength(a) && this.successList.push(e), !0
            }, customDataMessage: function (e, i) {
                return t(e).data("msg" + i.charAt(0).toUpperCase() + i.substring(1).toLowerCase()) || t(e).data("msg")
            }, customMessage: function (t, e) {
                var i = this.settings.messages[t];
                return i && (i.constructor === String ? i : i[e])
            }, findDefined: function () {
                for (var t = 0; t < arguments.length; t++) if (void 0 !== arguments[t]) return arguments[t]
            }, defaultMessage: function (e, i) {
                "string" == typeof i && (i = {method: i});
                var s = this.findDefined(this.customMessage(e.name, i.method), this.customDataMessage(e, i.method), !this.settings.ignoreTitle && e.title || void 0, t.validator.messages[i.method], "<strong>Warning: No message defined for " + e.name + "</strong>"),
                    n = /\$?\{(\d+)\}/g;
                return "function" == typeof s ? s = s.call(this, i.parameters, e) : n.test(s) && (s = t.validator.format(s.replace(n, "{$1}"), i.parameters)), s
            }, formatAndAdd: function (t, e) {
                var i = this.defaultMessage(t, e);
                this.errorList.push({
                    message: i,
                    element: t,
                    method: e.method
                }), this.errorMap[t.name] = i, this.submitted[t.name] = i
            }, addWrapper: function (t) {
                return this.settings.wrapper && (t = t.add(t.parent(this.settings.wrapper))), t
            }, defaultShowErrors: function () {
                var t, e, i;
                for (t = 0; this.errorList[t]; t++) i = this.errorList[t], this.settings.highlight && this.settings.highlight.call(this, i.element, this.settings.errorClass, this.settings.validClass), this.showLabel(i.element, i.message);
                if (this.errorList.length && (this.toShow = this.toShow.add(this.containers)), this.settings.success) for (t = 0; this.successList[t]; t++) this.showLabel(this.successList[t]);
                if (this.settings.unhighlight) for (t = 0, e = this.validElements(); e[t]; t++) this.settings.unhighlight.call(this, e[t], this.settings.errorClass, this.settings.validClass);
                this.toHide = this.toHide.not(this.toShow), this.hideErrors(), this.addWrapper(this.toShow).show()
            }, validElements: function () {
                return this.currentElements.not(this.invalidElements())
            }, invalidElements: function () {
                return t(this.errorList).map(function () {
                    return this.element
                })
            }, showLabel: function (e, i) {
                var s, n, r, a, o = this.errorsFor(e), l = this.idOrName(e), h = t(e).attr("aria-describedby");
                o.length ? (o.removeClass(this.settings.validClass).addClass(this.settings.errorClass), o.html(i)) : (o = t("<" + this.settings.errorElement + ">").attr("id", l + "-error").addClass(this.settings.errorClass).html(i || ""), s = o, this.settings.wrapper && (s = o.hide().show().wrap("<" + this.settings.wrapper + "/>").parent()), this.labelContainer.length ? this.labelContainer.append(s) : this.settings.errorPlacement ? this.settings.errorPlacement.call(this, s, t(e)) : s.insertAfter(e), o.is("label") ? o.attr("for", l) : 0 === o.parents("label[for='" + this.escapeCssMeta(l) + "']").length && (r = o.attr("id"), h ? h.match(new RegExp("\\b" + this.escapeCssMeta(r) + "\\b")) || (h += " " + r) : h = r, t(e).attr("aria-describedby", h), (n = this.groups[e.name]) && (a = this, t.each(a.groups, function (e, i) {
                    i === n && t("[name='" + a.escapeCssMeta(e) + "']", a.currentForm).attr("aria-describedby", o.attr("id"))
                })))), !i && this.settings.success && (o.text(""), "string" == typeof this.settings.success ? o.addClass(this.settings.success) : this.settings.success(o, e)), this.toShow = this.toShow.add(o)
            }, errorsFor: function (e) {
                var i = this.escapeCssMeta(this.idOrName(e)), s = t(e).attr("aria-describedby"),
                    n = "label[for='" + i + "'], label[for='" + i + "'] *";
                return s && (n = n + ", #" + this.escapeCssMeta(s).replace(/\s+/g, ", #")), this.errors().filter(n)
            }, escapeCssMeta: function (t) {
                return t.replace(/([\\!"#$%&'()*+,.\/:;<=>?@\[\]^`{|}~])/g, "\\$1")
            }, idOrName: function (t) {
                return this.groups[t.name] || (this.checkable(t) ? t.name : t.id || t.name)
            }, validationTargetFor: function (e) {
                return this.checkable(e) && (e = this.findByName(e.name)), t(e).not(this.settings.ignore)[0]
            }, checkable: function (t) {
                return /radio|checkbox/i.test(t.type)
            }, findByName: function (e) {
                return t(this.currentForm).find("[name='" + this.escapeCssMeta(e) + "']")
            }, getLength: function (e, i) {
                switch (i.nodeName.toLowerCase()) {
                    case"select":
                        return t("option:selected", i).length;
                    case"input":
                        if (this.checkable(i)) return this.findByName(i.name).filter(":checked").length
                }
                return e.length
            }, depend: function (t, e) {
                return !this.dependTypes[typeof t] || this.dependTypes[typeof t](t, e)
            }, dependTypes: {
                boolean: function (t) {
                    return t
                }, string: function (e, i) {
                    return !!t(e, i.form).length
                }, function: function (t, e) {
                    return t(e)
                }
            }, optional: function (e) {
                var i = this.elementValue(e);
                return !t.validator.methods.required.call(this, i, e) && "dependency-mismatch"
            }, startRequest: function (e) {
                this.pending[e.name] || (this.pendingRequest++, t(e).addClass(this.settings.pendingClass), this.pending[e.name] = !0)
            }, stopRequest: function (e, i) {
                this.pendingRequest--, this.pendingRequest < 0 && (this.pendingRequest = 0), delete this.pending[e.name], t(e).removeClass(this.settings.pendingClass), i && 0 === this.pendingRequest && this.formSubmitted && this.form() ? (t(this.currentForm).submit(), this.submitButton && t("input:hidden[name='" + this.submitButton.name + "']", this.currentForm).remove(), this.formSubmitted = !1) : !i && 0 === this.pendingRequest && this.formSubmitted && (t(this.currentForm).triggerHandler("invalid-form", [this]), this.formSubmitted = !1)
            }, previousValue: function (e, i) {
                return i = "string" == typeof i && i || "remote", t.data(e, "previousValue") || t.data(e, "previousValue", {
                    old: null,
                    valid: !0,
                    message: this.defaultMessage(e, {method: i})
                })
            }, destroy: function () {
                this.resetForm(), t(this.currentForm).off(".validate").removeData("validator").find(".validate-equalTo-blur").off(".validate-equalTo").removeClass("validate-equalTo-blur")
            }
        },
        classRuleSettings: {
            required: {required: !0},
            email: {email: !0},
            url: {url: !0},
            date: {date: !0},
            dateISO: {dateISO: !0},
            number: {number: !0},
            digits: {digits: !0},
            creditcard: {creditcard: !0}
        },
        addClassRules: function (e, i) {
            e.constructor === String ? this.classRuleSettings[e] = i : t.extend(this.classRuleSettings, e)
        },
        classRules: function (e) {
            var i = {}, s = t(e).attr("class");
            return s && t.each(s.split(" "), function () {
                this in t.validator.classRuleSettings && t.extend(i, t.validator.classRuleSettings[this])
            }), i
        },
        normalizeAttributeRule: function (t, e, i, s) {
            /min|max|step/.test(i) && (null === e || /number|range|text/.test(e)) && (s = Number(s), isNaN(s) && (s = void 0)), s || 0 === s ? t[i] = s : e === i && "range" !== e && (t[i] = !0)
        },
        attributeRules: function (e) {
            var i, s, n = {}, r = t(e), a = e.getAttribute("type");
            for (i in t.validator.methods) "required" === i ? ("" === (s = e.getAttribute(i)) && (s = !0), s = !!s) : s = r.attr(i), this.normalizeAttributeRule(n, a, i, s);
            return n.maxlength && /-1|2147483647|524288/.test(n.maxlength) && delete n.maxlength, n
        },
        dataRules: function (e) {
            var i, s, n = {}, r = t(e), a = e.getAttribute("type");
            for (i in t.validator.methods) s = r.data("rule" + i.charAt(0).toUpperCase() + i.substring(1).toLowerCase()), this.normalizeAttributeRule(n, a, i, s);
            return n
        },
        staticRules: function (e) {
            var i = {}, s = t.data(e.form, "validator");
            return s.settings.rules && (i = t.validator.normalizeRule(s.settings.rules[e.name]) || {}), i
        },
        normalizeRules: function (e, i) {
            return t.each(e, function (s, n) {
                if (!1 !== n) {
                    if (n.param || n.depends) {
                        var r = !0;
                        switch (typeof n.depends) {
                            case"string":
                                r = !!t(n.depends, i.form).length;
                                break;
                            case"function":
                                r = n.depends.call(i, i)
                        }
                        r ? e[s] = void 0 === n.param || n.param : (t.data(i.form, "validator").resetElements(t(i)), delete e[s])
                    }
                } else delete e[s]
            }), t.each(e, function (s, n) {
                e[s] = t.isFunction(n) && "normalizer" !== s ? n(i) : n
            }), t.each(["minlength", "maxlength"], function () {
                e[this] && (e[this] = Number(e[this]))
            }), t.each(["rangelength", "range"], function () {
                var i;
                e[this] && (t.isArray(e[this]) ? e[this] = [Number(e[this][0]), Number(e[this][1])] : "string" == typeof e[this] && (i = e[this].replace(/[\[\]]/g, "").split(/[\s,]+/), e[this] = [Number(i[0]), Number(i[1])]))
            }), t.validator.autoCreateRanges && (null != e.min && null != e.max && (e.range = [e.min, e.max], delete e.min, delete e.max), null != e.minlength && null != e.maxlength && (e.rangelength = [e.minlength, e.maxlength], delete e.minlength, delete e.maxlength)), e
        },
        normalizeRule: function (e) {
            if ("string" == typeof e) {
                var i = {};
                t.each(e.split(/\s/), function () {
                    i[this] = !0
                }), e = i
            }
            return e
        },
        addMethod: function (e, i, s) {
            t.validator.methods[e] = i, t.validator.messages[e] = void 0 !== s ? s : t.validator.messages[e], i.length < 3 && t.validator.addClassRules(e, t.validator.normalizeRule(e))
        },
        methods: {
            required: function (e, i, s) {
                if (!this.depend(s, i)) return "dependency-mismatch";
                if ("select" === i.nodeName.toLowerCase()) {
                    var n = t(i).val();
                    return n && n.length > 0
                }
                return this.checkable(i) ? this.getLength(e, i) > 0 : e.length > 0
            }, email: function (t, e) {
                return this.optional(e) || /^[a-zA-Z0-9.!#$%&'*+\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/.test(t)
            }, url: function (t, e) {
                return this.optional(e) || /^(?:(?:(?:https?|ftp):)?\/\/)(?:\S+(?::\S*)?@)?(?:(?!(?:10|127)(?:\.\d{1,3}){3})(?!(?:169\.254|192\.168)(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)(?:\.(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)*(?:\.(?:[a-z\u00a1-\uffff]{2,})).?)(?::\d{2,5})?(?:[\/?#]\S*)?$/i.test(t)
            }, date: function (t, e) {
                return this.optional(e) || !/Invalid|NaN/.test(new Date(t).toString())
            }, dateISO: function (t, e) {
                return this.optional(e) || /^\d{4}[\/\-](0?[1-9]|1[012])[\/\-](0?[1-9]|[12][0-9]|3[01])$/.test(t)
            }, number: function (t, e) {
                return this.optional(e) || /^(?:-?\d+|-?\d{1,3}(?:,\d{3})+)?(?:\.\d+)?$/.test(t)
            }, digits: function (t, e) {
                return this.optional(e) || /^\d+$/.test(t)
            }, minlength: function (e, i, s) {
                var n = t.isArray(e) ? e.length : this.getLength(e, i);
                return this.optional(i) || n >= s
            }, maxlength: function (e, i, s) {
                var n = t.isArray(e) ? e.length : this.getLength(e, i);
                return this.optional(i) || n <= s
            }, rangelength: function (e, i, s) {
                var n = t.isArray(e) ? e.length : this.getLength(e, i);
                return this.optional(i) || n >= s[0] && n <= s[1]
            }, min: function (t, e, i) {
                return this.optional(e) || t >= i
            }, max: function (t, e, i) {
                return this.optional(e) || t <= i
            }, range: function (t, e, i) {
                return this.optional(e) || t >= i[0] && t <= i[1]
            }, step: function (e, i, s) {
                var n, r = t(i).attr("type"), a = "Step attribute on input type " + r + " is not supported.",
                    o = new RegExp("\\b" + r + "\\b"), l = function (t) {
                        var e = ("" + t).match(/(?:\.(\d+))?$/);
                        return e && e[1] ? e[1].length : 0
                    }, h = function (t) {
                        return Math.round(t * Math.pow(10, n))
                    }, u = !0;
                if (r && !o.test(["text", "number", "range"].join())) throw new Error(a);
                return n = l(s), (l(e) > n || h(e) % h(s) != 0) && (u = !1), this.optional(i) || u
            }, equalTo: function (e, i, s) {
                var n = t(s);
                return this.settings.onfocusout && n.not(".validate-equalTo-blur").length && n.addClass("validate-equalTo-blur").on("blur.validate-equalTo", function () {
                    t(i).valid()
                }), e === n.val()
            }, remote: function (e, i, s, n) {
                if (this.optional(i)) return "dependency-mismatch";
                n = "string" == typeof n && n || "remote";
                var r, a, o, l = this.previousValue(i, n);
                return this.settings.messages[i.name] || (this.settings.messages[i.name] = {}), l.originalMessage = l.originalMessage || this.settings.messages[i.name][n], this.settings.messages[i.name][n] = l.message, s = "string" == typeof s && {url: s} || s, o = t.param(t.extend({data: e}, s.data)), l.old === o ? l.valid : (l.old = o, r = this, this.startRequest(i), a = {}, a[i.name] = e, t.ajax(t.extend(!0, {
                    mode: "abort",
                    port: "validate" + i.name,
                    dataType: "json",
                    data: a,
                    context: r.currentForm,
                    success: function (t) {
                        var s, a, o, h = !0 === t || "true" === t;
                        r.settings.messages[i.name][n] = l.originalMessage, h ? (o = r.formSubmitted, r.resetInternals(), r.toHide = r.errorsFor(i), r.formSubmitted = o, r.successList.push(i), r.invalid[i.name] = !1, r.showErrors()) : (s = {}, a = t || r.defaultMessage(i, {
                            method: n,
                            parameters: e
                        }), s[i.name] = l.message = a, r.invalid[i.name] = !0, r.showErrors(s)), l.valid = h, r.stopRequest(i, h)
                    }
                }, s)), "pending")
            }
        }
    });
    var e, i = {};
    return t.ajaxPrefilter ? t.ajaxPrefilter(function (t, e, s) {
        var n = t.port;
        "abort" === t.mode && (i[n] && i[n].abort(), i[n] = s)
    }) : (e = t.ajax, t.ajax = function (s) {
        var n = ("mode" in s ? s : t.ajaxSettings).mode, r = ("port" in s ? s : t.ajaxSettings).port;
        return "abort" === n ? (i[r] && i[r].abort(), i[r] = e.apply(this, arguments), i[r]) : e.apply(this, arguments)
    }), t
});

$(document).ready(function () {
    $("#phone").mask("+7 (000) 000-0000"), $(".form-group input").focusout(function (e) {
        $(this).val() ? $(this).addClass("focused") : $(this).removeClass("focused")
    }), $(".toggle").click(function () {
        $(".nav").toggle("fold", 1e3)

    });
    $("#callback-form").validate({
        rules: {name: {required: !0}, phone: {required: !0}},
        messages: {phone: "Введите ваш Телефон", name: "Введите ваше Имя"},
        submitHandler: function (e) {
            var a = $(e), n = a.find('[name="name"]').val(), o = a.find('[name="phone"]').val();
            $.ajax({
                type: "POST", url: "../mail.php", data: {name: n, phone: o},
                success: function (e) {
                    yaCounter47132289.reachGoal('zakaz'),
                        gtag('event','zakaz', {
                            eventCategory: 'zakaz',
                            eventAction: 'zakaz',
                        });
                    "Message sent! Thanks for contacting us." == e && $("#callback").modal("hide")
                }
            })
        }
    })
});


$(document).ready(function(){
    $(".combos").on("click","a", function (event) {
        //отменяем стандартную обработку нажатия по ссылке
        event.preventDefault();

        //забираем идентификатор бока с атрибута href
        var id  = $(this).attr('href'),

            //узнаем высоту от начала страницы до блока на который ссылается якорь
            top = $(id).offset().top;

        //анимируем переход на расстояние - top за 1500 мс
        $('body,html').animate({scrollTop: top}, 1500);
    });

    $(".nav").on("click","a", function (event) {
        //отменяем стандартную обработку нажатия по ссылке
        event.preventDefault();

        //забираем идентификатор бока с атрибута href
        var id  = $(this).attr('href'),

            //узнаем высоту от начала страницы до блока на который ссылается якорь
            top = $(id).offset().top;

        //анимируем переход на расстояние - top за 1500 мс
        $('body,html').animate({scrollTop: top}, 1500);
    });
});