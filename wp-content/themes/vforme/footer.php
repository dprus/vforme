
<footer>
	<div class="container">
		<div class="row" id="contacts">
			<div class="col-md-6 col-sm-12 col-xs-12">
				<div class="w_contact">
					<h2 class="w_contact__head">
						Мы находимся:
					</h2>
					<div class="w_contact__desc">
						г. Белгород улица Благодатная 5а <br>
						Работаем в будни <span class="w_contact__time">с 07:00 до 23:00</span> <br>
						Выходные <span class="w_contact__time">с 08:00 до 22:00</span> <br>
						<span class="w_contact__number">+7 (4722) 41-18-18</span>
					</div>
					<ul class="soc_icon">
						<li>
							<a href="https://vk.com/yavforme31">
								<i class="fa fa-vk" aria-hidden="true"></i>
							</a>
						</li>
						<li>
							<a href="https://www.instagram.com/yavforme31/">
								<i class="fa fa-instagram" aria-hidden="true"></i>
							</a>
						</li>
						<li>
							<a href="https://t.me/yavforme31">
								<i class="fa fa-telegram" aria-hidden="true"></i>
							</a>
						</li>
						<li>
							<a href="https://ok.ru/group/58758418071610">
								<i class="fa fa-odnoklassniki" aria-hidden="true"></i>
							</a>
						</li>
						<li>
							<a href="https://www.facebook.com/%D0%A4%D0%B8%D1%82%D0%BD%D0%B5%D1%81-%D0%BA%D0%BB%D1%83%D0%B1-%D1%8F%D0%B2%D1%84%D0%BE%D1%80%D0%BC%D0%B5-2268465413252172/">
								<i class="fa fa-facebook-square" aria-hidden="true"></i>
							</a>
						</li>
					</ul>
					<div class="w_contact__button">
						<a href="http://xn--b1aguhkw9e.xn--p1ai/" class="btn btn-sm">Основной сайт</a>
						<a href="#" class="btn btn-sm" data-toggle="modal" data-target="#callback"> Заказать
							звонок</a>
					</div>
				</div>
			</div>
			<div class="col-md-6 col-sm-12 col-xs-12">
				<div class="w_contact__map">
					<script type="text/javascript" charset="utf-8" async src="https://api-maps.yandex.ru/services/constructor/1.0/js/?um=constructor%3A070e8975b240d707da511de14d6b7fa060061a63035dedb760002c2650fcb75f&amp;width=100%25&amp;height=445&amp;lang=ru_RU&amp;scroll=true"></script>
				</div>
			</div>
		</div>
	</div>
	<div class="w_footer">
		<div class="container">
			<div class="row">
				<div class="col-md-4">
					<div class="footer_politic">
						<a href="./privatePolicy.html">Политика конфиденциальности</a>
					</div>
				</div>
				<div class="col-md-4">
					<div class="footer">
						<div class="footer_logo">
							<img src="<?=get_template_directory_uri();?>/img/h-logo.png" alt="logo" class="img-fluid">
						</div>
					</div>
				</div>
				<div class="col-md-4">
					<div class="footer">
						<div class="footer_dp">
							<div class="footer_dp__logo">
								<a href="http://dprus.com/"><img src="<?=get_template_directory_uri();?>/img/dp_logo.png" alt="dp_logo"></a>
							</div>
							<p class="footer_dp__desc">
								<a href="http://dprus.com/">
									Разработано консалтинговой компанией <br>
									«Деловой Потенциал»
								</a>
							</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="sub_desc">
					*Рассрочка – это кредитный продукт, предоставляемый кредитными учреждениями. Организатор акции
					предоставляет покупателям скидку с цены товара, приобретаемого в кредит. В итоге переплаты за товар
					в размере процентов за пользование кредитом не возникает и фактически выплаченная сумма за товар
					непревышает его первоначальной стоимости при условии, что кредит предоставляется только на товар.
					Указанные условия определяют полную стоимость кредита (займа), рассчитанную в соответствии с
					требованиями ФЗ «О потребительском кредите (займе)». Кредит предоставляется банком-партнёром ООО
					«Халва» и ООО «ОТП Банк». Первоначальный взнос 0 рублей. Переплата по кредиту составляет 0% от цены
					товара. Процентная ставка составляет 29,9% годовых. Срок действия договора 6месяцев. Кредит
					погашается ежемесячными равными (аннуитетными) платежами. В зависимости от клуба, в котором
					приобретается абонемент, список банков-партнёров, предоставляющих услуги, может меняться. Указанные
					цены действительны с 01.12.17 по 31.12.18.
				</div>
			</div>
		</div>
	</div>
</footer>


<div class="modal fade" id="callback" tabindex="-1" role="dialog">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="window">
				<a href="#" class="window_close" data-dismiss="modal" aria-label="Close"><i class="fa fa-times"
				                                                                            aria-hidden="true"></i></a>
				<div class="h_logo">
					<a href="#"><img src="<?=get_template_directory_uri();?>/img/modal-logo.png" alt="modal-logo"></a>
					<p class="h_logo_desc">фитнес клуб</p>
				</div>
				<form id="callback-form">
					<div class="form-group">
						<input type="text" id="name" name="name">
						<label for="name">Ваше имя</label>
					</div>
					<div class="form-group">
						<input type="text" id="phone" name="phone">
						<label for="phone">Ваш телефон</label>
					</div>
					<div class="form-group">

						<label for="acceptance" class="checkbox-custom">
							Согласие на обработку <a href="./privatePolicy.html"> персональных данных</a>
							<input type="checkbox" name="acceptance" id="acceptance" checked>
							<span class="checkmark"></span>
						</label>
					</div>
					<div class="form-group">
						<input type="submit" class="btn btn-xs" onclick="yaCounter47132289.reachGoal ('zakaz'); return true;">Заказать звонок</input>

					</div>
				</form>
			</div>
		</div>
	</div>
</div>

<? wp_footer();?>

</body>
</html>