<!DOCTYPE html>
<html lang="en">
<head>
    <title>ФИТНЕС-КЛУБ #ЯВФОРМЕ | Белгород </title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="icon" type="image/png" href="favicon.png" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

	<?php wp_head();?>
</head>
<body>

<header>
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="h_logo">
                    <a href="#"><img src="<?=get_template_directory_uri();?>/img/h-logo.png" alt="h-logo"></a>
                    <p class="h_logo_desc">фитнес-клуб</p>
                </div>
            </div>
            <div class="col-md-6">
                <div class="number">
                    <a href="#" class="btn btn-sm" data-toggle="modal" data-target="#callback">заказать звонок</a>
                    <span class="number_num">+7 (4722) 41-18-18</span>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <nav>
                    <div class="toggle">
                        <span></span>
                        <span></span>
                        <span></span>
                    </div>

                    <?php wp_nav_menu(array('theme_location'=>'top', 'menu_class'=>'nav'));?>




                </nav>
            </div>
        </div>
    </div>
</header>