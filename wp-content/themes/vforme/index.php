
<?get_header();?>

<main>

    <section class="combos">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12">
                    <div class="title">
                        <span class="combos_title__color">«Новогоднее комбо»</span> <br>
                        для годового абонемента <br>
                        Только до конца января!
                    </div>
                    <!--<div class="combos_gift">-->
                        <!--<div class="combos_gift_img"><img src="./img/gift.png" alt="gift"></div>-->
                        <!--<div class="combos_gift__text">приобрети 1 год и получи в подарок:</div>-->
                    <!--</div>-->
                    <div class="combos_desc">
                        <!--<div class="combos_desc__img">-->
                            <!--<div class="part_fetnes__line">-->
                                <!--<p class="part_fetnes__item">1 год + 4 Персональные тренировки</p>-->
                            <!--</div>-->
                        <!--</div>-->
                        <!--<div class="combos_desc__img">-->
                            <!--<div class="part_fetnes__line">-->
                                <!--<p class="part_fetnes__item">6 месяцев + 3 Персональные тренировки</p>-->
                            <!--</div>-->
                        <!--</div>-->
                        <!--<div class="combos_desc__img">-->
                            <!--<div class="part_fetnes__line">-->
                                <!--<p class="part_fetnes__item">3 месяца + 2 Персональные тренировки</p>-->
                            <!--</div>-->
                        <!--</div>-->
                        <!--<div class="combos_desc__img">-->
                            <!--<div class="part_fetnes__line">-->
                                <!--<p class="part_fetnes__item">1 месяц + 1 Персональная тренировка</p>-->
                            <!--</div>-->
                        <!--</div>-->

                    </div>
                </div>
                <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
                    <div class="combos_desc__img">
                        <div class="combos_desc__img_box">
                            <img src="./img/giftbox.png" alt="giftbox">
                        </div>
                        <p class="combos_gift__item-head"><span>в подарок:</span></p>
                    </div>

                    <div class="combos_desc__img">
                        <div class="combos_desc__img_box">
                            <img src="./img/1.png" alt="muscles">
                        </div>
                        <p class="combos_gift__item">4 персональные тренировки</p>
                    </div>

                    <div class="combos_desc__img">
                        <div class="combos_desc__img_box">
                            <img src="./img/2.png" alt="balispa">
                        </div>
                        <p class="combos_gift__item">3 часовых сеанса массажа</p>
                    </div>
                    <div class="combos_desc__img">
                        <div class="combos_desc__img_box">
                            <img src="./img/3.png" alt="lazer">
                        </div>
                        <p class="combos_gift__item">1 сеанс лазерной эпиляции</p>
                    </div>
                </div>
                <div class="col-lg-5 offset-lg-1 col-md-12 col-sm-12 col-xs-12">
                    <div class="row combos_column">
                        <div class="col-md-12">
                            <div class="subscription_number">
                                <a href="#" class="btn btn-lg-sub" data-toggle="modal" data-target="#callback">Оставить заявку на абонемент</a>
                                <a href="#" class="btn btn-trans" data-toggle="modal" data-target="#callback">Подробнее</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="combos_next">
                        <a href="#subscription"><img src="./img/next.png" alt="next"></a>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="part_fetnes" id="part_fetnes">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="part_fetnes__box">
                        <div class="w_direction">
                            <h2 class="title">СТАНЬТЕ ЧАСТЬЮ ФИТНЕС-КЛУБА #<span class="title_colo">Я</span>ВФОРМЕ!</h2>
                            <h3 class="title_up">
                                <span>
                                    Чтобы вы чувствовали себя комфортно
                                    в нашем фитнес-клубе, мы предоставляем
                                    следующие услуги:
                                </span>
                            </h3>
                        </div>
                        <!--<div class="combos_title">-->

                        <!--</div>-->
                        <!--<h5 class="title_line">-->
                        <!--Станьте частью фитнес-клуба &nbsp; <span>#<span>Я</span>ВФОРМЕ!</span>-->
                        <!--</h5>-->
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="part_fetnes__desc">
                        <!--Чтобы вы чувствовали себя комфортно-->
                        <!--в нашем фитнес-клубе, мы предоставляем-->
                        <!--следующие услуги:-->
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="combos_desc">
                        <div class="combos_desc__img">
                            <div class="part_fetnes__line">
                                <p class="part_fetnes__item">Большая парковка</p>
                            </div>
                        </div>
                        <div class="combos_desc__img">
                            <div class="part_fetnes__line">
                                <p class="part_fetnes__item">Чистые и комфортные душевые</p>
                            </div>
                        </div>
                        <div class="combos_desc__img">
                            <div class="part_fetnes__line">
                                <p class="part_fetnes__item">Предоставление полотенца</p>
                            </div>
                        </div>
                        <div class="combos_desc__img">
                            <div class="part_fetnes__line">
                                <p class="part_fetnes__item">Персональный шкафчик в раздевалке</p>
                            </div>
                        </div>
                        <div class="combos_desc__img">
                            <div class="part_fetnes__line">
                                <p class="part_fetnes__item">Питьевая вода</p>
                            </div>
                        </div>
                        <div class="combos_desc__img">
                            <div class="part_fetnes__line">
                                <p class="part_fetnes__item">Бесплатный Wi-Fi</p>
                            </div>
                        </div>
                        <div class="combos_desc__img">
                            <div class="part_fetnes__line">
                                <p class="part_fetnes__item">Бесплатный вводный инструктаж в тренажерном зале</p>
                            </div>
                        </div>
                        <div class="combos_desc__img">
                            <div class="part_fetnes__line">
                                <p class="part_fetnes__item">Отзывчивый администратор и персонал</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="part_fetnes__video">
                        <iframe src="https://player.vimeo.com/video/220016937" width="100%" height="100%" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
                    </div>
                    <!---->
                    <!--<p><a href="">ФК #ЯВФОРМЕ</a> from <a href="https://vimeo.com/dprus">DELOVOY POTENCIAL</a> on <a href="https://vimeo.com">Vimeo</a>.</p>-->
                </div>
            </div>
        </div>
    </section>

    <section class="subscription" id="subscription">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="subscription__box">
                        <h1 class="title">
                            Продлите действующий <br>
                            <span class="combos_title__color">абонемент по выгодным условиям</span>
                        </h1>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <div class="subscription_unlimited" id="mes_1">
                        <div class="wrap_bord">
                            <p class="subscription_unlimited__title">Безлимитный абонемент</p>
                            <p class="subscription_unlimited__year">1 месяц</p>
                            <!--<div class="subscription_desc">-->
                            <!--<div class="subscription_desc__img">-->
                            <!--<div class="subscription_desc__img_box">-->
                            <!--<img src="./img/muscles-black.png" alt="muscles-black">-->
                            <!--</div>-->
                            <!--<p class="subscription_desc__item">1 персональная тренировка <br> с инструктором</p>-->
                            <!--</div>-->
                            <!--</div>-->
                            <div class="subscription_number">
                                <p class="subscription_number__text">3 000 Р</p>
                                <a href="#" class="btn btn-lg" data-toggle="modal" data-target="#callback">ЗАКАЗАТЬ</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="subscription_unlimited" id="mes_3">
                        <div class="wrap_bord">
                            <p class="subscription_unlimited__title">Безлимитный абонемент</p>
                            <p class="subscription_unlimited__year">3 месяца</p>
                            <!--<div class="subscription_desc">-->
                            <!--<div class="subscription_desc__img">-->
                            <!--<div class="subscription_desc__img_box">-->
                            <!--<img src="./img/muscles-black.png" alt="muscles-black">-->
                            <!--</div>-->
                            <!--<p class="subscription_desc__item">2 персональные тренировки <br> с инструктором</p>-->
                            <!--</div>-->
                            <!--</div>-->
                            <div class="subscription_number">
                                <p class="subscription_number__text">7 000 Р</p>
                                <a href="#" class="btn btn-lg" data-toggle="modal" data-target="#callback">ЗАКАЗАТЬ</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="subscription_unlimited" id="mes_6">
                        <div class="wrap_bord">
                            <p class="subscription_unlimited__title">Безлимитный абонемент</p>
                            <p class="subscription_unlimited__year">6 месяцев</p>
                            <!--<div class="subscription_desc">-->
                            <!--<div class="subscription_desc__img">-->
                            <!--<div class="subscription_desc__img_box">-->
                            <!--<img src="./img/muscles-black.png" alt="muscles-black">-->
                            <!--</div>-->
                            <!--<p class="subscription_desc__item">3 персональные тренировки <br> с инструктором</p>-->
                            <!--</div>-->
                            <!--</div>-->
                            <div class="subscription_number">
                                <p class="subscription_number__text">10 000 Р</p>
                                <a href="#" class="btn btn-lg" data-toggle="modal" data-target="#callback">ЗАКАЗАТЬ</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="year">
        <div class="container year_border">
            <div class="row">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="year_wrapp">
                                <div class="year_header">
                                    <div class="year_header__title">
                                        1год
                                    </div>
                                    <div class="year_header__desc">
                                        <span>«Новогоднее комбо»</span>
                                        Безлимитный абонемент
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="year_wrapp">
                                <div class="year_header">
                                    <div class="year_header__price">
                                        Цена: <br>
                                        <span>16000р</span>
                                    </div>
                                    <div class="year_header__button">
                                        <div class="subscription_number">
                                            <a href="#" class="btn btn-lg-sub" data-toggle="modal" data-target="#callback">Оставить заявку на абонемент</a>
                                            <a href="#" class="btn btn-trans" data-toggle="modal" data-target="#callback">Подробнее</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-12">
                            <div class="row set">
                                <div class="col-lg-4">
                                    <div class="year_set">
                                        <div class="set_img">
                                            <img src="./img/1.png" alt="1.png">
                                        </div>
                                        <div class="year_desc">4 персональные тренировки</div>
                                        <div class="year_text">
                                            Тренировки с профессионалом <br>
                                            приблизят к результату намного <br>
                                            скорей. Вы получите ценные <br>
                                            консультации по питанию <br>
                                            и плану тренировок
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 year_line">
                                    <div class="year_set">
                                        <div class="set_img">
                                            <img src="./img/2.png" alt="1.png">
                                        </div>
                                        <div class="year_desc">3 сеанса массажа</div>
                                        <div class="year_text">
                                            3 сеанса фитнес-массажа <br>
                                            от нашего партнера BaliSpa, <br>
                                            каждый из которых длится 1 час. <br>
                                            Лучшее средство для <br>
                                            расслабления мышц после <br>
                                            тренировки и обновления <br>
                                            внутренних сил и настроения.
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="year_set">
                                        <div class="set_img">
                                            <img src="./img/lazer.png" alt="1.png">
                                        </div>
                                        <div class="year_desc">1 сеанс лазерной эпиляции</div>
                                        <div class="year_text">
                                            Диодная лазерная эпиляция <br>
                                            для одной зоны на современном <br>
                                            оборудовании в ТутГладко. <br>
                                            Избавление от нежелательных <br>
                                            волос надолго, без боли и вреда <br>
                                            для здоровья
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="dream">
        <div class="container">
            <div class="row">
                <div class="col-md-10">
                    <div class="title">
                        Рассрочка на абонементы: <br>
                        <span class="combos_title__color">занимайся сейчас - плати потом</span>
                    </div>
                    <div class="dream_desc">
                        Воспользуйтесь рассрочкой <br>
                        на 6 месяцев под 0%
                    </div>
                </div>
            </div>
            <div class="row deream_marg">
                <div class="col-md-4">
                    <div class="b_dream__box">
                        <div class="b_dream__box">
                            <img src="./img/like.png" alt="like">
                        </div>
                        <p class="b_dream__title">Удобно</p>
                        <p class="b_dream__desc">
                            Ежемесячный <br>
                            платеж можно совершать у <br>
                            администратора клуба
                        </p>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="b_dream__box">
                        <div class="b_dream__box">
                            <img src="./img/trophy.png" alt="trophy">
                        </div>
                        <p class="b_dream__title">Перспектива</p>
                        <p class="b_dream__desc">
                            Благоприятная кредитная <br>
                            история
                        </p>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="b_dream__box">
                        <div class="b_dream__box">
                            <img src="./img/target.png" alt="target">
                        </div>
                        <p class="b_dream__title">Выгодно</p>
                        <p class="b_dream__desc">
                            Если абонемент подорожает, то <br>
                            вы оплатите его по цене <br>
                            заключения договора
                        </p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 col-ms-12 col-xs-12">
                    <div class="dream_bottom">
                        <!--<a href="#" class="btn btn-big" data-toggle="modal" data-target="#callback">Взять абонемент в рассрочку</a>-->
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!--<section class="credit">-->
        <!--<div class="container">-->
            <!--<div class="row">-->
                <!--<div class="col-lg-12">-->
                    <!--<h1 class="title">-->
                        <!--хотите приобрести<span class="combos_title__color">абонемент в рассрочку?</span>-->
                    <!--</h1>-->
                    <!--<p class="credit_desc">Оставьте заявку и наш менеджер свяжется с вами</p>-->
                <!--</div>-->
                <!--<div class="col-lg-12">-->
                    <!--<form class="openForm">-->
                        <!--<input type="text" placeholder="Ваше имя">-->
                        <!--<input type="text" placeholder="Ваш телефон">-->
                        <!--<input type="submit" value="Отправить заявку">-->
                    <!--</form>-->
                <!--</div>-->
            <!--</div>-->
        <!--</div>-->
    <!--</section>-->

    <section class="health" id="health">
        <div class="container">
            <div class="row">
                <div class="col-md-10">
                    <div class="w_health">
                        <h3 class="title">ФИТНЕС-КЛУБ – МЕСТО ПОЛЕЗНОГО ДОСУГА</h3>
                        <h3 class="title_up">
                            <span>
                                праздничные мероприятия и конкурсы для наших гостей
                            </span>
                        </h3>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="v_health">
                        <iframe width="100%" height="100%" src="https://www.youtube.com/embed/OzFIYKaKtWk?controls=0&amp;showinfo=0" frameborder="0" gesture="media" allow="encrypted-media" allowfullscreen></iframe>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="v_health">
                        <iframe width="100%" height="100%" src="https://www.youtube.com/embed/kBzc1-7SpMY?controls=0&amp;showinfo=0" frameborder="0" gesture="media" allow="encrypted-media" allowfullscreen></iframe>
                    </div>
                </div>
            </div>
        </div>
    </section>


    <!--<section class="unlimited_month3">-->
        <!--<div class="container">-->
            <!--<div class="row">-->
                <!--<div class="col-md-12">-->
                    <!--<div class="unlimited_month3__wrapp">-->
                        <!--<div class="subscription_unlimited__month3">-->
                            <!--<p class="unlimited_month3__title">БЕЗЛИМИТНЫЙ АБОНЕМЕНТ</p>-->
                            <!--<ul class="subscription_unlimited__month3">-->
                                <!--<li class="unlimited_month3__text">3 МЕСЯЦА</li>-->
                                <!--<li class="pluse">+</li>-->
                                <!--<li class="unlimited_month3__gift__img"><img src="./img/podarok-box.png"-->
                                                                             <!--alt="podarok-box"></li>-->
                                <!--<li class="unlimited_month3__gift_text">в подарок!</li>-->
                            <!--</ul>-->
                        <!--</div>-->

                        <!--<div class="unlimited_month3__muscul">-->
                            <!--<div class="unlimited__month3__img_box">-->
                                <!--<img src="./img/muscles-black.png" alt="muscles-black">-->
                            <!--</div>-->
                            <!--<p class="unlimited_month3__desc">-->
                                <!--2 персональные тренировки <br>-->
                                <!--с инструктором-->
                            <!--</p>-->
                        <!--</div>-->

                        <!--<div class="unlimited_month3__muscul">-->
                            <!--<div class="unlimited__month3__img_box">-->
                                <!--<img src="./img/balispa.png" alt="muscles-black">-->
                            <!--</div>-->
                            <!--<p class="unlimited_month3__desc">-->
                                <!--1 часовых <br>-->
                                <!--сеанса массажа-->
                            <!--</p>-->
                        <!--</div>-->

                        <!--<div class="unlimited_month3__muscul">-->
                            <!--<div class="unlimited__month3__img_box">-->
                                <!--<img src="./img/lazer-min.png" alt="muscles-black">-->
                            <!--</div>-->
                            <!--<p class="unlimited_month3__desc">-->
                                <!--1 сеанс <br>-->
                                <!--лазерной эпиляции-->
                            <!--</p>-->
                        <!--</div>-->
                        <!--<div class="subscription_unlimited__price">-->
                            <!--<p class="unlimited_month3__number">5 000 Р</p>-->
                            <!--<div class="unlimited_month3__gift">-->
                                <!--<a href="#" class="btn btn-md" data-toggle="modal" data-target="#callback">ЗАКАЗАТЬ</a>-->
                            <!--</div>-->
                        <!--</div>-->
                    <!--</div>-->
                <!--</div>-->
            <!--</div>-->
            <!--<div class="row">-->
                <!--<div class="col-md-12">-->
                    <!--<div class="unlimited_month3__wrapp">-->
                        <!--<div class="subscription_unlimited__month3">-->
                            <!--<p class="unlimited_month3__title">БЕЗЛИМИТНЫЙ АБОНЕМЕНТ</p>-->
                            <!--<ul class="subscription_unlimited__month3">-->
                                <!--<li class="unlimited_month3__text">1 МЕСЯЦА</li>-->
                                <!--<li class="pluse">+</li>-->
                                <!--<li class="unlimited_month3__gift__img"><img src="./img/podarok-box.png"-->
                                                                             <!--alt="podarok-box"></li>-->
                                <!--<li class="unlimited_month3__gift_text">в подарок!</li>-->
                            <!--</ul>-->
                        <!--</div>-->
                        <!--<div class="unlimited_month1__muscul">-->
                            <!--<div class="unlimited__month1__img_box">-->
                                <!--<img src="./img/muscles-big.png" alt="muscles-black">-->
                            <!--</div>-->
                            <!--<p class="unlimited_month1__desc">-->
                                <!--2 персональные тренировки-->
                                <!--с инструктором-->
                            <!--</p>-->
                        <!--</div>-->
                        <!--<div class="subscription_unlimited__price">-->
                            <!--<p class="unlimited_month3__number">2 400 Р</p>-->
                            <!--<div class="unlimited_month3__gift">-->
                                <!--<a href="#" class="btn btn-md" data-toggle="modal" data-target="#callback">ЗАКАЗАТЬ</a>-->
                            <!--</div>-->
                        <!--</div>-->
                    <!--</div>-->
                <!--</div>-->
            <!--</div>-->
        <!--</div>-->
    <!--</section>-->

    <section class="direction" id="direction">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="w_direction">
                        <h2 class="title">Выбери свое направление</h2>
                        <h3 class="title_up">
                        <span>
                            В безлимитный абонемент входят ГРУППОВЫЕ ЗАНЯТИЯ НА ЛЮБОЙ ВКУС!
                        </span>
                        </h3>
                        <!--<h2 class="title_line">-->
                            <!--<span><span>Выбери </span>-->
                                 <!--&nbsp;свое направление-->
                            <!--</span>-->
                        <!--</h2>-->
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="direction_video">
                        <h3 class="direction_video__title">Фитнес</h3>
                        <div class="direction_video__box">
                            <img src="./img/layer-15.png" alt="" class="img-fluid">
                        </div>
                        <a href="#" class="btn btn-sm" data-toggle="modal" data-target="#callback">ЗАКАЗАТЬ</a>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="direction_video">
                        <h3 class="direction_video__title">Йога</h3>
                        <div class="direction_video__box">
                            <img src="./img/layer-16.png" alt="" class="img-fluid">
                        </div>
                        <a href="#" class="btn btn-sm" data-toggle="modal" data-target="#callback">ЗАКАЗАТЬ</a>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="direction_video">
                        <h3 class="direction_video__title">Реформа</h3>
                        <div class="direction_video__box">
                            <img src="./img/layer-17.png" alt="" class="img-fluid">
                        </div>
                        <a href="#" class="btn btn-sm" data-toggle="modal" data-target="#callback">ЗАКАЗАТЬ</a>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="direction_video">
                        <h3 class="direction_video__title">Twerk</h3>
                        <div class="direction_video__box">
                            <img src="./img/layer-18.png" alt="" class="img-fluid">
                        </div>
                        <a href="#" class="btn btn-sm" data-toggle="modal" data-target="#callback">ЗАКАЗАТЬ</a>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="direction_video">
                        <h3 class="direction_video__title">K1</h3>
                        <div class="direction_video__box">
                            <img src="./img/layer-19.png" alt="" class="img-fluid">
                        </div>
                        <a href="#" class="btn btn-sm" data-toggle="modal" data-target="#callback">ЗАКАЗАТЬ</a>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="direction_video">
                        <h3 class="direction_video__title">Худеем вместе</h3>
                        <div class="direction_video__box">
                            <img src="./img/layer-20.png" alt="" class="img-fluid">
                        </div>
                        <a href="#" class="btn btn-sm" data-toggle="modal" data-target="#callback">ЗАКАЗАТЬ</a>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!--<section class="table" id="table">-->
        <!--<div class="container">-->
            <!--<div class="row">-->
                <!--<div class="col-md-12">-->
                    <!--<div class="w_table">-->
                        <!--<h2 class="title">В безлимитный абонемент входят</h2>-->
                        <!--<h2 class="title_line">-->
                            <!--<span>групповые<span>&nbsp;занятия на любой вкус!</span></span></h2>-->
                    <!--</div>-->
                <!--</div>-->
                <!--<div class="col-md-12">-->
                    <!--<div class="table_img">-->
                        <!--<img src="./img/Gruppa_1.png" alt="table">-->

                        <!--<div class="row table_bot">-->
                            <!--<div class="col-md-6">-->
                                <!--<p class="w_table__desc"><span class="part_fetnes__logo_last">Не можете выбрать?</span>-->
                                    <!--Оставьте заявку,<br>-->
                                    <!--и наш менеджер проконсультирует вас <br>-->
                                    <!--по каждому направлению!</p>-->
                            <!--</div>-->
                            <!--<div class="col-md-6">-->
                                <!--<div class="table_big__btn">-->
                                    <!--<a href="#" class="btn btn-big" data-toggle="modal" data-target="#callback">Оставить-->
                                        <!--заявку</a>-->
                                <!--</div>-->
                            <!--</div>-->
                        <!--</div>-->
                    <!--</div>-->
                <!--</div>-->
            <!--</div>-->
        <!--</div>-->
    <!--</section>-->



</main>


<?get_footer();?>